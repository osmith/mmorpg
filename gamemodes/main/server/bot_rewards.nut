
local drops = [
	[
		{item = "ITPO_HEALTH_01", amount = 1, chance = 35},
		{item = "ITPO_HEALTH_02", amount = 1, chance = 30},
		{item = "ITMW_1H_BAU_MACE", amount = 1, chance = 15},
		{item = "ITAR_BAU_L", amount = 1, chance = 10},
		{item = "ITPO_HEALTH_03", amount = 1, chance = 8},
		{item = "ITAR_PRISONER", amount = 1, chance = 5},
		{item = "ITMW_1H_MISC_SWORD", amount = 1, chance = 3},
		{item = "ITAR_LEATHER_L", amount = 1, chance = 2}
	],
	[
		{item = "ITPO_HEALTH_01", amount = 1, chance = 40},
		{item = "ITPO_HEALTH_02", amount = 1, chance = 30},
		{item = "ITMW_ADDON_KNIFE01", amount = 1, chance = 15},
		{item = "ITAR_NOV_L", amount = 1, chance = 10},
		{item = "ITPO_HEALTH_03", amount = 1, chance = 8},
		{item = "ITAR_LESTER", amount = 1, chance = 5},
		{item = "ITMW_SHORTSWORD4", amount = 1, chance = 3},
		{item = "ITAR_DIEGO", amount = 1, chance = 2}
	],
	[
		{item = "ITPO_HEALTH_01", amount = 1, chance = 45},
		{item = "ITPO_HEALTH_02", amount = 1, chance = 30},
		{item = "ITMW_1H_BAU_MACE", amount = 1, chance = 15},
		{item = "ITAR_BDT_M", amount = 1, chance = 10},
		{item = "ITPO_HEALTH_03", amount = 1, chance = 8},
		{item = "ITAR_PIR_L_ADDON", amount = 1, chance = 5},
		{item = "ITMW_1H_MISC_SWORD", amount = 1, chance = 3},
		{item = "ITAR_KDF_L", amount = 1, chance = 2}
	],
	[
		{item = "ITPO_HEALTH_01", amount = 1, chance = 40},
		{item = "ITPO_HEALTH_02", amount = 1, chance = 30},
		{item = "ITMW_ADDON_KNIFE01", amount = 1, chance = 15},
		{item = "ITAR_NOV_L", amount = 1, chance = 10},
		{item = "ITPO_SPEED", amount = 1, chance = 8},
		{item = "ITAR_LESTER", amount = 1, chance = 5},
		{item = "ITMW_SHORTSWORD4", amount = 1, chance = 3},
		{item = "ITAR_DIEGO", amount = 1, chance = 2}
	]];

function givePlayerXP(pid,xp) {
	local packet = Packet();
	packet.writeChar('b');
	packet.writeUInt8(PACKET_SENDXP);
	packet.writeUInt16(xp);
	packet.send(pid,RELIABLE_ORDERED);
}

function givePlayerBossDrop(ident) {
	if (ident == 7) {}};


function givePlayerDrop(pid,lvlbot) {
	if (lvlbot == 1) {
		local _chance = (1.0 * 100 * rand() / RAND_MAX).tointeger();
		for (local i = 0; i < drops[0].len(); i++) {
			local __chance = drops[0][(drops[0].len() - 1) - i].chance;
			if (_chance <= __chance) {
				local item = drops[0][(drops[0].len() - 1) - i].item;
				local amount = drops[0][(drops[0].len() - 1) - i].amount;
				giveItem(pid,Items.id(item),amount);
				if (item != "ITPO_HEALTH_01" && item != "ITPO_HEALTH_02") {
					local packet = Packet();
					packet.writeChar('b');
					packet.writeUInt8(PACKET_SENDITEMNAME);
					packet.writeString(item);
					packet.writeUInt8(amount);
					packet.send(pid,RELIABLE_ORDERED);
				}
				break;
			} else {
				continue;
			}
		}
	} else if (lvlbot == 2) {
		local _chance = 1.0 * 100 * rand() / RAND_MAX;
		for (local i = 0; i < drops[1].len(); i++) {
			local __chance = drops[1][(drops[1].len() - 1) - i].chance;
			if (_chance <= __chance) {
				local item = drops[1][(drops[1].len() - 1) - i].item;
				local amount = drops[1][(drops[1].len() - 1) - i].amount;
				giveItem(pid,Items.id(item),amount);
				if (item != "ITPO_HEALTH_01" && item != "ITPO_HEALTH_02") {
					local packet = Packet();
					packet.writeChar('b');
					packet.writeUInt8(PACKET_SENDITEMNAME);
					packet.writeString(item);
					packet.writeUInt8(amount);
					packet.send(pid,RELIABLE_ORDERED);
				}
				break;
			} else {
				continue;
			}
		}
	}
}


function botrewards_exp(bid,kid) {
	local botid = Bot.getBot(bid).getIdent();
	local botlvl = Bot.getBot(bid).getLevel();
	if (botid == 1) { 					// Wolf
		givePlayerXP(kid,25);
	} else if (botid == 2) {			// Rat
		givePlayerXP(kid,35);
	} else if (botid == 3) {			// Bug
		givePlayerXP(kid,50);
	} else if (botid == 4) {			// Infected bug
		givePlayerXP(kid,70);
	} else if (botid == 5) {			// Enraged wolf
		givePlayerXP(kid,100);
	} else if (botid == 6) {			// Black wolf
		givePlayerXP(kid,130);
	} else if (botid == 7) {			// Mutated wolf (BOSS)
		local pos = getPlayerPos(bid);
		for (local i = 0; i < getPlayersCount(); i++) {
			local pos2 = getPlayerPos(i);
			if (getDistance3d(pos.x,pos.y,pos.z,pos2.x,pos2.y,pos2.z) <= 800) {
				givePlayerXP(kid,800);
			}
		}
	} else if (botid == 8) {			// King of the Wolves (BOSS)
		local pos = getPlayerPos(bid);
		for (local i = 0; i < getPlayersCount(); i++) {
			local pos2 = getPlayerPos(i);
			if (getDistance3d(pos.x,pos.y,pos.z,pos2.x,pos2.y,pos2.z) <= 800) {
				givePlayerXP(kid,1500);
			}
		}
	}
	givePlayerDrop(kid,botlvl);
}

addEventHandler("onBotDead",botrewards_exp);