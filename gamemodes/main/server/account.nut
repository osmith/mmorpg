
function account_create(pid, pass) {
	if (!getPlayer(pid).getLogged()) {
		if (!mysql_checkAccount(getPlayerName(pid))) {
			local pattern = regexp("[0-9a-zA-Z]+");
			local _search = pattern.search(pass);
			if (_search == null) {
				sendMessageToPlayer(pid,255,0,0,"������ ����� ��������� ����� 0-9, � ����� ���������� �������� (A-Z)!");
			} else {
				if (pass.slice(_search.begin,_search.end) != pass) {
					sendMessageToPlayer(pid,255,0,0,"������ ����� ��������� ����� 0-9, � ����� ���������� �������� (A-Z)!");
				} else {
					mysql_createAccount(getPlayerName(pid),pass);
					equipItem(pid,Items.id("ITMW_1H_VLK_DAGGER"));
					equipItem(pid,Items.id("ITAR_BAU_M"));
					giveItem(pid,Items.id("ITPO_HEALTH_01"),1);
					setPlayerStrength(pid,25);
					setPlayerDexterity(pid,25);
					getPlayer(pid).setLogged(true);
					mysql_saveCharacter(pid);
					local packet = Packet();
					packet.writeChar('b');
					packet.writeUInt8(PACKET_LOGGED);
					packet.send(pid,RELIABLE_ORDERED);
					sendMessageToPlayer(pid,0,255,0,"�� ������� ������������������. �������� ����!");
					spawnPlayer(pid);
					setPlayerMaxHealth(pid,150);
					setPlayerHealth(pid,150);
				}
			}
		} else {
			sendMessageToPlayer(pid,255,0,0,"������� � ����� ������ ��� ����������. ������� �������, ��� ������� � ������� (/��� ������).");
		}
	}
}

function account_login(pid, pass) {
	if (!getPlayer(pid).getLogged()) {
		if (mysql_checkAccount(getPlayerName(pid))) {
			if (mysql_readAccount(getPlayerName(pid),pass)) {
				getPlayer(pid).setLogged(true);
				sendMessageToPlayer(pid,0,255,0,"�� ������� ��������������. �������� ����!");
				local packet = Packet();
				packet.writeChar('b');
				packet.writeUInt8(PACKET_LOGGED);
				packet.send(pid,RELIABLE_ORDERED);
				spawnPlayer(pid);
				mysql_readInventory(pid);
				mysql_readCharacter(pid);
				mysql_readAddStats(pid);
			} else {
				getPlayer(pid).setWrongAttempts(getPlayer(pid).getWrongAttempts() + 1);
				if (getPlayer(pid).getWrongAttempts() != 3) {
					sendMessageToPlayer(pid,255,0,0,"�������� ������! �������: " + getPlayer(pid).getWrongAttempts() + "/3");
				} else {
					kick(pid,"Wrong password!");
				}
			}
		} else {
			sendMessageToPlayer(pid,255,0,0,"�������� � ����� ������ �� ����������. ����������������� (/��� ������), ��� ������� �������.");
		}
	}
}

// ============ ============ //

function account_onCommand(pid,cmd,params) {
	switch (cmd) {
		case "���":
			local args = sscanf("s",params);
			if (args)
				account_create(pid,args[0]);
			break;
		case "���":
			local args = sscanf("s",params);
			if (args)
				account_login(pid,args[0]);
			break;
	}
}

function account_onDisconnect(pid,reason) {
	if (getPlayer(pid).getLogged())
		mysql_saveCharacter(pid);
	deletePlayer(pid);
}

addEventHandler("onPlayerCommand",account_onCommand);
addEventHandler("onPlayerDisconnect",account_onDisconnect);