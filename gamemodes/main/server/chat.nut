
function chat_onPlayerMessage(pid,message) {
	if (getPlayer(pid).getLogged()) {
		sendMessageToAll(255,255,255,getPlayerName(pid) + ": " + message);
	}
}

addEventHandler("onPlayerMessage",chat_onPlayerMessage);