
function spawn_WolfLVL1(x,y,z,angle) {
	local bot = BotMonster("����",x,y,z,angle);

	bot.setLevel(1);
	bot.setIdent(1);
	bot.setMaxHealth(10);
	bot.setHealth(10);
	bot.setStrength(5);
	bot.setDexterity(5);

	bot.setInstance("WOLF");
}

function spawn_RatLVL1(x,y,z,angle) {
	local bot = BotMonster("�����",x,y,z,angle);

	bot.setLevel(1);
	bot.setIdent(2);
	bot.setMaxHealth(15);
	bot.setHealth(15);
	bot.setStrength(8);
	bot.setDexterity(8);

	bot.setInstance("GIANT_RAT");
}

function spawn_BugLVL2(x,y,z,angle) {
	local bot = BotMonster("������� ���",x,y,z,angle);

	bot.setLevel(2);
	bot.setIdent(3);
	bot.setMaxHealth(25);
	bot.setHealth(25);
	bot.setStrength(15);
	bot.setDexterity(15);
	
	bot.setInstance("GIANT_BUG");
	bot.setScale(0.5,0.5,0.5);
}

function spawn_BugLVL3(x,y,z,angle) {
	local bot = BotMonster("���������� ���",x,y,z,angle);

	bot.setLevel(3);
	bot.setIdent(4);
	bot.setMaxHealth(45);
	bot.setHealth(45);
	bot.setStrength(25);
	bot.setDexterity(25);

	bot.setInstance("GIANT_BUG");
	bot.setScale(0.5,0.5,0.5);
}

function spawn_EnragedWolfLVL4(x,y,z,angle) {
	local bot = BotMonster("����������� ����",x,y,z,angle);

	bot.setLevel(4);
	bot.setIdent(5);
	bot.setMaxHealth(70);
	bot.setHealth(70);
	bot.setStrength(35);
	bot.setDexterity(35);

	bot.setInstance("WOLF");
}

function spawn_BlackWolfLVL6(x,y,z,angle) {
	local bot = BotMonster("������ ����",x,y,z,angle);

	bot.setLevel(6);
	bot.setIdent(6);
	bot.setMaxHealth(90);
	bot.setHealth(90);
	bot.setStrength(45);
	bot.setDexterity(45);

	bot.setInstance("BLACKWOLF");
}

function spawn_BossWolf01(x,y,z,angle) {
	local bot = BotMonster("������������ ����",x,y,z,angle);

	bot.setLevel(8);
	bot.setIdent(7);
	bot.setMaxHealth(500);
	bot.setHealth(500);
	bot.setStrength(40);
	bot.setDexterity(40);

	bot.setInstance("BLACKWOLF");
	bot.setScale(1.5,1.5,1.5);
}

function spawn_BossWolf02(x,y,z,angle) {
	local bot = BotMonster("������ ������",x,y,z,angle);

	bot.setLevel(10);
	bot.setIdent(8);
	bot.setMaxHealth(1000);
	bot.setHealth(1000);
	bot.setStrength(60);
	bot.setDexterity(60);

	bot.setInstance("WARG");
	bot.setScale(2.5,2.5,2.5);
}