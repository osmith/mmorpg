
const MYSQL_HOST = "localhost";
const MYSQL_DB = "g2ommo";
const MYSQL_PASS = "";
const MYSQL_USER = "root";
MYSQL <- -1;

function mysql_saveCharacter(pid) {
	getPlayer(pid).reduceLVLStats();
	local pos = getPlayerPosition(pid);
	local angle = getPlayerAngle(pid);
	local maxhp = getPlayerMaxHealth(pid);
	local hp = getPlayerHealth(pid);
	local str = getPlayerStrength(pid);
	local dex = getPlayerDexterity(pid);
	local _str = pos.x + "," + pos.y + "," + pos.z + "," + angle + "," + maxhp + "," + hp + "," + str + "," + dex;
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`characters` WHERE `name` = '" + getPlayerName(pid) + "'");
	if (mysql_num_rows(handler) != 0) { 
		local row = mysql_fetch_assoc(handler);
		mysql_query(MYSQL,"DELETE FROM `" + MYSQL_DB + "`.`characters` WHERE `name` = '" + getPlayerName(pid) + "'");
		mysql_query(MYSQL,"INSERT INTO `" + MYSQL_DB + "`.`characters` (`name`,`mainstats`,`addstats`) VALUES ('" + getPlayerName(pid) + "','" + _str + "','" + row["addstats"] + "')");
	} else {
		mysql_query(MYSQL,"DELETE FROM `" + MYSQL_DB + "`.`characters` WHERE `name` = '" + getPlayerName(pid) + "'");
		mysql_query(MYSQL,"INSERT INTO `" + MYSQL_DB + "`.`characters` (`name`,`mainstats`) VALUES ('" + getPlayerName(pid) + "','" + _str + "')");
	}
}

function mysql_saveAdditionalStats(pid,str) {
	mysql_query(MYSQL,"UPDATE `" + MYSQL_DB + "`.`characters` SET `addstats` = '" + str + "' WHERE `name` = '" + getPlayerName(pid) + "'");
}

function mysql_clearInventory(pid) {
	mysql_query(MYSQL,"DELETE FROM `" + MYSQL_DB + "`.`items` WHERE `name` = '" + getPlayerName(pid) + "'");
}

function mysql_saveItem(pid,item,amount,eq) {
	local instance = Items.name(item);
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`items` WHERE `name` = '" + getPlayerName(pid) + "' AND `item` = '" + instance + "'");
	if (mysql_num_rows(handler) != 0) {
		mysql_query(MYSQL,"UPDATE `" + MYSQL_DB + "`.`items` SET `amount` = '" + amount + ", `eq` = '" + eq + "' WHERE `name` = '" + getPlayerName(pid) + "' AND `item` = '" + instance + "'");
	} else {
		mysql_query(MYSQL,"INSERT INTO `" + MYSQL_DB + "`.`items` (`name`,`item`,`amount`,`eq`) VALUES ('" + getPlayerName(pid) + "','" + instance + "','" + amount + "','" + eq + "')");
	}
}

function mysql_readCharacter(pid,exceptPosition = false) {
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`characters` WHERE `name` = '" + getPlayerName(pid) + "'");
	local row = mysql_fetch_assoc(handler);
	local params = row["mainstats"];
	local params2 = row["addstats"];
	local stats = split(params,",");
	local stat2 = split(params2,",");
	if (exceptPosition == false) {
		setPlayerPosition(pid,stats[0].tofloat(),stats[1].tofloat(),stats[2].tofloat());
		setPlayerAngle(pid,stats[3].tofloat());
		setPlayerHealth(pid,stats[4].tointeger());
	}
}

function mysql_readAddStats(pid) {
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`characters` WHERE `name` = '" + getPlayerName(pid) + "'");
	local row = mysql_fetch_assoc(handler);
	local params = row["addstats"];
	local stats = split(params,",");
	local packet = Packet();
	packet.writeChar('b');
	packet.writeUInt8(PACKET_READSTATS);
	packet.writeUInt8(stats[0].tointeger());
	packet.writeUInt16(stats[1].tointeger());
	packet.writeUInt16(stats[2].tointeger());
	packet.writeUInt16(stats[3].tointeger());
	packet.send(pid,RELIABLE_ORDERED);
	getPlayer(pid).setLevel(stats[0].tointeger());
}

function mysql_readInventory(pid) {
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`items` WHERE `name` = '" + getPlayerName(pid) + "'");
	if (handler) {
		local count = mysql_num_rows(handler);
		for (local i = 0; i < count; i++) {
			local row = mysql_fetch_assoc(handler);
			if (row["eq"] == 1) {
				equipItem(pid,Items.id(row["item"]));
				giveItem(pid,Items.id(row["item"]),row["amount"] - 1);
			} else {
				giveItem(pid,Items.id(row["item"]),row["amount"]);
			}
		}
	}
}


function mysql_readAccount(name,pass) {
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`accounts` WHERE `name` = '" + name + "'");
	local row = mysql_fetch_assoc(handler);
	if (row["pass"] == pass)
		return true;
	else
		return false;
}

function mysql_createAccount(name,pass) {
	mysql_query(MYSQL,"INSERT INTO `" + MYSQL_DB + "`.`accounts` (`name`,`pass`) VALUES ('" + name + "','" + pass + "')");
}

function mysql_checkAccount(name) {
	local handler = mysql_query(MYSQL,"SELECT * FROM `" + MYSQL_DB + "`.`accounts` WHERE `name` = '" + name + "'");
	if (mysql_num_rows(handler) != 0)
		return true;
	else
		return false;
}

// ============== =============== //

function mysql_init() {
	MYSQL = mysql_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB,3306);
	if (MYSQL)
		print("-====== \tMYSQL connected! \t======-");
	else
		print("-====== \tFailed to connect MYSQL! \t======-");

	mysql_query(MYSQL,"SET CHARACTER SET CP1251");
}

function mysql_packet(pid, data) {
	local id = data.readChar();
	if (id == 'b') {
		local id2 = data.readUInt8();
		if (id2 == PACKET_SENDSAVESTATS) {
			local str = data.readString();
			mysql_saveAdditionalStats(pid,str);
		}
		if (id2 == PACKET_SAVEITEM) {
			local item = data.readUInt16();
			local amount = data.readUInt16();
			local eq = data.readUInt8();
			mysql_saveItem(pid,item,amount,eq);
		}
		if (id2 == PACKET_CLEARINVENTORY) {
			mysql_clearInventory(pid);
		}
	}
}

addEventHandler("onInit",mysql_init);
addEventHandler("onPacket",mysql_packet);