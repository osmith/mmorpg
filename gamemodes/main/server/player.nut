
Player <- [];

class Players {
	constructor (id) {
		this.id = id;
		this.name = getPlayerName(id);
	}

	// ----- GETTERS ------ //

	function getID() { return id;}
	function getLogged() { return logged;}
	function getName() { return name;}
	function getWrongAttempts() { return wrong_attempts;};
	function getLevel() { return level;};

	id = -1;
	name = -1;
	wrong_attempts = 0;

	level = 0;
	logged = false;
}

function Players::setLogged(arg) {
	logged = arg;
}

function Players::checkNickname() {
	local pattern = regexp("[a-zA-Z]+");
	local _search = pattern.search(getName());
	if (_search == null) {
		kick(getID(),"Only A-Z, a-z in the nickname!");
	} else if (name.slice(_search.begin,_search.end) != name) {
		kick(getID(),"Only A-Z, a-z in the nickname!");
	}
}

function Players::setWrongAttempts(arg) {
	wrong_attempts = arg;
}

function Players::setLevel(lvl) {
	reduceLVLStats();
	level = lvl;
	gainLVLStats();
}

function Players::reduceLVLStats() {
	local lvl = getLevel();
	local hp = getPlayerMaxHealth(id);
	local str = getPlayerStrength(id);
	local dex = getPlayerDexterity(id);
	setPlayerStrength(id,str - (lvl * STR_PER_LVL));
	setPlayerDexterity(id,dex - (lvl * DEX_PER_LVL));
	setPlayerMaxHealth(id,hp - (lvl * HEALTH_PER_LVL));
}


function Players::gainLVLStats() {
	local lvl = getLevel();
	local hp = getPlayerMaxHealth(id);
	local str = getPlayerStrength(id);
	local dex = getPlayerDexterity(id);
	setPlayerStrength(id,str + (lvl * STR_PER_LVL));
	setPlayerDexterity(id,dex + (lvl * DEX_PER_LVL));
	setPlayerMaxHealth(id,hp + (lvl * HEALTH_PER_LVL));
}


/// -------- ---------- ////

function createPlayer(id) {
	Player.append(Players(id));
	local _player = getPlayer(id);
	_player.checkNickname();
}

function getPlayer(id) {
	for (local i = 0; i < Player.len(); i++) {
		if (Player[i].getID() == id) {
			return Player[i];
			break;
		}
	}
}

function deletePlayer(id) {
	for (local i = 0; i < Player.len(); i++) {
		if (Player[i].getID() == id) {
			Player.remove(i);
			break;
		}
	}
}

/// -------- ---------- ////

function player_onJoin(pid) {
	createPlayer(pid);
}

function player_onDisconnect(pid,reason) {
	//deletePlayer(pid);
}

function player_onPacket(pid, data) {
	local id = data.readChar();
	if (id == 'b') {
		local id2 = data.readUInt8();
		if (id2 == PACKET_SENDLVL) {
			local lvl = data.readUInt8();
			getPlayer(pid).setLevel(lvl);
		}
	}
}

addEventHandler("onPlayerJoin",player_onJoin);
addEventHandler("onPlayerDisconnect",player_onDisconnect);
addEventHandler("onPacket",player_onPacket);