
function save_additionalStats() {
	local str = PlayerLevel + "," + PlayerExp + "," + PlayerNextExp + "," + PlayerLP;
	local packet = Packet();
	packet.writeChar('b');
	packet.writeUInt8(PACKET_SENDSAVESTATS);
	packet.writeString(str);
	packet.send(RELIABLE_ORDERED);
}

function save_inventory() {
	local packet = Packet();
	packet.writeChar('b');
	packet.writeUInt8(PACKET_CLEARINVENTORY);
	packet.send(RELIABLE_ORDERED);
	if (PlayerInventory.len() != 0) {
		for (local i = 0; i < PlayerInventory.len(); i++) {
			local item = PlayerInventory[i].item;
			local amount = PlayerInventory[i].amount;
			local eq = PlayerInventory[i].eq;
			local packet = Packet();
			packet.writeChar('b');
			packet.writeUInt8(PACKET_SAVEITEM);
			packet.writeUInt16(Items.id(item));
			packet.writeUInt16(amount);
			packet.writeUInt8(eq);
			packet.send(RELIABLE_ORDERED);
		}
	}
}

function save_onPacket(data) {
	local id = data.readChar();
	if (id == 'b') {
		local id2 = data.readUInt8();
		if (id2 == PACKET_SENDSAVESTATS) {
			save_additionalStats();
		}
		if (id2 == PACKET_READSTATS) {
			local lvl = data.readUInt8();
			local exp = data.readUInt16();
			local nexp = data.readUInt16();
			local lp = data.readUInt16();
			setLevel(lvl);
			setExp(exp);
			setNextLevelExp(nexp);
			setLearnPoints(lp);
		}
	}
}

function save_destroy(id) {
	if (id == heroId)
		inv_save();
}

addEventHandler("onPacket",save_onPacket);
addEventHandler("onPlayerDestroy",save_destroy);