
function inv_save() {
	local inv = getEq();
	local melee = getPlayerMeleeWeapon(heroId);
	local ranged = getPlayerRangedWeapon(heroId);
	local armor = getPlayerArmor(heroId);
	PlayerInventory.clear();
	for (local i = 0; i < inv.len(); i++) {
		local item = inv[i].instance;
		local amount = inv[i].amount;
		if (item == melee || item == ranged || item == armor) 
			PlayerInventory.append({item = item, amount = amount, eq = 1});
		else
			PlayerInventory.append({item = item, amount = amount, eq = 0});
	}
	save_inventory();
}

function inv_onTake(item,amount) {
	inv_save();
}

function inv_onDrop(item,amount) {
	inv_save();
}

function inv_onUse(item,amount,hand) {
	inv_save();
}

addEventHandler("onTakeItem",inv_onTake);
addEventHandler("onDropItem",inv_onDrop);
addEventHandler("onUseItem",inv_onUse);