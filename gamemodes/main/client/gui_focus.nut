enableHud(4,false);

local bar_health_back = Button({
	x = 3237,
	y = 317,
	w = 1725,
	h = 221,
	texture = "BAR_BACK.TGA"});

local bar_health = Button({
	x = 3303,
	y = 361,
	w = 1606,
	h = 135,
	texture = "BAR_HEALTH.TGA"});

local focus_name = Text({
	text = " ",
	font = "Font_Old_10_White_Hi.TGA",
	x = 3327,
	y = 61,
	r = 255,
	g = 255,
	b = 255});

local focus_lvl = Text({
	text = " ",
	font = "Font_Old_10_White_Hi.TGA",
	x = 3327,
	y = 61,
	r = 255,
	g = 255,
	b = 255});

local focus_type = Text({
	text = " ",
	font = "Font_Old_10_White_Hi.TGA",
	x = 3327,
	y = 61,
	r = 255,
	g = 255,
	b = 255});

function guifocus_onRenderFocus(vob,id,x,y,name) {
	local maxhp = getPlayerMaxHealth(id);
	local hp = getPlayerHealth(id);
	local percents = hp.tofloat() / (maxhp.tofloat() / 100.0);
	local params = bar_health.getParams();
	bar_health.setSize(16 * percents,params.h);
}

function guifocus_onFocus(id,oid) {
	if (id != -1) {
		local _isbot = Bot.isBot(id);
		if (_isbot) {
			local name = getPlayerName(id);
			local lvl = Bot.getBot(id).getLevel();
			local plvl = getLevel();
			local _switch = lvl - plvl;
			local lvlcolor = -1;
			local type = BotTypeText1;
			local typecolor = BotTypesColors[0];
			if (_switch <= 0)
				lvlcolor = BotLevelColors[0];
			else if (_switch == 1)
				lvlcolor = BotLevelColors[1];
			else if (_switch >= 2)
				lvlcolor = BotLevelColors[2];
			focus_name.setText(name);
			focus_lvl.setText(BotLevelText + " " + lvl.tostring());
			focus_type.setText(type);
			local pos = focus_name.getParams();
			focus_lvl.setPosition(pos.x + textWidth(focus_name.getParams().text) + 80,pos.y);
			focus_lvl.setColor(lvlcolor[0],lvlcolor[1],lvlcolor[2]);
			focus_type.setPosition(pos.x + textWidth(focus_name.getParams().text) + textWidth(focus_lvl.getParams().text) + 160,pos.y);
			focus_type.setColor(typecolor[0],typecolor[1],typecolor[2]);
			focus_lvl.visible(true);
			focus_name.visible(true);
			focus_type.visible(true);
			bar_health_back.visible(true);
			bar_health.visible(true);
		} else {
			local name = getPlayerName(id);
			focus_name.setText(name);
			focus_name.setColor(255,255,255);
			focus_type.visible(false);
			focus_name.visible(true);
			focus_lvl.visible(false);
			bar_health.visible(true);
			bar_health_back.visible(true);
		}
	} else {
		bar_health_back.visible(false);
		bar_health.visible(false);
		focus_lvl.visible(false);
		focus_name.visible(false);
		focus_type.visible(false);
	}
}

addEventHandler("onFocus",guifocus_onFocus);
addEventHandler("onRenderFocus",guifocus_onRenderFocus);