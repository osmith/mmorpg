
local playerLogged = false;

function main_onPlayerCreate(pid) {
	clearMultiplayerMessages();
	if (pid == heroId && playerLogged == false)
		setFreeze(true);
}

function main_onPacket(data) {
	local id = data.readChar();
	if (id == 'b') {
		local id2 = data.readUInt8();
		if (id2 == PACKET_LOGGED) {
			setFreeze(false);
			inv_save();
			save_additionalStats();
		}
	}
}

function main_onKey(key) {
	if (key == KEY_NUMPAD0) {
		local packet = Packet();
		packet.writeChar('b');
		packet.writeUInt8(15);
		packet.send(RELIABLE_ORDERED);
	}
}

addEventHandler("onPlayerCreate",main_onPlayerCreate);
addEventHandler("onPacket",main_onPacket);
addEventHandler("onKey",main_onKey);