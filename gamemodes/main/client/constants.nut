
BotTypesColors <- [
	[0,255,0],							// Common (Obicniy)
	[112,204,89]						// Uncommon (Neobichniy)
];

BotLevelColors <- [
	[255,255,255],						// Same
	[255,255,0],						// +1
	[255,67,0]							// +2
];

PlayerInventory <- [];
PlayerLevel <- 0;
PlayerExp <- 0;
PlayerNextExp <- 500;
PlayerLP <- 0;

const BotLevelText = "��.";
const BotTypeText1 = "�������";
const BotTypeText2 = "���������";

const HEALTH_PER_LVL = 15;
const STR_PER_LVL = 5;
const DEX_PER_LVL = 5;

const MAX_LEVEL = 6;

const PACKET_SENDXP = 1;
const PACKET_SENDITEMNAME = 2;
const PACKET_SENDSAVESTATS = 3;
const PACKET_SAVEITEM = 4;
const PACKET_READSTATS = 5;
const PACKET_LOGGED = 6;
const PACKET_CLEARINVENTORY = 7;
const PACKET_SENDLVL = 8;