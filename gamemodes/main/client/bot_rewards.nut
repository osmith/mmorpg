

function givePlayerXP(xp) {
	local exp = getExp();
	if (getLevel() != MAX_LEVEL)
		setExp(xp + exp);

	if (getExp() >= getNextLevelExp()) {
		local lvl = getLevel();
		setLevel(lvl + 1);
		setNextLevelExp(getNextLevelExp()*2);
		setLearnPoints(getLearnPoints() + 5);
		local packet = Packet();
		packet.writeChar('b');
		packet.writeUInt8(PACKET_SENDLVL);
		packet.writeUInt8(getLevel());
		packet.send(RELIABLE_ORDERED);
	}
	PlayerLevel = getLevel();
	PlayerExp = getExp();
	PlayerNextExp = getNextLevelExp();
	PlayerLP = getLearnPoints();
}

function botrewards_onPacket(data) {
	local id = data.readChar();
	if (id == 'b') {
		local id2= data.readUInt8();
		if (id2 == PACKET_SENDXP) {
			local xp = data.readUInt16();
			givePlayerXP(xp);
		}
		if (id2 == PACKET_SENDITEMNAME) {
			local item = data.readString();
			local amount = data.readUInt8();
			local inv = getEq();
			for (local i = 0; i < inv.len(); i++) {
				if (inv[i].instance == item) {
					local itemname = inv[i].name;
					Chat.print(255,255,0,"(�� �������� " + itemname + "x" + amount + ")");
					break;
				}
			}
		}
		inv_save();
		save_additionalStats();
	}
}

addEventHandler("onPacket",botrewards_onPacket);