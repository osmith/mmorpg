
local botnick_draw = Draw(0,0,"");
local botnick_curtarget = -1;
enable_NicknameId(false);

function botnick_onRenderFocus(vob,id,x,y,name) {
	//setPlayerName(id,"");
	botnick_draw.setPosition(x,y);
	local lvl = Bot.getBot(id).getLevel();
	local plvl = getLevel();
	local _switch = lvl - plvl;
	local lvlcolor = -1;
	if (_switch <= 0)
		lvlcolor = BotLevelColors[0];
	else if (_switch == 1)
		lvlcolor = BotLevelColors[1];
	else if (_switch >= 2)
		lvlcolor = BotLevelColors[2];
	botnick_draw.setColor(lvlcolor[0],lvlcolor[1],lvlcolor[2]);
	botnick_draw.text = name;
}

function botnick_onFocus(id,oid) {
	if (id != -1) {
		if (Bot.isBot(id)) {
			botnick_draw.visible = true;
			enableEvent_RenderFocus(true);
		} else {
			botnick_draw.visible = false;
			enableEvent_RenderFocus(false);
		}
	} else {
		botnick_draw.visible = false;
		enableEvent_RenderFocus(false);
	}
}

addEventHandler("onFocus",botnick_onFocus);
addEventHandler("onRenderFocus",botnick_onRenderFocus);