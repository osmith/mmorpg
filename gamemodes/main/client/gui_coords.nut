
local draw_xcoord = Draw(167,7571,"");
local draw_ycoord = Draw(700,7571,"");

enableEvent_Render(true);
function coords_onRender() {
	local pos = getPlayerPosition(heroId);
	draw_xcoord.text = "X: " + (pos.x / 100).tointeger();
	draw_ycoord.text = "Y: " + (pos.z / 100).tointeger();
	draw_xcoord.visible = true;
	draw_ycoord.visible = true;
}

addEventHandler("onRender",coords_onRender);