// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEvent("onSpawnBots");

// ------------------------------------------------------------------- //

addEventHandler("onInit", function()
{
    local startTick = getTickCount();
    print("Initializing bots (v" + Bot.getVersion() + ")...");

    callEvent("onSpawnBots");

    local deltaMS = getTickCount() - startTick;
    print ("Initialized " + Bot.getBotCount() + " bots, took: " + deltaMS + "ms");
    print("-========================================-");
});

// ------------------------------------------------------------------- //

addEventHandler("onSpawnBots", function()
{
   // BotPlacementNW.spawn();
});

// ------------------------------------------------------------------- //

class BotPlacementNW
{
    static function spawn()
    {
        spawnMonastery();
        //spawnBattle();
    }

    // ------------------------------------------------------------------- //

    function spawnBattle()
    {
        for (local x = 0; x < 1; x++)
            for (local z = 0; z < 1; z++)
                BotOrcWarrior("Orcwarrior " + (x + z), 65000 + x*300, 2615, -8000 + z*300, 0);
        for (local x = 0; x < 1; x++)
            for (local z = 0; z < 1; z++)
                BotOrcElite("Orcelite " + (x + z), 64800 + x*300, 2615, -8000 + z*300, 0);
        for (local x = 0; x < 1; x++)
            for (local z = 0; z < 1; z++)
                BotOrcShaman("Orcshaman " + (x + z), 64600 + x*300, 2615, -8000 + z*300, 0);

        // -- Wolves -- //
        for (local x = 0; x < 1; x++)
            for (local z = 0; z < 20; z++)
                local bot = BotMonster("Wolf " + (x + z), 64400 + x*300, 2615, -8000 + z*300, 0);

        // -- Bandits -- //
        for (local x = 0; x < 3; x++)
        {
            for (local z = 0; z < 20; z++)
            {
                local bot = BotHuman("Bandit " + (x + z), 65200 + x*300, 2615, -8000 + z*300, 0);
                bot.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Pony", rand() % 100);
                bot.setStrength(90);
                bot.setMaxHealth(150);
                bot.setHealth(100);

                if (rand() % 2 == 0)
                    bot.equipArmor(Items.id("ITAR_SLD_M"));
                else
                    bot.equipArmor(Items.id("ITAR_SLD_H"));

                if (rand() % 2 == 0)
                {
                    if (rand() % 2 == 0)
                        bot.equipMeleeWeapon(Items.id("ITMW_1H_SLD_SWORD"));
                    else
                        bot.equipMeleeWeapon(Items.id("ITMW_1H_SLD_AXE"));
                }
                else
                {
                    if (rand() % 2 == 0)
                        bot.equipMeleeWeapon(Items.id("ITMW_2H_ORCAXE_01"));
                    else
                        bot.equipMeleeWeapon(Items.id("ITMW_2H_SLD_SWORD"));

                    bot.setCombatTypeComp(COMBATTYPE_HUM_2H);
                }

                bot.getFactionComp().addToFaction("HumanBandit");
            }
        }

        // -- Paladins -- //
        for (local x = 0; x < 3; x++)
        {
            for (local z = 0; z < 20; z++)
            {
                local bot = BotHuman("Paladin " + (x + z), 64200 + x*300, 2626, -8000 + z*300, 0);
                bot.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Pony", rand() % 100);
                bot.setStrength(70);
                bot.setMaxHealth(100);
                bot.setHealth(100);

                if (rand() % 2 == 0)
                    bot.equipArmor(Items.id("ITAR_PAL_M"));
                else
                    bot.equipArmor(Items.id("ITAR_PAL_H"));

                if (rand() % 2 == 0)
                {
                    if (rand() % 2 == 0)
                        bot.equipMeleeWeapon(Items.id("ITMW_1H_MIL_SWORD"));
                    else
                        bot.equipMeleeWeapon(Items.id("ITMW_RUNENSCHWERT"));
                }
                else
                {
                    if (rand() % 2 == 0)
                        bot.equipMeleeWeapon(Items.id("ITMW_2H_SWORD_M_01"));
                    else
                        bot.equipMeleeWeapon(Items.id("ITMW_2H_PAL_SWORD"));

                    bot.setCombatTypeComp(COMBATTYPE_HUM_2H);
                }

                bot.getFactionComp().addToFaction("HumanFriendly");
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function spawnMonastery()
    {
        // -- Sheep 0 -- //
        local sheep0 = BotPrey("Sheep 0", 47836, 4990, 19571, 290);
        sheep0.getScheduleComp().addGoto(8, 30, 47836, 4990, 19571);
        sheep0.getScheduleComp().addTurn(8, 30, 290);

        // -- Sheep 1 -- //
        local sheep1 = BotPrey("Sheep 1", 47378, 4990, 19446, 10);
        sheep1.getScheduleComp().addGoto(8, 30, 47378, 4990, 19446);
        sheep1.getScheduleComp().addTurn(8, 30, 10);

        // -- Sheep 2 -- //
        local sheep2 = BotPrey("Sheep 2", 47233, 4990, 20065, 135);
        sheep2.getScheduleComp().addGoto(8, 30, 47233, 4990, 20065);
        sheep2.getScheduleComp().addTurn(8, 30, 135);

        // -- Novice 0 -- //
        local novice0 = BotNovice("Novice 0", 47034, 4990, 19750, 99);
        novice0.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Pony", 74);

        novice0.getScheduleComp().addGoto(8, 30, 49868, 5090, 18506);
        novice0.getScheduleComp().addTurn(8, 30, 306);
        novice0.getScheduleComp().addPlayAni(8, 30, "T_STAND_2_LGUARD");

        novice0.getScheduleComp().addGoto(12, 0, 47439, 4990, 18562);
        novice0.getScheduleComp().addTurn(12, 0, 6.5);

        novice0.getScheduleComp().addGoto(15, 0, 47019, 5090, 21527);
        novice0.getScheduleComp().addTurn(15, 0, 330);
        novice0.getScheduleComp().addPlayAni(15, 0,"T_STAND_2_PRAY");

        novice0.getScheduleComp().addSleep(20, 0, "NW_MONASTERY_NOVICE01_06", 238);

        // -- Novice 1 -- //
        local novice1 = BotNovice("Novice 1", 48528, 4990, 20036, 212);
        novice1.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Fighter", 36);

        novice1.getScheduleComp().addGoto(8, 0, 48528, 4990, 20036);
        novice1.getScheduleComp().addTurn(8, 0, 212);
        novice1.getScheduleComp().addPlayAni(8, 0, "T_STAND_2_LGUARD");

        novice1.getScheduleComp().addGoto(12, 0, 50174, 5090, 19976);
        novice1.getScheduleComp().addTurn(12, 0, 10);

        novice1.getScheduleComp().addGoto(13, 0, 48745, 5090, 22467);
        novice1.getScheduleComp().addTurn(13, 0, 261);
        novice1.getScheduleComp().addPlayAni(13, 0, "T_STAND_2_LGUARD");

        novice1.getScheduleComp().addSleep(19, 30, "NW_MONASTERY_NOVICE01_04", 56);

        // -- Novice 2 -- //
        local novice2 = BotNovice("Novice 2", 48131, 4990, 18116, 337);
        novice2.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Fighter", 33);
        novice2.setFatness(0);
        novice2.setCombatTypeComp(COMBATTYPE_HUM_FIST);

        novice2.getScheduleComp().addGoto(8, 0, 48131, 4990, 18116);
        novice2.getScheduleComp().addTurn(8, 0, 337);
        novice2.getScheduleComp().addRepeatAni(8, 0, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addGoto(8, 30, 47918, 4990, 18793);
        novice2.getScheduleComp().addTurn(8, 30, 90);
        novice2.getScheduleComp().addRepeatAni(8, 30, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addGoto(9, 0, 48470, 4990, 18601);
        novice2.getScheduleComp().addTurn(9, 0, 250);
        novice2.getScheduleComp().addRepeatAni(9, 0, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addGoto(10, 0, 49016, 4990, 19214);
        novice2.getScheduleComp().addTurn(10, 0, 247);
        novice2.getScheduleComp().addPlayAni(10, 0, "T_STAND_2_SIT", 3000);

        novice2.getScheduleComp().addGoto(12, 0, 48131, 4990, 18116);
        novice2.getScheduleComp().addTurn(12, 0, 337);
        novice2.getScheduleComp().addRepeatAni(12, 0, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addGoto(12, 30, 47918, 4990, 18793);
        novice2.getScheduleComp().addTurn(12, 30, 90);
        novice2.getScheduleComp().addRepeatAni(12, 30, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addGoto(13, 0, 48470, 4990, 18601);
        novice2.getScheduleComp().addTurn(13, 0, 250);
        novice2.getScheduleComp().addRepeatAni(13, 0, "T_PLUNDER", 5000, 0);

        novice2.getScheduleComp().addSleep(19, 0, "NW_MONASTERY_NOVICE02_04", 56);

        // -- Novice 3 -- //
        local novice3 = BotNovice("Novice 3", 47569, 4340, 17682, 340);
        novice3.setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Bald", 77);
        novice3.setFatness(0);
        novice3.equipMeleeWeapon(Items.id("ITMW_1H_VLK_DAGGER"));

        novice3.getScheduleComp().addGoto(9, 0, 47445, 4340, 17908);
        novice3.getScheduleComp().addTurn(9, 0, 329);
        novice3.getScheduleComp().addPlayAni(9, 0, "T_LAB_S0_2_S1", 5000);

        novice3.getScheduleComp().addGoto(12, 30, 47376, 4340, 18215);
        novice3.getScheduleComp().addTurn(12, 30, 328);
        novice3.getScheduleComp().addPlayAni(12, 30, "T_BOOK_S0_2_S1", 5000);

        novice3.getScheduleComp().addGoto(14, 0, 47339, 4340, 17529);
        novice3.getScheduleComp().addTurn(14, 0, 238);
        novice3.getScheduleComp().addRepeatAni(14, 0, "T_IGETYOU", 10000, 0);

        novice3.getScheduleComp().addGoto(17, 0, 47445, 4340, 17908);
        novice3.getScheduleComp().addTurn(17, 0, 329);
        novice3.getScheduleComp().addPlayAni(17, 0, "T_LAB_S0_2_S1", 5000);

        novice3.getScheduleComp().addSleep(19, 0, "NW_MONASTERY_NOVICE03_04", 60);
    }
}
