WayPoints.FPs["ADDON\\ADDON.ZEN"] <-
{
	["FP_ROAM_RHADEMES_03"] = {x = 21477.7832, y = -4026.63013, z = 27434.9688, adjacent =
	[
	]},
	["FP_ROAM_RHADEMES_02"] = {x = 21299.3555, y = -4041.88428, z = 27244.3828, adjacent =
	[
	]},
	["FP_ROAM_RHADEMES_01"] = {x = 21482.3477, y = -4019.29077, z = 27031.7598, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_12"] = {x = 19958.8359, y = -2587.58887, z = 22592.1152, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_11"] = {x = 16415.0078, y = -3305.41089, z = 23995.4863, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_10"] = {x = 17090.6973, y = -3307.30737, z = 24813.293, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_09"] = {x = 15682.5928, y = -3397.55469, z = 23515.875, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_08"] = {x = 16609.2422, y = -3724.28271, z = 27618.8105, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_07"] = {x = 19142.7285, y = -3724.59766, z = 26881.4336, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_06"] = {x = 24178.2422, y = -3994.98633, z = 28297.4727, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_05"] = {x = 25526.959, y = -4057.2688, z = 28949.3398, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_04"] = {x = 25030.5527, y = -3636.25928, z = 35810.3594, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_03"] = {x = 25713.4707, y = -3967.00146, z = 33649.5742, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_02"] = {x = 24543.5898, y = -4054.40869, z = 32079.0195, adjacent =
	[
	]},
	["FP_ITEM_ADANOSTEMPEL_01"] = {x = 24274.6836, y = -4044.7085, z = 30365.1113, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14C"] = {x = 23809.4785, y = -3977.06445, z = 28060.5586, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14B"] = {x = 23201.8281, y = -3958.57617, z = 28098.3789, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14A"] = {x = 22599.543, y = -3945.55347, z = 28105.0508, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14F"] = {x = 22605.5234, y = -3992.87158, z = 29578.793, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14D"] = {x = 23827.043, y = -3971.24194, z = 29480.2012, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_RHADEMES_14E"] = {x = 23228.1973, y = -3984.44629, z = 29535.7227, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05F"] = {x = 18040.5195, y = -3988.92407, z = 23733.0664, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05E"] = {x = 18059.2754, y = -3976.78271, z = 24342.8477, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05D"] = {x = 18087.502, y = -3956.52563, z = 24945.8203, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05C"] = {x = 19490.3203, y = -3974.97852, z = 24949.4922, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05B"] = {x = 19446.5215, y = -3984.97852, z = 24327.9238, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_05A"] = {x = 19445.9805, y = -3983.62695, z = 23736.5117, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09F"] = {x = 19451.9863, y = -3975.14355, z = 30746.041, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09D"] = {x = 19408.1113, y = -3973.23779, z = 29547.8242, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09C"] = {x = 18044.8613, y = -3982.33032, z = 29549.3359, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09B"] = {x = 18068.7012, y = -3981.36572, z = 30765.8848, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09A"] = {x = 18068.8828, y = -3978.15381, z = 30146.3594, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADANOSTEMPEL_TREASUREPITS_09E"] = {x = 19402.8438, y = -3974.15381, z = 30152.0137, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADATMPL_ENTR_STONEGD_04"] = {x = 15437.8018, y = -3380.59082, z = 25220.3164, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADATMPL_ENTR_STONEGD_03"] = {x = 13641.2803, y = -3384.40918, z = 23998.1621, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADATMPL_ENTR_STONEGD_02"] = {x = 14220.7529, y = -3380.93286, z = 23383.2695, adjacent =
	[
	]},
	["FP_ROAM_ADW_ADATMPL_ENTR_STONEGD_01"] = {x = 15444.6904, y = -3388.51904, z = 23601.2207, adjacent =
	[
	]},
	["FP_PRAY_RAVEN"] = {x = 25401.5977, y = -3954.93042, z = 32772.4844, adjacent =
	[
	]},
	["FP_ITEM_BL_SNAF"] = {x = 31566.1973, y = -4451.02686, z = 10417.7188, adjacent =
	[
	]},
	["FP_ITEM_BL_CHEST"] = {x = 29205.0391, y = -4414.78711, z = 8508.94727, adjacent =
	[
	]},
	["FP_ITEM_BL_FISK_01"] = {x = 30752.0605, y = -4162.64404, z = 14770.7256, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_07"] = {x = 29287.8203, y = -4628.87646, z = -8329.11816, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_06"] = {x = 29336.7715, y = -4448.87646, z = -7892.26953, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_08"] = {x = 23233.8828, y = -4531.99902, z = -9415.91797, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_07"] = {x = 26268.3496, y = -4753.88867, z = -9344.62305, adjacent =
	[
	]},
	["FP_ROAM_DRONE_04"] = {x = 21411.7793, y = -4186.31055, z = -11156.8506, adjacent =
	[
	]},
	["FP_ROAM_DRONE_03"] = {x = 21366.2813, y = -4106.31055, z = -11745.3066, adjacent =
	[
	]},
	["FP_ROAM_DRONE_02"] = {x = 20665.5645, y = -4214.71484, z = -11177.7734, adjacent =
	[
	]},
	["FP_ROAM_DRONE_01"] = {x = 20779.957, y = -4154.71484, z = -11762.1895, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_06"] = {x = 17358.0527, y = -2354.96265, z = 13927.21, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_05"] = {x = 17013.5664, y = -2354.96265, z = 13731.3047, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_04"] = {x = 16732.4551, y = -2344.96265, z = 13389.5205, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_03"] = {x = 16907, y = -2334.96265, z = 12757.2373, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_02"] = {x = 17458.6113, y = -2344.96265, z = 12523.3594, adjacent =
	[
	]},
	["FP_ROAM_ZOBIE_01"] = {x = 18160.8828, y = -2353.56079, z = 12768.6416, adjacent =
	[
	]},
	["FP_ROAM_INSIDE_03"] = {x = 27528.8145, y = -2181.47461, z = -19094.2344, adjacent =
	[
	]},
	["FP_ROAM_INSIDE_02"] = {x = 26604.5039, y = -2163.80322, z = -17917.7559, adjacent =
	[
	]},
	["FP_ROAM_INSIDE_01"] = {x = 26019.5742, y = -2178.146, z = -18145.9824, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_20"] = {x = 19504.5762, y = -5217.67822, z = 2177.7937, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_19"] = {x = 16562.7188, y = -5173.82031, z = -2539.95068, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_18"] = {x = 23203.3008, y = -4534.08887, z = -9562.10352, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_17"] = {x = 23342.8262, y = -4539.08887, z = -9291.85156, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_16"] = {x = 25699.207, y = -4753.59814, z = -8616.31152, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_15"] = {x = 25829.3008, y = -4754.59814, z = -8567.54785, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_14"] = {x = 22124.416, y = -4919.80957, z = -4007.33325, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_13"] = {x = 23827.2168, y = -5159.74854, z = -1339.90076, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_12"] = {x = 26265.7656, y = -4771.87842, z = -8914.41797, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_11"] = {x = 25752.8242, y = -5162.2998, z = -471.358276, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_10"] = {x = 35727.0938, y = -5271.41455, z = -1642.32068, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_09"] = {x = 32463.9688, y = -4424.8877, z = -7292.52197, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_08"] = {x = 25275.4941, y = -3338.06201, z = -13985.415, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_07"] = {x = 18935.0645, y = -5259.44043, z = -1816.81873, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_06"] = {x = 19147.293, y = -4215.56445, z = -12542.0137, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_05"] = {x = 24323.3008, y = -4182.90723, z = -11649.0781, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_04"] = {x = 27038.3203, y = -2076.72266, z = -20357.1836, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_03"] = {x = 27917.8203, y = -2078.81128, z = -19940.4902, adjacent =
	[
	]},
	["FP_ROAM_SHARK_SPECIAL_03"] = {x = 14728.5928, y = -5050.97998, z = 9231.86426, adjacent =
	[
	]},
	["FP_ROAM_SHARK_SPECIAL_02"] = {x = 14583.957, y = -5050.97998, z = 9567.58789, adjacent =
	[
	]},
	["FP_ROAM_SHARK_SPECIAL_01"] = {x = 14792.4502, y = -5040.97998, z = 9951.63184, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_02"] = {x = 28890.3613, y = -3515.52197, z = 19642.3027, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_01"] = {x = 29124.4141, y = -3532.52197, z = 19773.3633, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_05"] = {x = 31375.9082, y = -4339.19678, z = -7851.09424, adjacent =
	[
	]},
	["FP_ROAM_SWAMPGOLEM_01"] = {x = 19499.7246, y = -5283.95557, z = -2994.62134, adjacent =
	[
	]},
	["FP_ROAM_HEILER"] = {x = 27140.2188, y = -2205.7666, z = -19453.334, adjacent =
	[
	]},
	["FP_ITEM_SENAT_CAVE_02"] = {x = 37781.875, y = -5044.42725, z = -6328.75732, adjacent =
	[
	]},
	["FP_ITEM_SENAT_CAVE_01"] = {x = 37973.0977, y = -5058.42725, z = -6525.11572, adjacent =
	[
	]},
	["FP_CAMPFIRE_CAVE_01"] = {x = 38265.1094, y = -5066.75732, z = -6250.98877, adjacent =
	[
	]},
	["FP_STAND_EINGANG"] = {x = 19014.8496, y = -5153.62842, z = 2840.33984, adjacent =
	[
	]},
	["FP_STAND_VP1_03"] = {x = 24767.1582, y = -5158.49805, z = -682.528198, adjacent =
	[
	]},
	["FP_CAMPFIRE_VP1_02"] = {x = 23504.8477, y = -5156.60498, z = -307.711304, adjacent =
	[
	]},
	["FP_SMALLTALK_HUNTER_04"] = {x = 25534.748, y = -4977.43555, z = 6853.52539, adjacent =
	[
	]},
	["FP_SMALLTALK_HUNTER_03"] = {x = 25448.3691, y = -4991.43555, z = 6912.229, adjacent =
	[
	]},
	["FP_ROAM_BF_NEST_27"] = {x = 34364.6992, y = -5208.72559, z = -2110.14722, adjacent =
	[
	]},
	["FP_ROAM_BF_NEST_26"] = {x = 33970.5, y = -5194.72559, z = -2562.31494, adjacent =
	[
	]},
	["FP_ROAM_BF_NEST_08"] = {x = 36720.6133, y = -5033.91162, z = -4961.14697, adjacent =
	[
	]},
	["FP_ROAM_BF_NEST_04"] = {x = 37252.2266, y = -5043.91162, z = -4129.52148, adjacent =
	[
	]},
	["FP_CAMPFIRE_SWAMP_01"] = {x = 18905.5742, y = -5147.20459, z = 3322.63232, adjacent =
	[
	]},
	["FP_ROAM_SHARK_34"] = {x = 16361.7344, y = -5247.52002, z = 2155.38599, adjacent =
	[
	]},
	["FP_ROAM_SHARK_33"] = {x = 16864.5781, y = -5257.52002, z = 2835.38794, adjacent =
	[
	]},
	["FP_ROAM_SHARK_32"] = {x = 16388.6738, y = -5287.52002, z = 3369.21899, adjacent =
	[
	]},
	["FP_ROAM_SHARK_31"] = {x = 16680.8965, y = -5257.52002, z = 4336.40625, adjacent =
	[
	]},
	["FP_ROAM_SHARK_30"] = {x = 14534.1436, y = -5250.09229, z = 4716.70898, adjacent =
	[
	]},
	["FP_ROAM_SHARK_29"] = {x = 14208.3076, y = -5250.09229, z = 5235.53076, adjacent =
	[
	]},
	["FP_ROAM_SHARK_28"] = {x = 13507.4902, y = -5260.09229, z = 4944.59668, adjacent =
	[
	]},
	["FP_ROAM_SHARK_27"] = {x = 13262.4512, y = -5270.09229, z = 5774.50146, adjacent =
	[
	]},
	["FP_ROAM_SHARK_26"] = {x = 12576.7451, y = -5280.57568, z = 5963.00439, adjacent =
	[
	]},
	["FP_ROAM_SHARK_25"] = {x = 12079.4473, y = -5275.68506, z = 6555.74512, adjacent =
	[
	]},
	["FP_ROAM_SHARK_24"] = {x = 11364.1904, y = -5266.32568, z = 6490.79785, adjacent =
	[
	]},
	["FP_ROAM_SHARK_23"] = {x = 10609.2197, y = -5269.16992, z = 6681.24854, adjacent =
	[
	]},
	["FP_ROAM_SHARK_22"] = {x = 10408.6084, y = -5239.16992, z = 7290.34717, adjacent =
	[
	]},
	["FP_STAND_FRANCO_01"] = {x = 24591.5195, y = -5120.5791, z = 4834.59717, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_24"] = {x = 28147.7598, y = -5233.62207, z = -1473.54602, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_23"] = {x = 27558.4004, y = -5260.21387, z = -804.032227, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_22"] = {x = 27324.3301, y = -5260.89502, z = -3271.31128, adjacent =
	[
	]},
	["FP_ROAM_SHARK_21"] = {x = 27663.3594, y = -5166.33008, z = -2461.96655, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_04"] = {x = 24567.0273, y = -4738.06104, z = -6132.0376, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_03"] = {x = 25370.8027, y = -4759.30908, z = -6196.02881, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_02"] = {x = 23672.4043, y = -4765.25586, z = -5512.86865, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_01"] = {x = 23583.3262, y = -4813.49121, z = -4711.47559, adjacent =
	[
	]},
	["FP_ROAM_RUDEL_25"] = {x = 19808.9648, y = -3186.12866, z = -28595.4688, adjacent =
	[
	]},
	["FP_ROAM_WAY_03"] = {x = 19579.7109, y = -3369.22559, z = -13168.1152, adjacent =
	[
	]},
	["FP_ROAM_SHARK_20"] = {x = 8822.32422, y = -5239.76709, z = 7989.65576, adjacent =
	[
	]},
	["FP_ROAM_SHARK_05"] = {x = 8249.16895, y = -5272.76709, z = 8311.17285, adjacent =
	[
	]},
	["FP_ROAM_SHARK_19"] = {x = 28838.3359, y = -5281.28369, z = -2077.62964, adjacent =
	[
	]},
	["FP_ROAM_SHARK_18"] = {x = 28911.6523, y = -5266.28369, z = -2714.16016, adjacent =
	[
	]},
	["FP_ROAM_SHARK_17"] = {x = 29281.7324, y = -5271.28369, z = -1119.22827, adjacent =
	[
	]},
	["FP_ROAM_SHARK_16"] = {x = 29902.4668, y = -5271.28369, z = -721.662659, adjacent =
	[
	]},
	["FP_ROAM_SHARK_15"] = {x = 30680.666, y = -5182.05957, z = -2363.87427, adjacent =
	[
	]},
	["FP_ROAM_SHARK_14"] = {x = 34664.6133, y = -5261.02637, z = -4689.85254, adjacent =
	[
	]},
	["FP_ROAM_SHARK_13"] = {x = 34042.1445, y = -5246.02637, z = -4095.97827, adjacent =
	[
	]},
	["FP_ROAM_SHARK_12"] = {x = 30990.4199, y = -5221.02637, z = -3179.02954, adjacent =
	[
	]},
	["FP_ROAM_SHARK_11"] = {x = 33780.5195, y = -5271.02637, z = -4707.90723, adjacent =
	[
	]},
	["FP_ROAM_SHARK_10"] = {x = 31907.877, y = -5241.02637, z = -3924.271, adjacent =
	[
	]},
	["FP_ROAM_SHARK_09"] = {x = 32268.9648, y = -5251.02637, z = -2280.26343, adjacent =
	[
	]},
	["FP_ROAM_SHARK_08"] = {x = 32222.5898, y = -5251.02637, z = -3032.19678, adjacent =
	[
	]},
	["FP_ROAM_SHARK_07"] = {x = 32846.5156, y = -5281.02637, z = -3881.95068, adjacent =
	[
	]},
	["FP_ROAM_SHARK_06"] = {x = 35074.1094, y = -5241.02637, z = -3745.57788, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_ENTRANCE_01"] = {x = 27456.0723, y = -4578.81104, z = 7724.49658, adjacent =
	[
	]},
	["FP_BL_ITEM_SMITH_01"] = {x = 24933.3535, y = -3842.33179, z = 13456.791, adjacent =
	[
	]},
	["FP_BL_ITEM_SMITH_02"] = {x = 25801.0957, y = -3942.13232, z = 11982.3945, adjacent =
	[
	]},
	["FP_BL_ITEM_SMITH_BACK_01"] = {x = 25477.4492, y = -4016.37012, z = 11628.5537, adjacent =
	[
	]},
	["FP_STAND_UP_02"] = {x = 29233.6348, y = -4036.77466, z = 12907.9209, adjacent =
	[
	]},
	["FP_STAND_UP_01"] = {x = 29136.9473, y = -4026.77466, z = 13025.6416, adjacent =
	[
	]},
	["FP_SMALLTALK_UP_02"] = {x = 28918.1895, y = -4016.19678, z = 12854.2168, adjacent =
	[
	]},
	["FP_SMALLTALK_UP_01"] = {x = 28946.2402, y = -4026.48584, z = 12757.0176, adjacent =
	[
	]},
	["FP_PEE_BL_02"] = {x = 28809.8906, y = -4030.08838, z = 13349.2158, adjacent =
	[
	]},
	["FP_STAND_BL_RING_03"] = {x = 28557.2324, y = -4512.38184, z = 10579.0273, adjacent =
	[
	]},
	["FP_SMALLTALK_RING_02"] = {x = 28699.6563, y = -4489.46436, z = 9993.7627, adjacent =
	[
	]},
	["FP_SMALLTALK_RING_01"] = {x = 28779.4082, y = -4512.34766, z = 9943.90137, adjacent =
	[
	]},
	["FP_STAND_BL_RING_02"] = {x = 28944.9297, y = -4508.81689, z = 10037.0811, adjacent =
	[
	]},
	["FP_STAND_BL_RING_01"] = {x = 28992.0957, y = -4519.58594, z = 10507.3262, adjacent =
	[
	]},
	["FP_ITEM_BL_MERCHANT_01"] = {x = 30395.4609, y = -4269.87109, z = 15758.0898, adjacent =
	[
	]},
	["FP_PICK_MERCHANT_03"] = {x = 31141.0527, y = -4259.87109, z = 15409.5674, adjacent =
	[
	]},
	["FP_PICK_MERCHANT_02"] = {x = 30792.6895, y = -4260.87109, z = 15538.9668, adjacent =
	[
	]},
	["FP_PICK_MERCHANT_01"] = {x = 30939.6855, y = -4254.87109, z = 15269.9648, adjacent =
	[
	]},
	["FP_PEE_BL_YARD_01"] = {x = 31448.0684, y = -4257.87109, z = 15188.2061, adjacent =
	[
	]},
	["FP_SMALLTALK_BL_MID_04"] = {x = 28910.9355, y = -4258.72705, z = 11669.0713, adjacent =
	[
	]},
	["FP_ITEM_BL_TRYSTAN"] = {x = 30521.3613, y = -4231.41016, z = 11801.1836, adjacent =
	[
	]},
	["FP_ITEM_BL_STAIRS_01"] = {x = 26916.6348, y = -3822.19043, z = 14245.3672, adjacent =
	[
	]},
	["FP_ITEM_BL_STAIRS_02"] = {x = 27632.459, y = -3438.12891, z = 14223.166, adjacent =
	[
	]},
	["FP_ITEM_BL_STAIRS_03"] = {x = 28861.4043, y = -3437.12891, z = 15171.6494, adjacent =
	[
	]},
	["FP_ITEM_BL_STAIRS_04"] = {x = 29604.7051, y = -3852.74878, z = 15108.585, adjacent =
	[
	]},
	["FP_ITEM_BL_STAIRS_05"] = {x = 29976.5664, y = -3848.74878, z = 14212.9551, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_MERCHANT_01"] = {x = 31210.8926, y = -4273.77197, z = 13540.1436, adjacent =
	[
	]},
	["FP_STAND_BL_MID_01"] = {x = 30364.5723, y = -4244.75488, z = 12225.7363, adjacent =
	[
	]},
	["FP_SMALLTALK_BL_MID_03"] = {x = 29015.5234, y = -4249.02441, z = 11702.3242, adjacent =
	[
	]},
	["FP_STAND_WACHE_02"] = {x = 30447.123, y = -4277.71533, z = 11897.9463, adjacent =
	[
	]},
	["FP_STAND_WACHE_01"] = {x = 30272.8555, y = -4191.3252, z = 12535.9873, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_MERCHANT_02"] = {x = 31121.0801, y = -4283.44775, z = 13330.542, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_01"] = {x = 28122.0723, y = -4252.03516, z = 11270.9814, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_03"] = {x = 29417.5293, y = -4484.91943, z = 9305.27832, adjacent =
	[
	]},
	["FP_PEE_BL_01"] = {x = 29951.9844, y = -4539.70215, z = 11334.7158, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_04"] = {x = 29230.2539, y = -4514.31689, z = 8680.85547, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BL_05"] = {x = 28513.3906, y = -4016.76587, z = 12284.4131, adjacent =
	[
	]},
	["FP_SWEEP_BL_MERCHANT"] = {x = 31129.0566, y = -4255.54834, z = 14368.7617, adjacent =
	[
	]},
	["FP_STAND_BL_INN_01"] = {x = 30315.6113, y = -4540.6958, z = 9277.43652, adjacent =
	[
	]},
	["FP_STAND_BL_INN_02"] = {x = 30517.502, y = -4536.6958, z = 9260.1748, adjacent =
	[
	]},
	["FP_SWEEP_BL_INN_01"] = {x = 30886.5918, y = -4546.6958, z = 10350.0732, adjacent =
	[
	]},
	["FP_SMALLTALK_UP_03"] = {x = 27573.0469, y = -4007.58667, z = 12081.8691, adjacent =
	[
	]},
	["FP_SMALLTALK_UP_04"] = {x = 27667.2227, y = -4007.58667, z = 12152.0811, adjacent =
	[
	]},
	["FP_SWEEP_INN_01"] = {x = 31350.1816, y = -4559.0752, z = 9867.80176, adjacent =
	[
	]},
	["FP_SMALLTALK_INN_01"] = {x = 31414.0547, y = -4097.23926, z = 10005.2041, adjacent =
	[
	]},
	["FP_SMALLTALK_INN_02"] = {x = 31308.9922, y = -4107.23926, z = 9999.1123, adjacent =
	[
	]},
	["FP_RAVEN_01"] = {x = 17468.3926, y = -2187.65771, z = 13067.4961, adjacent =
	[
	]},
	["FP_RAVEN_02"] = {x = 17373.2793, y = -2182.65771, z = 13227.3525, adjacent =
	[
	]},
	["FP_STAND_SCATTY"] = {x = 28985.8418, y = -3502.11816, z = 17870.7148, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_01"] = {x = 21869.1426, y = -5232.25342, z = 10295.5439, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_02"] = {x = 22115.8223, y = -5182.25342, z = 10819.7451, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_03"] = {x = 21372.9277, y = -5272.25342, z = 10076.0283, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_04"] = {x = 22531.0586, y = -5272.25342, z = 9718.11816, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_05"] = {x = 22314.0586, y = -5232.25342, z = 10245.6582, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_06"] = {x = 21771.5801, y = -5222.25342, z = 11493.2383, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_07"] = {x = 22591.1875, y = -5162.25342, z = 10626.4209, adjacent =
	[
	]},
	["FP_ROAM_BL_FLIES_08"] = {x = 23141.4902, y = -5197.25342, z = 10790.001, adjacent =
	[
	]},
	["FP_ROAM_SHARK_01"] = {x = 10219.5684, y = -5138.28906, z = 9467.57715, adjacent =
	[
	]},
	["FP_ROAM_SHARK_02"] = {x = 12096.8701, y = -5218.28906, z = 10812.3867, adjacent =
	[
	]},
	["FP_ROAM_SHARK_03"] = {x = 12627.5527, y = -5168.28906, z = 11621.7305, adjacent =
	[
	]},
	["FP_ROAM_SHARK_04"] = {x = 10165.0684, y = -5166.375, z = 8502.76855, adjacent =
	[
	]},
	["FP_ROAM_SWAMPFLIES_01"] = {x = 28506.7832, y = -4276.62207, z = -5397.63135, adjacent =
	[
	]},
	["FP_ROAM_SWAMPFLIES_02"] = {x = 29120.0957, y = -4359.27881, z = -5269.81885, adjacent =
	[
	]},
	["FP_ROAM_SWAMPFLIES_03"] = {x = 28853.6797, y = -4344.79004, z = -5209.91211, adjacent =
	[
	]},
	["FP_ROAM_SEA_01"] = {x = 26310.0352, y = -4795.65283, z = -9702.77832, adjacent =
	[
	]},
	["FP_ROAM_SEA_02"] = {x = 26101.7246, y = -4788.94385, z = -9861.2627, adjacent =
	[
	]},
	["FP_ROAM_SEA_03"] = {x = 26391.6367, y = -4656.5791, z = -11250.1465, adjacent =
	[
	]},
	["FP_ROAM_SEA_04"] = {x = 26824.3457, y = -4308.01367, z = -12002.5625, adjacent =
	[
	]},
	["FP_ROAM_SEA_05"] = {x = 26060.6191, y = -4282.23682, z = -11880.8037, adjacent =
	[
	]},
	["FP_ROAM_SEA_06"] = {x = 25523.2344, y = -4588.47119, z = -11042.0625, adjacent =
	[
	]},
	["FP_ROAM_SEA_07"] = {x = 25547.0957, y = -4625.56738, z = -10383.7422, adjacent =
	[
	]},
	["FP_ROAM_SEA_08"] = {x = 25426.1973, y = -4660.45752, z = -9674.48926, adjacent =
	[
	]},
	["FP_ROAM_SEA_09"] = {x = 27238.791, y = -4643.66357, z = -11111.7451, adjacent =
	[
	]},
	["FP_ROAM_SEA_10"] = {x = 28407.541, y = -4506.97998, z = -11367.8887, adjacent =
	[
	]},
	["FP_ROAM_SEA_11"] = {x = 27716.2598, y = -4524.38379, z = -11812.6768, adjacent =
	[
	]},
	["FP_ROAM_SEA_12"] = {x = 27919.9414, y = -4790.05811, z = -10586.9717, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_01"] = {x = 12915.3828, y = -5182.24414, z = -7871.93701, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_02"] = {x = 14637.2285, y = -5235.50732, z = -8385.00391, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_03"] = {x = 14212.5107, y = -5187.24414, z = -8981.52734, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_04"] = {x = 13798.042, y = -5263.85938, z = -8017.82178, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_05"] = {x = 13602.6543, y = -5203.85938, z = -6597.23535, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_06"] = {x = 13740.7891, y = -5273.85938, z = -7314.80811, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_07"] = {x = 14396.3701, y = -5213.85938, z = -7674.94971, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_08"] = {x = 14554.6953, y = -5253.85938, z = -6928.12988, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_09"] = {x = 16723.5742, y = -5039.52197, z = -10711.252, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_10"] = {x = 16204.9561, y = -5171.86426, z = -10313.9082, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_11"] = {x = 17377.084, y = -4969.84375, z = -9065.58887, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_12"] = {x = 17423.9414, y = -4865.11768, z = -9987.52051, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_13"] = {x = 15440.3379, y = -5138.21387, z = -9392.64355, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_14"] = {x = 16664.4395, y = -4999.52197, z = -8801.53613, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_15"] = {x = 15091.3398, y = -5159.09473, z = -8872.33105, adjacent =
	[
	]},
	["FP_ROAM_PFUETZE_17"] = {x = 16952.1328, y = -5012.18115, z = -9521.5625, adjacent =
	[
	]},
	["FP_ROAM_PATH_01"] = {x = 14360.6025, y = -4826.85889, z = -1898.65735, adjacent =
	[
	]},
	["FP_ROAM_PATH_02"] = {x = 13932.4844, y = -4789.3833, z = -2611.1875, adjacent =
	[
	]},
	["FP_ROAM_PATH_03"] = {x = 13422.3076, y = -4687.04248, z = -1415.80884, adjacent =
	[
	]},
	["FP_ROAM_PATH_04"] = {x = 15477.9033, y = -4967.24463, z = -728.368103, adjacent =
	[
	]},
	["FP_ROAM_PATH_05"] = {x = 13816.9434, y = -4750.01904, z = -1026.99255, adjacent =
	[
	]},
	["FP_ROAM_PATH_06"] = {x = 14158.2363, y = -4763.26416, z = -1217.68066, adjacent =
	[
	]},
	["FP_ROAM_PATH_07"] = {x = 13736.3623, y = -4767.77197, z = -1841.56006, adjacent =
	[
	]},
	["FP_ROAM_PATH_08"] = {x = 14578.9834, y = -4937.35791, z = -2378.37793, adjacent =
	[
	]},
	["FP_ROAM_WARAN_01"] = {x = 32648.8027, y = -4680.90283, z = -5907.5957, adjacent =
	[
	]},
	["FP_ROAM_DANGER_26"] = {x = 10777.9434, y = -5210.41699, z = 12611.7988, adjacent =
	[
	]},
	["FP_ROAM_DANGER_27"] = {x = 10078.4951, y = -5245.56836, z = 12390.9189, adjacent =
	[
	]},
	["FP_ROAM_DANGER_28"] = {x = 10698.5244, y = -5195.56836, z = 12222.6309, adjacent =
	[
	]},
	["FP_ROAM_DANGER_29"] = {x = 10227.1865, y = -5249.90527, z = 12775.8291, adjacent =
	[
	]},
	["FP_CAMPFIRE_VP1_01"] = {x = 23635.8145, y = -5111.9917, z = -47.8863983, adjacent =
	[
	]},
	["FP_STAND_VP1_01"] = {x = 23246.0586, y = -5150.9917, z = -1073.54224, adjacent =
	[
	]},
	["FP_STAND_VP1_02"] = {x = 24095.8008, y = -5140.9917, z = -791.805298, adjacent =
	[
	]},
	["FP_ITEM_SWAMP_04"] = {x = 29058.4883, y = -4216.17676, z = -5573.4043, adjacent =
	[
	]},
	["FP_ITEM_SWAMP_06"] = {x = 26135.252, y = -5120.19727, z = -1128.75098, adjacent =
	[
	]},
	["FP_ITEM_SWAMP_07"] = {x = 24883.0469, y = -5095.20898, z = -254.578812, adjacent =
	[
	]},
	["FP_STAND_LOGAN"] = {x = 16377.3643, y = -5041.55566, z = 9512.39355, adjacent =
	[
	]},
	["FP_SMALLTALK_HUNTER_01"] = {x = 25214.9629, y = -5052.96875, z = 5066.36914, adjacent =
	[
	]},
	["FP_SMALLTALK_HUNTER_02"] = {x = 25199.1094, y = -5042.96875, z = 5179.73486, adjacent =
	[
	]},
	["FP_STAND_BANDIT_01"] = {x = 24630.8027, y = -5146.52246, z = 4498.21582, adjacent =
	[
	]},
	["FP_STAND_FRANCO_02"] = {x = 25233.6641, y = -5010.58154, z = 6399.90869, adjacent =
	[
	]},
	["FP_CAMPFIRE_HOEHLE_01"] = {x = 18050.998, y = -4949.09668, z = 12670.165, adjacent =
	[
	]},
	["FP_ITEM_MINE_01"] = {x = 29244.5547, y = -2979.44995, z = 18199.4355, adjacent =
	[
	]},
	["FP_ROAM_SENAT_01"] = {x = 27587.0078, y = -2877.19824, z = -16095.666, adjacent =
	[
	]},
	["FP_ROAM_SENAT_02"] = {x = 27472.2715, y = -2837.19824, z = -15771.3018, adjacent =
	[
	]},
	["FP_ROAM_SENAT_03"] = {x = 27716.8828, y = -2877.19824, z = -15701.7813, adjacent =
	[
	]},
	["FP_ROAM_SENAT_04"] = {x = 28024.5527, y = -2868.19824, z = -16020.5908, adjacent =
	[
	]},
	["FP_ROAM_SENAT_05"] = {x = 27254.8945, y = -2867.19824, z = -14802.5381, adjacent =
	[
	]},
	["FP_ROAM_SENAT_06"] = {x = 27295.8086, y = -2897.19824, z = -15136.3506, adjacent =
	[
	]},
	["FP_ROAM_SENAT_07"] = {x = 26868.1816, y = -2867.19824, z = -14696.3594, adjacent =
	[
	]},
	["FP_ROAM_SENAT_08"] = {x = 26845.4355, y = -2877.19824, z = -15103.8242, adjacent =
	[
	]},
	["FP_ROAM_SENAT_09"] = {x = 25526.5449, y = -2853.28418, z = -15115.3418, adjacent =
	[
	]},
	["FP_ROAM_SENAT_10"] = {x = 26409.9375, y = -2853.28418, z = -14737.7178, adjacent =
	[
	]},
	["FP_ROAM_SENAT_11"] = {x = 26029.3145, y = -2853.28418, z = -14859.8154, adjacent =
	[
	]},
	["FP_ROAM_SENAT_12"] = {x = 26471.6523, y = -2802.28418, z = -15080.1797, adjacent =
	[
	]},
	["FP_ROAM_SENAT_13"] = {x = 26642.7734, y = -2208.4541, z = -16691.5254, adjacent =
	[
	]},
	["FP_ROAM_SENAT_15"] = {x = 25180.3926, y = -2208.4541, z = -17436.2266, adjacent =
	[
	]},
	["FP_ROAM_SENAT_16"] = {x = 25398.6035, y = -2208.4541, z = -16792.8027, adjacent =
	[
	]},
	["FP_ITEM_SENAT_01"] = {x = 27341.4316, y = -2423.88135, z = -17021.9707, adjacent =
	[
	]},
	["FP_ITEM_SENAT_02"] = {x = 24711.0918, y = -2442.88135, z = -16694.8945, adjacent =
	[
	]},
	["FP_ITEM_SENAT_03"] = {x = 26313.6895, y = -2209.88135, z = -16302.9961, adjacent =
	[
	]},
	["FP_ITEM_SENAT_04"] = {x = 26745.2969, y = -2437.88135, z = -15619.4941, adjacent =
	[
	]},
	["FP_ITEM_SENAT_05"] = {x = 27159.9824, y = -2639.88135, z = -16191.5, adjacent =
	[
	]},
	["FP_ROAM_HILL_01"] = {x = 22827.957, y = -4088.63574, z = -11469.2822, adjacent =
	[
	]},
	["FP_ROAM_HILL_02"] = {x = 22670.4414, y = -4188.63574, z = -11031.1963, adjacent =
	[
	]},
	["FP_ROAM_HILL_03"] = {x = 23085.4785, y = -4148.63574, z = -11222.7646, adjacent =
	[
	]},
	["FP_ROAM_HILL_04"] = {x = 22513.4258, y = -4118.63574, z = -11394.5166, adjacent =
	[
	]},
	["FP_ROAM_HILL_05"] = {x = 23769.0938, y = -3993.93677, z = -12128.9023, adjacent =
	[
	]},
	["FP_ROAM_HILL_06"] = {x = 23363.5547, y = -4003.93677, z = -12093.6738, adjacent =
	[
	]},
	["FP_ROAM_HILL_07"] = {x = 23466.1426, y = -3963.93677, z = -12448.9834, adjacent =
	[
	]},
	["FP_ROAM_HILL_08"] = {x = 23868.8613, y = -3983.93677, z = -12462.1855, adjacent =
	[
	]},
	["FP_ROAM_HILL_09"] = {x = 26981.8945, y = -4084.70703, z = -12996.3955, adjacent =
	[
	]},
	["FP_ROAM_HILL_10"] = {x = 26390.3555, y = -4164.70703, z = -12839.0615, adjacent =
	[
	]},
	["FP_ROAM_HILL_11"] = {x = 26932.8223, y = -4144.70703, z = -12714.3789, adjacent =
	[
	]},
	["FP_ROAM_HILL_12"] = {x = 26501.5664, y = -4094.70703, z = -13126.749, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_01"] = {x = 32696.9707, y = -4583.97803, z = -6182.48047, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_02"] = {x = 30875.7285, y = -4383.97803, z = -7488.8418, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_03"] = {x = 33214.5781, y = -4673.97803, z = -6464.08789, adjacent =
	[
	]},
	["FP_ROAM_HOHLWEG_04"] = {x = 33005.0117, y = -4583.97803, z = -6742.34473, adjacent =
	[
	]},
	["FP_ROAM_HILL_13"] = {x = 24737.4082, y = -4660.10889, z = -7175.12207, adjacent =
	[
	]},
	["FP_ROAM_HILL_14"] = {x = 25168.9824, y = -4689.91553, z = -7645.78467, adjacent =
	[
	]},
	["FP_ROAM_HILL_15"] = {x = 25380.7227, y = -4690.10889, z = -7279.72021, adjacent =
	[
	]},
	["FP_ROAM_HILL_16"] = {x = 24812.3438, y = -4640.10889, z = -7669.99756, adjacent =
	[
	]},
	["FP_PICK_SWAMP_01"] = {x = 21367.0156, y = -5109.97949, z = 7013.79932, adjacent =
	[
	]},
	["FP_PICK_SWAMP_02"] = {x = 22133.0762, y = -5139.97949, z = 6526.11035, adjacent =
	[
	]},
	["FP_PICK_SWAMP_03"] = {x = 21383.1211, y = -5139.97949, z = 5783.90039, adjacent =
	[
	]},
	["FP_ROAM_SWAMPGOLEM_02"] = {x = 18036.1953, y = -5282.55566, z = -5192.48828, adjacent =
	[
	]},
	["FP_ROAM_SWAMPGOLEM_03"] = {x = 17557.4512, y = -5277.99902, z = -3461.42041, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOLEM_01"] = {x = 34235.7422, y = -5261.25342, z = -833.403442, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOLEM_02"] = {x = 35314.9297, y = -5281.25342, z = -940.910828, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOLEM_03"] = {x = 35882.8789, y = -5281.25342, z = -901.901855, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_01"] = {x = 22059.2461, y = -4677.38965, z = -5937.54639, adjacent =
	[
	]},
	["FP_ITEM_BANDITSCAMP_02"] = {x = 31627.293, y = -4454.9707, z = -6334.31641, adjacent =
	[
	]},
	["FP_ROAM_GOLEM_01"] = {x = 28673.2109, y = -5287.32373, z = 125.092773, adjacent =
	[
	]},
	["FP_ROAM_BF_01"] = {x = 28195.4492, y = -5283.32373, z = 190.152832, adjacent =
	[
	]},
	["FP_ROAM_BF_02"] = {x = 27846.5234, y = -5285.32373, z = 423.812561, adjacent =
	[
	]},
	["FP_ROAM_BF_03"] = {x = 27603.9863, y = -5265.32373, z = -419.692535, adjacent =
	[
	]},
	["FP_ROAM_BF_04"] = {x = 27383.1445, y = -5285.32373, z = -30.8253098, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_01"] = {x = 25309.6621, y = -5105.78809, z = -1494.50574, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_02"] = {x = 26071.9941, y = -5095.78809, z = -1411.60095, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_03"] = {x = 25705.7129, y = -5085.78809, z = -1512.19885, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_04"] = {x = 21027.6016, y = -5242.03223, z = 1478.56152, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_05"] = {x = 25354.9121, y = -4718.02295, z = -7917.63184, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_GOBBO_06"] = {x = 25106.3457, y = -4719.02295, z = -8065.51318, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE2_09_04"] = {x = 3212.11279, y = -3367.11011, z = 30592.7461, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE2_09_03"] = {x = 4185.28125, y = -3347.11011, z = 30896.4648, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE2_09_02"] = {x = 3727.99561, y = -3352.14307, z = 30388.8047, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE2_09_01"] = {x = 3582.60938, y = -3357.11011, z = 30816.6816, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_06_03"] = {x = -15405.4248, y = -1798.90735, z = 15894.1084, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_06_02"] = {x = -14485.4756, y = -1839.53357, z = 15684.0576, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_06_01"] = {x = -15151.4209, y = -1821.53357, z = 15396.9424, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_12_03"] = {x = -16689.2285, y = -1653.5863, z = 12734.5586, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_12_02"] = {x = -16357.5225, y = -1656.59851, z = 12304.3828, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_12_01"] = {x = -16631.7852, y = -1628.90161, z = 11896.3838, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_14_03"] = {x = -14881.5537, y = -1869.40088, z = 13289.2793, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_14_02"] = {x = -14779.6113, y = -1934.06982, z = 12793.2354, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_MINE1_14_01"] = {x = -14551.8262, y = -1935.46265, z = 12251.002, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_27"] = {x = 1671.26147, y = -2612.87549, z = 10973.6787, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_26"] = {x = 1993.42188, y = -2618.61182, z = 10950.3564, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_25"] = {x = 2333.66016, y = -2627.84424, z = 10917.9189, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_24"] = {x = 2841.94946, y = -2648.57739, z = 11200.2891, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_23"] = {x = -2906.1084, y = -2572.38989, z = 13668.1484, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_22"] = {x = -2437.78931, y = -2604.5603, z = 14094.2207, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_21"] = {x = -2305.02637, y = -2649.19873, z = 13532.043, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_20"] = {x = 5241.61035, y = -2983.92383, z = 14255.8672, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_19"] = {x = 4441.51074, y = -2985.97192, z = 13562.5195, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_18"] = {x = 5336.45117, y = -2939.75659, z = 13539.7139, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_17"] = {x = 4824.32764, y = -3005.70752, z = 14519.3086, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_16"] = {x = 4255.48584, y = -2968.54297, z = 14191.1201, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_15"] = {x = 1409.40198, y = -2805.70776, z = 15956.3457, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_14"] = {x = 2009.75476, y = -2661.14429, z = 16224.5664, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_13"] = {x = 2741.46558, y = -2555.32983, z = 16032.5498, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_12"] = {x = 2275.72388, y = -2584.80981, z = 15749.9971, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_11"] = {x = 2980.75806, y = -2539.34546, z = 15435.4512, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_10"] = {x = 2573.29834, y = -2566.91748, z = 14961.043, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_09"] = {x = -1505.20654, y = -2739.67285, z = 15274.2275, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_08"] = {x = -518.054199, y = -2885.95044, z = 14698.4854, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_07"] = {x = -699.345581, y = -2865.89429, z = 15052.0547, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_06"] = {x = -385.01828, y = -2854.24292, z = 15581.5615, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_05"] = {x = -815.359741, y = -2844.12134, z = 15409.0625, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_04"] = {x = -1036.31519, y = -2767.50952, z = 15827.7373, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_03"] = {x = -4005.38623, y = -2194.03857, z = 15870.7764, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_02"] = {x = -3442.52393, y = -2357.10107, z = 15283.9531, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_01"] = {x = -3426.31104, y = -2260.36597, z = 15821.9141, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_43B"] = {x = -17508.7773, y = -3003.53564, z = 20018.9512, adjacent =
	[
	]},
	["ADW_ITEM_CANYON_TOKEN_01"] = {x = -4953.55908, y = -3071.97168, z = 36475.9609, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_01"] = {x = -10428.0469, y = -4080.47314, z = 32157.4629, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_02"] = {x = -10015.373, y = -4043.81982, z = 32566.9824, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_03"] = {x = -10676.3926, y = -4043.40576, z = 32688.9785, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_04"] = {x = -8755.77051, y = -3528.72388, z = 32655.1973, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_05"] = {x = -9006.65234, y = -3699.72388, z = 31769.6836, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_06"] = {x = -8015.10791, y = -3220.23633, z = 32016.5957, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_07"] = {x = -8016.41162, y = -3190.23633, z = 32562.9668, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_08"] = {x = -10530.8848, y = -3835.39404, z = 34132.7109, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_09"] = {x = -10526.9844, y = -3925.45239, z = 33673.8984, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_10"] = {x = -9938.93457, y = -3722.22388, z = 33999.9336, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_11"] = {x = -13579.9717, y = -4169.41602, z = 33542.4336, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_12"] = {x = -13795.9141, y = -4235.11084, z = 34116.8477, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_13"] = {x = -14050.7256, y = -4351.52295, z = 33831.5508, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_14"] = {x = -13964.8164, y = -4281.53955, z = 32525.0234, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_15"] = {x = -13514.4111, y = -4134.69482, z = 32443.6152, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_16"] = {x = -13848.6348, y = -4170.93604, z = 32138.0547, adjacent =
	[
	]},
	["FP_CAMPFIRE_ADW_CANYON_ORCS_17"] = {x = -12550.2793, y = -4006.39771, z = 31378.7813, adjacent =
	[
	]},
	["FP_CAMPFIRE_ADW_CANYON_ORCS_18"] = {x = -12390.5635, y = -3962.12939, z = 31865.8516, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_19"] = {x = -12582.9141, y = -3997.13501, z = 31612.6563, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_20"] = {x = -12391.7168, y = -3925.87866, z = 33062.1367, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_ORCS_21"] = {x = -12063.5371, y = -3846.65015, z = 33117.1836, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_01"] = {x = -12881.4248, y = -4153.97119, z = 28593, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_02"] = {x = -12672.376, y = -4156.38037, z = 28254.8672, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_03"] = {x = -12431.8447, y = -4170.07227, z = 28623.8984, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_04"] = {x = -11932.4912, y = -4089.7085, z = 26190.9512, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_05"] = {x = -12257.3193, y = -4004.0625, z = 26176.3711, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_06"] = {x = -12216.0869, y = -4030.13379, z = 26620.1211, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_07"] = {x = -10968.5918, y = -4142.87256, z = 23584.6289, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_08"] = {x = -11677.1777, y = -4131.30908, z = 23663.8594, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_09"] = {x = -11252.1504, y = -4256.73535, z = 24291.0215, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_10"] = {x = -13710.7676, y = -3918.1001, z = 21644.5566, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_11"] = {x = -13176.1777, y = -3953.64941, z = 22106.7344, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_12"] = {x = -13451.127, y = -3954.72583, z = 22115.1582, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_13"] = {x = -15921.7783, y = -3679.84375, z = 24407.3711, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_14"] = {x = -14994.7246, y = -3662.1416, z = 24571.4512, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_15"] = {x = -15666.7178, y = -3706.77417, z = 24219.3887, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_16"] = {x = -15367.7178, y = -3689.54272, z = 24388.2988, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_17"] = {x = -15559.4893, y = -3506.93433, z = 24967.5098, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_18"] = {x = -18886.9727, y = -3338.45459, z = 21675.0117, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_19"] = {x = -18968.1836, y = -3325.52686, z = 22250.5156, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_20"] = {x = -18365.0703, y = -3312.35571, z = 21707.8027, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_21"] = {x = -18521.0508, y = -3325.56421, z = 21961.1191, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_22"] = {x = -18251.2461, y = -2533.24316, z = 17364.4785, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_23"] = {x = -17886.0137, y = -2526.66187, z = 17662.3633, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_24"] = {x = -18387.1738, y = -2577.05811, z = 17749.752, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_25"] = {x = -22298.916, y = -1235.08887, z = 15990.5146, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_26"] = {x = -21791.7969, y = -1294.21228, z = 15871.9951, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_27"] = {x = -21792.8047, y = -1277.49805, z = 16439.666, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_28"] = {x = -22617.5684, y = -1133.65125, z = 18240.6641, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_29"] = {x = -22999.5234, y = -1107.23755, z = 18051.6445, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_30"] = {x = -22458.6777, y = -1139.40833, z = 17828.9922, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_31"] = {x = -24030.7422, y = -1109.25806, z = 18306.5176, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_32"] = {x = -24205.9609, y = -1096.5459, z = 17997.252, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_33"] = {x = -23852.0977, y = -1068.24219, z = 17837.9063, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_34"] = {x = -24769.4473, y = -947.078308, z = 15076.2002, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_35"] = {x = -24756.9004, y = -898.857788, z = 14735.1709, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_36"] = {x = -24512.1758, y = -912.689758, z = 15114.8926, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_37"] = {x = -26340.3848, y = -876.322754, z = 14712.1875, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_38"] = {x = -25792.3965, y = -857.579529, z = 14656.8242, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_39"] = {x = -26056.4902, y = -870.538391, z = 14481.1709, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_40"] = {x = -25801.0996, y = -840.221924, z = 14310.8428, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_41"] = {x = -17407.3145, y = -3086.87354, z = 20661.7109, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_42"] = {x = -17013.0566, y = -3098.34204, z = 20186.75, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_43"] = {x = -17862.8848, y = -3042.90015, z = 20258.1816, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_45"] = {x = -14141.3457, y = -4007.86353, z = 20387.7676, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_46"] = {x = -14598.2266, y = -3951.77759, z = 20690, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_47"] = {x = -14641.0068, y = -4006.05981, z = 20307.4141, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_48"] = {x = -11946.0908, y = -3854.93066, z = 19185.4512, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_49"] = {x = -12540.1328, y = -3749.24976, z = 19489.9512, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_50"] = {x = -12678.4502, y = -3778.86157, z = 19160.6582, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_51"] = {x = -5226.71045, y = -2962.99487, z = 22945.5801, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_52"] = {x = -5447.97363, y = -2912.14453, z = 23313.3535, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_53"] = {x = -5742.65723, y = -2943.29297, z = 22950.8984, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_54"] = {x = -2642.32104, y = -3973.91675, z = 22166.998, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_55"] = {x = -2314.05469, y = -3973.57251, z = 22399.3223, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_56"] = {x = -2749.30151, y = -3839.57373, z = 22599.0977, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_57"] = {x = 1050.70178, y = -3722.91699, z = 22989.0215, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_58"] = {x = 443.839172, y = -3739.55176, z = 22636.1152, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_59"] = {x = 511.538513, y = -3749.77026, z = 23325.1523, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_60"] = {x = -3439.50977, y = -3793.16772, z = 25680.2422, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_61"] = {x = -3107.38428, y = -3756.59961, z = 25472.4316, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_62"] = {x = -2990.43945, y = -4068.00293, z = 26035.0996, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_63"] = {x = -10169.5195, y = -4379.64453, z = 29142.2344, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_64"] = {x = -9978.07422, y = -4303.68945, z = 28875.6152, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_65"] = {x = -10374.9629, y = -4369.71777, z = 28717.248, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_66"] = {x = -10905.8203, y = -4196.88184, z = 30842.9961, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_67"] = {x = -10838.6689, y = -4227.31396, z = 30571.9082, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_68"] = {x = -10447.6016, y = -4190.63086, z = 30768.9336, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_70"] = {x = -11718.0381, y = -4124.13867, z = 24127.6758, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_40A"] = {x = -26437.6914, y = -897.14386, z = 15044.9834, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_40B"] = {x = -26754.5195, y = -867.125977, z = 14773.373, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_40C"] = {x = -26166.2324, y = -856.085571, z = 13989.8633, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_36A"] = {x = -24459.8438, y = -1056.31079, z = 15529.9385, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_36B"] = {x = -24080.9473, y = -964.426025, z = 15402.4941, adjacent =
	[
	]},
	["FP_ROAM_CANYON_MONSTER_36C"] = {x = -24944.6699, y = -1010.69006, z = 15331.1621, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_PATH_TO_BANDITS_31_01"] = {x = -6121.81494, y = -1859.05273, z = 20627.7148, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_PATH_TO_BANDITS_31_02"] = {x = -5764.45166, y = -1835.16357, z = 20955.4688, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYON_PATH_TO_BANDITS_31_03"] = {x = -6373.31934, y = -1849.05273, z = 21068.5684, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_28"] = {x = -409.122192, y = -2820.48193, z = 12803.9268, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_29"] = {x = -1035.41821, y = -2776.12695, z = 12732.8076, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_30"] = {x = -217.739044, y = -2824.96289, z = 12173.8613, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_31"] = {x = 17.8356419, y = -2835.84888, z = 12462.002, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_32"] = {x = 6873.00488, y = -2876.58984, z = 14895.0361, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_33"] = {x = 7260.74707, y = -2915.40698, z = 14713.4629, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_34"] = {x = 7535.25537, y = -2944.09839, z = 14856.4922, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_35"] = {x = 6963.48535, y = -2852.8418, z = 14333.6719, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_36"] = {x = 8990.5166, y = -3935.0144, z = 17070.2832, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_37"] = {x = 9542.70801, y = -4033.93115, z = 17051.4082, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_38"] = {x = 9399.31836, y = -3849.6604, z = 17547.2246, adjacent =
	[
	]},
	["FP_ROAM_ADW_CANYONBANDITSCAVE_39"] = {x = 9100.99316, y = -4006.55737, z = 16389.791, adjacent =
	[
	]},
	["FP_ITEM_CANYON_01"] = {x = -1966.32971, y = -2751.28516, z = 15587.1787, adjacent =
	[
	]},
	["FP_ITEM_CANYON_02"] = {x = 1043.19128, y = -2650.66846, z = 10800.2549, adjacent =
	[
	]},
	["FP_ITEM_CANYON_03"] = {x = 7821.77637, y = -3098.18945, z = 14168.3076, adjacent =
	[
	]},
	["FP_ITEM_CANYON_04"] = {x = 7619.74854, y = -3684.23413, z = 19181.709, adjacent =
	[
	]},
	["FP_ITEM_CANYON_05"] = {x = -3825.97583, y = -2522.46118, z = 13941.8877, adjacent =
	[
	]},
	["FP_ITEM_CANYON_06"] = {x = -5192.18848, y = -2169.02393, z = 16162.1904, adjacent =
	[
	]},
	["FP_ITEM_CANYON_07"] = {x = -6108.33301, y = -2453.20313, z = 25339.0293, adjacent =
	[
	]},
	["FP_ITEM_CANYON_08"] = {x = -2907.12036, y = -4095.52832, z = 26357.8672, adjacent =
	[
	]},
	["FP_ITEM_CANYON_09"] = {x = 4410.1958, y = -3254.30688, z = 25824.5801, adjacent =
	[
	]},
	["FP_ITEM_CANYON_10"] = {x = 2567.22607, y = -3296.24414, z = 30402.7422, adjacent =
	[
	]},
	["FP_ITEM_CANYON_11"] = {x = 2133.20874, y = -3914.31592, z = 23392.8105, adjacent =
	[
	]},
	["FP_ITEM_CANYON_12"] = {x = -10878.1611, y = -4176.1333, z = 27156.4902, adjacent =
	[
	]},
	["FP_ITEM_CANYON_13"] = {x = -13959.3867, y = -4220.33838, z = 34600.2578, adjacent =
	[
	]},
	["FP_ITEM_CANYON_14"] = {x = -7358.93994, y = -3061.50024, z = 31641.9473, adjacent =
	[
	]},
	["FP_ITEM_CANYON_16"] = {x = -4150.68799, y = -3073.64771, z = 29057.6582, adjacent =
	[
	]},
	["FP_ITEM_CANYON_15"] = {x = -5223.14307, y = -3070.28589, z = 36487.9883, adjacent =
	[
	]},
	["FP_ITEM_CANYON_17"] = {x = -12298.3936, y = -4022.6167, z = 30945.6035, adjacent =
	[
	]},
	["FP_ITEM_CANYON_18"] = {x = -18347.8301, y = -3341.88452, z = 22033.166, adjacent =
	[
	]},
	["FP_ITEM_CANYON_19"] = {x = -24766.5762, y = -1005.26251, z = 18847.6797, adjacent =
	[
	]},
	["FP_ITEM_CANYON_20"] = {x = -26026.4512, y = -1115.52271, z = 16460.2305, adjacent =
	[
	]},
	["FP_ITEM_CANYON_21"] = {x = -25186.2754, y = -952.662292, z = 15119.7959, adjacent =
	[
	]},
	["FP_ITEM_CANYON_22"] = {x = -14252.6357, y = -1876.75696, z = 15975.0791, adjacent =
	[
	]},
	["FP_ITEM_CANYON_23"] = {x = -14770.9805, y = -1910.66321, z = 11593.0889, adjacent =
	[
	]},
	["FP_ITEM_CANYON_24"] = {x = -11617.8438, y = -3872.87524, z = 19126.2012, adjacent =
	[
	]},
	["FP_ITEM_CANYON_UNIQUE"] = {x = -19688.625, y = -2458.44238, z = 25812.0371, adjacent =
	[
	]},
	["FP_ITEM_CANYON_25"] = {x = -11519.3369, y = -4300.90771, z = 24862.6289, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_08B"] = {x = 5163.73096, y = -1031.4209, z = 3400.84619, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_08A"] = {x = 4092.88721, y = -1041.76367, z = 3485.3877, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_BEHINDAKROPOLIS_04_04"] = {x = -1231.63684, y = -1820.46277, z = -14842.6699, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_BEHINDAKROPOLIS_04_03"] = {x = -1121.02979, y = -1842.66748, z = -15338.9043, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_BEHINDAKROPOLIS_04_02"] = {x = -750.514832, y = -1823.84607, z = -14682.1064, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_BEHINDAKROPOLIS_04_01"] = {x = -720.439026, y = -1843.04871, z = -15135.8867, adjacent =
	[
	]},
	["FP_ITEM_ADWPORTAL_02"] = {x = 534.776978, y = -785.474121, z = 484.766968, adjacent =
	[
	]},
	["FP_ITEM_ADWPORTAL_01"] = {x = 5020.52832, y = -933.370972, z = 4128.80518, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_19D"] = {x = 6423.48047, y = -1829.28674, z = -7563.15381, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_19C"] = {x = 6957.84619, y = -1832.00134, z = -7413.97412, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_19B"] = {x = 6042.82129, y = -1887.03931, z = -7161.80518, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_19A"] = {x = 6520.40186, y = -1849.88123, z = -6899.5293, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_18D"] = {x = 7210.30127, y = -2146.65381, z = -5128.38721, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_18C"] = {x = 7771.44824, y = -2214.46436, z = -4641.08105, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_18B"] = {x = 7637.5249, y = -2214.46436, z = -5309.76367, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_18A"] = {x = 7616.16406, y = -2140.31689, z = -4322.9834, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_17D"] = {x = 10092.1396, y = -3319.79858, z = -3409.39771, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_17C"] = {x = 10658.0752, y = -3418.01001, z = -3544.4939, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_17B"] = {x = 10040.1445, y = -3333.95239, z = -2850.40552, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_17A"] = {x = 10819.7412, y = -3503.09375, z = -3244.74341, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_16D"] = {x = 6357.97412, y = -1831.33801, z = -8468.2207, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_16C"] = {x = 7075.86133, y = -1769.17798, z = -8187.51465, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_16B"] = {x = 6294.34033, y = -1789.02942, z = -9129.30078, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_16A"] = {x = 6879.59619, y = -1747.34778, z = -9247.95703, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_15A"] = {x = -4477.97656, y = -1017.45996, z = -10451.3848, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11F"] = {x = -8128.56299, y = -1334.4729, z = -7994.87061, adjacent =
	[
	]},
	["START_ADDON"] = {x = 4702.61377, y = -874.548096, z = -1582.97778, adjacent =
	[
	]},
	["FP_SMALLTALK_ADW_ENTRANCE_BUILDING2_01"] = {x = 1938.24048, y = -571.177734, z = -2798.07813, adjacent =
	[
	]},
	["FP_SMALLTALK_ADW_ENTRANCE_BUILDING2_02"] = {x = 2096.42578, y = -571.177734, z = -2863.79395, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_01"] = {x = -8796.24219, y = -1828.87952, z = -11641.5586, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_02"] = {x = -9147.52148, y = -1877.16724, z = -11085.627, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_03"] = {x = -9558.41895, y = -1823.34839, z = -11717.3936, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_04"] = {x = -8925.79492, y = -1818.31958, z = -10451.5293, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_05"] = {x = -8521.80078, y = -1792.02148, z = -10381.7354, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_06"] = {x = -8501.25586, y = -1836.31262, z = -11118.8779, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_07"] = {x = -12243.3613, y = -2289.4812, z = -9250.55859, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_08"] = {x = -12235.2461, y = -2266.47681, z = -9891.39258, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_09"] = {x = -11795.3369, y = -2283.88354, z = -9258.48438, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_10"] = {x = -11504.123, y = -2270.68555, z = -9725.37598, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11"] = {x = -5570.55029, y = -1144.39209, z = -7898.81543, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_12"] = {x = -5091.20801, y = -1021.4599, z = -11082.7412, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_13"] = {x = -6596.0459, y = -1349.40015, z = -11582.8887, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_14"] = {x = -5248.49268, y = -1057.47827, z = -10523.0391, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_15"] = {x = -4680.77637, y = -990.699158, z = -10860.8232, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_13A"] = {x = -6315.66748, y = -1363.40015, z = -11899.1836, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_13B"] = {x = -6186.40283, y = -1369.40015, z = -11462.1846, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11A"] = {x = -5698.49805, y = -1128.67834, z = -8318.47559, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11B"] = {x = -4976.07666, y = -1113.28687, z = -8115.75342, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11C"] = {x = -7917.21924, y = -1358.67737, z = -7694.43896, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11D"] = {x = -7704.11426, y = -1407.04541, z = -8160.58789, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_11E"] = {x = -7306.7041, y = -1323.17798, z = -7775.00342, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_11_01"] = {x = 854.746704, y = -774.57373, z = 1471.41479, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_11_02"] = {x = 723.846069, y = -776.826599, z = 2433.80884, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_11_03"] = {x = 1089.77527, y = -857.750122, z = 1816.55249, adjacent =
	[
	]},
	["FP_ROAM_ADW_PORTALTEMPEL_11_04"] = {x = 795.169067, y = -844.504517, z = 2071.67505, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_RAKEPLACE_03_01"] = {x = 6976.43701, y = -4546.95264, z = -8089.75049, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_RAKEPLACE_03_02"] = {x = 7612.43213, y = -4544.95264, z = -9106.78516, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_RAKEPLACE_03_03"] = {x = 7309.74121, y = -4543.95264, z = -8684.98047, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_RAKEPLACE_03_04"] = {x = 6674.71143, y = -4552.46143, z = -8369.43164, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_01"] = {x = 1798.06641, y = -765.818909, z = -11718.4541, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_02"] = {x = -1440.03711, y = -1865.48364, z = -15311.1387, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_03"] = {x = -1334.47693, y = -367.72821, z = -12832.1943, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_04"] = {x = -2439.49634, y = -750.517578, z = -9908.01465, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_05"] = {x = -7962.39063, y = -1848.23169, z = -12452.5049, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_06"] = {x = -1844.82056, y = 1645.29285, z = 2151.36597, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_07"] = {x = -887.126343, y = -68.4168167, z = -1115.69385, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_08"] = {x = -1098.92017, y = -338.206177, z = -11876.7559, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_09"] = {x = 5024.95557, y = -1958.03894, z = -12213.8281, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_10"] = {x = 7122.24316, y = -4564.5415, z = -8024.99365, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_11"] = {x = -3104.59961, y = 36.6194839, z = -3143.10938, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_12"] = {x = -2914.94092, y = -919.268494, z = -4956.59277, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_13"] = {x = -8341.24219, y = -1033.20313, z = -5733.25439, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_14"] = {x = -14769.9707, y = -520.775635, z = -5738.80322, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_15"] = {x = -13969.2734, y = -286.363068, z = 2572.5105, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_16"] = {x = -18425.6504, y = -566.44751, z = -1277.88257, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_17"] = {x = -20916.6953, y = -424.07309, z = -2336.04395, adjacent =
	[
	]},
	["FP_ITEM_ENTRANCE_18"] = {x = -20759.3027, y = -420.07309, z = -2863.34351, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_19_01"] = {x = -19977.0742, y = -416.396729, z = -480.932373, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_19_02"] = {x = -19826.2246, y = -405.396729, z = -1277.57202, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_19_03"] = {x = -19557.4121, y = -420.074463, z = -1060.80713, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_19_04"] = {x = -19984.1855, y = -399.373199, z = -1001.39954, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_13_01"] = {x = -14695.5742, y = -358.44986, z = 936.884888, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_13_02"] = {x = -14282.3066, y = -322.207916, z = 1204.39917, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_13_03"] = {x = -14067.2412, y = -316.069916, z = 644.952759, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_13_04"] = {x = -14511.9756, y = -236.948792, z = 512.311646, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_05_01"] = {x = -9439.42773, y = -1013.09692, z = -3570.18091, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_05_02"] = {x = -9801.11621, y = -939.633789, z = -3065.56665, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_05_03"] = {x = -9091.74219, y = -981.279663, z = -3336.71802, adjacent =
	[
	]},
	["FP_ROAM_ADW_ENTRANCE_2_PIRATECAMP_05_04"] = {x = -9781.62207, y = -950.568604, z = -3611.14771, adjacent =
	[
	]},
	["FP_STAND_GOLDMINE_SCATTY"] = {x = 30670.4043, y = -3623.7605, z = 24361.1387, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_01"] = {x = 30196.9609, y = -4225.43848, z = 23898.2324, adjacent =
	[
	]},
	["FP_SMALLTALK_MINE_02"] = {x = 29721.8359, y = -4289.87598, z = 23290.3809, adjacent =
	[
	]},
	["FP_SMALLTALK_MINE_01"] = {x = 29650.0586, y = -4276.87598, z = 23237.5879, adjacent =
	[
	]},
	["FP_ROAM_MB_08"] = {x = 27720.6406, y = -3762.5293, z = 26676.7461, adjacent =
	[
	]},
	["FP_ROAM_MB_07"] = {x = 27974.3281, y = -3708.5293, z = 26155.0059, adjacent =
	[
	]},
	["FP_ROAM_MB_06"] = {x = 27613.0703, y = -3713.5293, z = 26166.8867, adjacent =
	[
	]},
	["FP_ROAM_MB_05"] = {x = 27959.7441, y = -3782.5293, z = 26893.9063, adjacent =
	[
	]},
	["FP_ROAM_MB_04"] = {x = 26318.2969, y = -3583.16504, z = 25861.4473, adjacent =
	[
	]},
	["FP_ROAM_MB_03"] = {x = 26706.7539, y = -3581.16504, z = 26184.6738, adjacent =
	[
	]},
	["FP_ROAM_MB_02"] = {x = 26629.6094, y = -3586.16504, z = 25921.1035, adjacent =
	[
	]},
	["FP_ROAM_MB_01"] = {x = 26356.3945, y = -3583.16504, z = 26131.0508, adjacent =
	[
	]},
	["FP_CAMPFIRE_PARDOS"] = {x = 27749.4336, y = -3789.83862, z = 27100.8711, adjacent =
	[
	]},
	["FP_ROAM_MC_01"] = {x = 33091.957, y = -4570.27393, z = 29330.0039, adjacent =
	[
	]},
	["FP_ROAM_MC_02"] = {x = 33678.707, y = -4505.96289, z = 28564.7949, adjacent =
	[
	]},
	["FP_ROAM_MC_03"] = {x = 33791.7148, y = -4654.24609, z = 29551.7109, adjacent =
	[
	]},
	["FP_ROAM_MC_04"] = {x = 32945.043, y = -4479.12891, z = 28680.377, adjacent =
	[
	]},
	["FP_ROAM_MC_05"] = {x = 34889.9141, y = -4487.68359, z = 29613.6309, adjacent =
	[
	]},
	["FP_ROAM_MC_06"] = {x = 34502.4023, y = -4464.50098, z = 28613.3867, adjacent =
	[
	]},
	["FP_ROAM_MC_07"] = {x = 34347.4531, y = -4567.68359, z = 29306.0703, adjacent =
	[
	]},
	["FP_ROAM_MC_08"] = {x = 35118.4609, y = -4527.68359, z = 29196.4395, adjacent =
	[
	]},
	["FP_ROAM_MC_09"] = {x = 34846.4297, y = -4361.43262, z = 28135.6484, adjacent =
	[
	]},
	["FP_ROAM_MC_10"] = {x = 35269.4609, y = -4541.229, z = 28915.1504, adjacent =
	[
	]},
	["FP_ROAM_MC_11"] = {x = 35914.2969, y = -4503.83398, z = 28529.7188, adjacent =
	[
	]},
	["FP_ROAM_MC_12"] = {x = 35633.875, y = -4386.93164, z = 28064.2129, adjacent =
	[
	]},
	["FP_ROAM_MC_13"] = {x = 35744.0742, y = -4560.17822, z = 29368.373, adjacent =
	[
	]},
	["FP_ROAM_MC_14"] = {x = 36273.3867, y = -4534.17383, z = 29733.25, adjacent =
	[
	]},
	["FP_ROAM_MC_15"] = {x = 35518.3633, y = -4476.55273, z = 29819.4277, adjacent =
	[
	]},
	["FP_ROAM_MC_16"] = {x = 35830.9648, y = -4529.36914, z = 30274.0508, adjacent =
	[
	]},
	["FP_ROAM_MC_17"] = {x = 35188.4414, y = -4217.9707, z = 27019.8281, adjacent =
	[
	]},
	["FP_ROAM_MC_18"] = {x = 35142.1367, y = -4304.96094, z = 27559.1797, adjacent =
	[
	]},
	["FP_ROAM_MC_19"] = {x = 35688.5664, y = -4342.24609, z = 27621.5508, adjacent =
	[
	]},
	["FP_ROAM_MC_20"] = {x = 34645.2266, y = -4170.83594, z = 27054.6914, adjacent =
	[
	]},
	["FP_STAND_MINE_01"] = {x = 29160.3281, y = -3711.32373, z = 24734.0859, adjacent =
	[
	]},
	["FP_STAND_MINE_02"] = {x = 32654.125, y = -3597.83716, z = 24685.25, adjacent =
	[
	]},
	["FP_STAND_MINE_SCATTY"] = {x = 28881.2324, y = -4072.71631, z = 22787.7207, adjacent =
	[
	]},
	["FP_STAND_GARAZ_01"] = {x = 34236.6211, y = -4706.19824, z = 30147.3457, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_02"] = {x = 29742.9785, y = -4281.94971, z = 23997.9395, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_03"] = {x = 29833.9316, y = -4281.94971, z = 24145.4824, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_04"] = {x = 29944.9082, y = -4266.93945, z = 24228.2793, adjacent =
	[
	]},
	["FP_STAND_MINEGUARD_05"] = {x = 29599.75, y = -4286.99854, z = 23331.6484, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_06"] = {x = -37291.4805, y = -1990.3075, z = 20188.8145, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_05"] = {x = -37494.6406, y = -2011.61475, z = 20214.6094, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_07"] = {x = -37150.7813, y = -1972.2854, z = 20394.2813, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_04"] = {x = -37498.4453, y = -1964.62292, z = 19151.4629, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_02"] = {x = -37788.9258, y = -1998.64429, z = 18877.7793, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_01"] = {x = -37719.8086, y = -1998.22046, z = 19218.0352, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_PIR_03"] = {x = -37523.2656, y = -1950.37146, z = 18859.3047, adjacent =
	[
	]},
	["FP_CAMPFIRE_STAND_08"] = {x = -37344.4648, y = -1972.36292, z = 20574.0449, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_06"] = {x = -20069.6289, y = -502.336182, z = 6133.13135, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_05"] = {x = -19984.3867, y = -418.727814, z = 6376.57617, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_04"] = {x = -20146.5723, y = -448.275116, z = 6435.51221, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_03"] = {x = -20475.0391, y = -566.627686, z = 6238.69238, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_02"] = {x = -20252.998, y = -560.854553, z = 6198.4502, adjacent =
	[
	]},
	["FP_ROAM_LUMBER_01"] = {x = -20317.0664, y = -497.028961, z = 6497.27393, adjacent =
	[
	]},
	["FP_CAMPFIRE_LUMBER_01"] = {x = -19910.8008, y = 53.2101135, z = 7709.62646, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_010"] = {x = -33474.2344, y = -218.909882, z = 9546.05469, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_009"] = {x = -33655.6914, y = -247.035095, z = 9914.5498, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_008"] = {x = -33014.1602, y = -211.404938, z = 9986.28418, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_007"] = {x = -33183.0273, y = -235.458099, z = 9473.71387, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_006"] = {x = -30023.8203, y = -876.611938, z = 9014.78516, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_005"] = {x = -29755.8027, y = -972.820679, z = 8877.12891, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_004"] = {x = -29037.1035, y = -1019.56732, z = 9393.69336, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_003"] = {x = -29221.3711, y = -1032.26575, z = 9277.58008, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_002"] = {x = -29598.9492, y = -967.738281, z = 9142.81738, adjacent =
	[
	]},
	["FP_ROAM_PIRAT_001"] = {x = -29794.9277, y = -883.985901, z = 9154.34766, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_16"] = {x = -32023.3027, y = -1985.54541, z = 5575.97754, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_15"] = {x = -32233.1504, y = -1941.72375, z = 5135.07422, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_14"] = {x = -33056.5586, y = -1964.22559, z = 5358.12158, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_13"] = {x = -37573.5195, y = -2050.16064, z = 12174.3877, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_12"] = {x = -36906.2891, y = -1987.1239, z = 12195.2158, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_11"] = {x = -37389.8594, y = -2011.73718, z = 10546.585, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_10"] = {x = -37640.7109, y = -2061.06006, z = 11190.5859, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_09"] = {x = -36878.6016, y = -2003.31018, z = 10684.3271, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_08"] = {x = -36659.7188, y = -2009.09277, z = 11020.5879, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_06"] = {x = -38488.8711, y = -2064.28613, z = 8625.04297, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_05"] = {x = -38546.9102, y = -2071.52222, z = 9346.77148, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_04"] = {x = -35095.5234, y = -1926.72986, z = 7582.04297, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_03"] = {x = -34545.7852, y = -1924.76746, z = 7742.86963, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_02"] = {x = -34776.4297, y = -1915.59155, z = 6912.55566, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_01"] = {x = -35233.6875, y = -1939.18787, z = 6810.96973, adjacent =
	[
	]},
	["FP_ROAM_ISLE_06"] = {x = -41167.7773, y = -2104.17065, z = 24452.7402, adjacent =
	[
	]},
	["FP_ROAM_ISLE_05"] = {x = -41843.7383, y = -2060.39209, z = 24764.459, adjacent =
	[
	]},
	["FP_ROAM_ISLE_04"] = {x = -40914.2031, y = -2037.95435, z = 24114.9316, adjacent =
	[
	]},
	["FP_ROAM_ISLE_03"] = {x = -41421.3672, y = -2015.10449, z = 23263.3535, adjacent =
	[
	]},
	["FP_ROAM_ISLE_02"] = {x = -42121.6523, y = -1983.71826, z = 23368.1445, adjacent =
	[
	]},
	["FP_ROAM_ISLE_01"] = {x = -42053.7852, y = -2065.96655, z = 22487.6914, adjacent =
	[
	]},
	["FP_ROAM_BEACH_09"] = {x = -27114.1992, y = -2046.85242, z = 27105.4922, adjacent =
	[
	]},
	["FP_ROAM_BEACH_08"] = {x = -26038.2539, y = -2220.90161, z = 24742.834, adjacent =
	[
	]},
	["FP_ROAM_BEACH_07"] = {x = -25734.2031, y = -2209.27661, z = 24259.2637, adjacent =
	[
	]},
	["FP_ROAM_BEACH_06"] = {x = -25698.4414, y = -2167.37354, z = 24902.0137, adjacent =
	[
	]},
	["FP_ROAM_BEACH_05"] = {x = -28145.6406, y = -2070.93921, z = 27460.3379, adjacent =
	[
	]},
	["FP_ROAM_BEACH_04"] = {x = -27329.5313, y = -2091.37744, z = 27633.0371, adjacent =
	[
	]},
	["FP_ROAM_BEACH_03"] = {x = -28479.543, y = -2002.74512, z = 26852.7031, adjacent =
	[
	]},
	["FP_ROAM_BEACH_02"] = {x = -28782.2461, y = -2058.20874, z = 27312.9766, adjacent =
	[
	]},
	["FP_ROAM_BEACH_01"] = {x = -28038.9531, y = -2042.06079, z = 26420.7656, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_05"] = {x = -36352.4805, y = -1963.37622, z = 21286.2129, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_04"] = {x = -31952.9512, y = -2099.59009, z = 23692.1328, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_03"] = {x = -35184.5547, y = -1958.43848, z = 19742.4258, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_08"] = {x = -35513.4219, y = -1961.75403, z = 19648.8086, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_09"] = {x = -35104.7695, y = -1961.74878, z = 19318.1621, adjacent =
	[
	]},
	["FP_SMALLTALK_PIRATECAMP_02"] = {x = -33097.1914, y = -2021.20581, z = 24383.1797, adjacent =
	[
	]},
	["FP_SMALLTALK_PIRATECAMP_01"] = {x = -32861.8516, y = -2028.03613, z = 24315.9414, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_02"] = {x = -33371.2148, y = -1945.80444, z = 23638.0996, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_01"] = {x = -33277.0781, y = -1988.7179, z = 24108.1152, adjacent =
	[
	]},
	["FP_ADW_GREGSBOTTLE"] = {x = -34090.2539, y = -1470.37439, z = 21296.3379, adjacent =
	[
	]},
	["ADW_PIRATECAMP_CAMPFIRE"] = {x = -32830.4023, y = -1139.73718, z = 12456.4668, adjacent =
	[
	]},
	["ADW_PIRATECAMP_SMALLTALK_02"] = {x = -35276.9844, y = -1962.03906, z = 18818.2305, adjacent =
	[
	]},
	["ADW_PIRATECAMP_SMALLTALK_01"] = {x = -35460.8672, y = -1963.59668, z = 18755.1563, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_06"] = {x = -35438.2578, y = -1960.20068, z = 20568.9727, adjacent =
	[
	]},
	["FP_STAND_PIRATECAMP_07"] = {x = -35849.6758, y = -1959.12219, z = 19872.3672, adjacent =
	[
	]},
	["FP_ROAM_BEACH_10"] = {x = -29931.8301, y = -2030.45361, z = 26385.4238, adjacent =
	[
	]},
	["FP_ROAM_BEACH_11"] = {x = -30268.1309, y = -2060.16211, z = 26879.9805, adjacent =
	[
	]},
	["FP_ROAM_BEACH_12"] = {x = -29716.6484, y = -2049.43237, z = 27244.002, adjacent =
	[
	]},
	["FP_ROAM_BEACH_13"] = {x = -29449.0156, y = -2027.21863, z = 26660.875, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_20"] = {x = -36833.6836, y = -1973.40771, z = 8327.52832, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_21"] = {x = -36240.4219, y = -1934.50208, z = 8280.60156, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_22"] = {x = -37142.3555, y = -1986.31799, z = 9131.9082, adjacent =
	[
	]},
	["FP_ROAM_LONEBEACH_23"] = {x = -36710.3086, y = -1982.55469, z = 9334.22949, adjacent =
	[
	]},
	["FP_ROAM_SECRETCAVE_01"] = {x = -22622.0508, y = -4510.40869, z = 7200.75098, adjacent =
	[
	]},
	["FP_ROAM_SECRETCAVE_02"] = {x = -22965.3633, y = -4498.87256, z = 7393.50635, adjacent =
	[
	]},
	["FP_ROAM_SECRETCAVE_03"] = {x = -22344.4707, y = -4589.45752, z = 7826.12988, adjacent =
	[
	]},
	["FP_ROAM_SECRETCAVE_04"] = {x = -22945.4473, y = -4548.87939, z = 7781.76807, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_01"] = {x = -22886.0117, y = -1963.42322, z = 4612.45166, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_02"] = {x = -23166.6523, y = -2089.23511, z = 4835.80176, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_03"] = {x = -23698.5723, y = -3035.86182, z = 7383.85303, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_04"] = {x = -26586.5703, y = -3966.76074, z = 8872.14355, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_05"] = {x = -30645.7227, y = -4526.51172, z = 9422.09473, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_06"] = {x = -23324.7676, y = -2499.60669, z = 5686.3042, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_07"] = {x = -26698.0898, y = -4091.53149, z = 9383.15039, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_08"] = {x = -24103.7598, y = -3045.30005, z = 7060.37012, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_09"] = {x = -28213.2012, y = -4170.34912, z = 9185.80371, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_10"] = {x = -30542.9883, y = -4444.6875, z = 8842.17383, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_11"] = {x = -27410.416, y = -4177.41455, z = 9297.34766, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_12"] = {x = -27696.834, y = -4216.34521, z = 9961.76563, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_13"] = {x = -30178.4629, y = -4403.98682, z = 8247.70996, adjacent =
	[
	]},
	["FP_ROAM_WATERHOLE_14"] = {x = -28194.8809, y = -3958.92725, z = 9526.43945, adjacent =
	[
	]},
	["FP_ROAM_PIRATECAMP_WAY_01"] = {x = -18577.5938, y = -677.474304, z = 1513.04834, adjacent =
	[
	]},
	["FP_ROAM_PIRATECAMP_WAY_02"] = {x = -18733.2441, y = -733.376648, z = 1782.27283, adjacent =
	[
	]},
	["FP_ROAM_PIRATECAMP_WAY_03"] = {x = -18536.1758, y = -725.059631, z = 3065.4917, adjacent =
	[
	]},
	["FP_ROAM_PIRATECAMP_WAY_04"] = {x = -18832.6348, y = -765.550415, z = 3130.03467, adjacent =
	[
	]},
	["FP_CAMPFIRE_BANDITE_1"] = {x = -31736.7539, y = -274.023315, z = 8372.26953, adjacent =
	[
	]},
	["FP_CAMPFIRE_BANDITS_2"] = {x = -31992.875, y = -257.031921, z = 8609.52637, adjacent =
	[
	]},
	["FP_STAND_BANDITS_01"] = {x = -31761.7871, y = -281.335144, z = 8833.47168, adjacent =
	[
	]},
	["FP_CAMPFIRE_LUMBER_03"] = {x = -20858.7012, y = -405.012024, z = 7249.6123, adjacent =
	[
	]},
	["FP_CAMPFIRE_OWEN"] = {x = -35767.5703, y = -1941.27576, z = 18449.8906, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_01"] = {x = -36823.4648, y = -2185.83667, z = 25703.1152, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_02"] = {x = -38673.9414, y = -2150.97534, z = 14009.5215, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_03"] = {x = -40734.375, y = -2225.24219, z = 18438.1191, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_04"] = {x = -40105.5039, y = -2170.68408, z = 20756.8867, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_05"] = {x = -39309.8398, y = -2208.63428, z = 11754.457, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_06"] = {x = -39291.543, y = -2163.41357, z = 16117.1152, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_07"] = {x = -40462.3008, y = -2181.13525, z = 17446.2871, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_08"] = {x = -39137.0898, y = -2172.73608, z = 19582.8516, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_09"] = {x = -40364.1016, y = -2235.64111, z = 23312.3086, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_10"] = {x = -36075.9023, y = -2266.30786, z = 28538.0879, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_11"] = {x = -36715.2734, y = -2179.96436, z = 24132.5078, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_12"] = {x = -39836.3242, y = -2228.83765, z = 10523.6162, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_13"] = {x = -34480.9844, y = -2219.59424, z = 26391.125, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_14"] = {x = -38913.0625, y = -2295.20874, z = 25407.0098, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_15"] = {x = -38654.4727, y = -2154.84937, z = 23969.6836, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_16"] = {x = -38992.8438, y = -2146.9668, z = 12761.6191, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_17"] = {x = -39109.4648, y = -2131.87183, z = 7625.51123, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_18"] = {x = -40169.8594, y = -2166.19824, z = 8109.23926, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_19"] = {x = -40737.582, y = -2272.91675, z = 21070.8652, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_20"] = {x = -32743.9922, y = -2207.79199, z = 26571.4629, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_21"] = {x = -31555.457, y = -2185.38159, z = 30376.9648, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_22"] = {x = -31006.2188, y = -2194.06567, z = 28694.9531, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_23"] = {x = -29286.8359, y = -2153.0625, z = 28652.7031, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_24"] = {x = -25168.2266, y = -2246.26489, z = 29609.2559, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_25"] = {x = -25327.7363, y = -2242.55298, z = 29750.3242, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_26"] = {x = -27071.0742, y = -2251.50024, z = 30496.6953, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_27"] = {x = -28915.0898, y = -2233.04468, z = 30510.1387, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_28"] = {x = -30035.6973, y = -2192.08032, z = 33512.9727, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_29"] = {x = -31122.7051, y = -2211.27686, z = 33153.4648, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SHALLOWWATER_30"] = {x = -33437.5039, y = -2283.15674, z = 31359.8086, adjacent =
	[
	]},
	["FP_ITEMSPAWN_WATERHOLE_01"] = {x = -30193.4121, y = -4597.65625, z = 10340.7246, adjacent =
	[
	]},
	["FP_ITEMSPAWN_SECRETCAVE_01"] = {x = -22051.082, y = -4521.17725, z = 6949.65723, adjacent =
	[
	]},
	["FP_ITEMSPAWN_WAY_01"] = {x = -27668.4082, y = -920.794983, z = 11524, adjacent =
	[
	]},
	["FP_ITEMSPAWN_WAY_02"] = {x = -28685.3008, y = -897.077515, z = 16162.5342, adjacent =
	[
	]},
	["FP_ITEMSPAWN_BANDITS_01"] = {x = -31294.7129, y = -313.416412, z = 6811.58789, adjacent =
	[
	]},
	["FP_ITEMSPAWN_BANDITS_02"] = {x = -33137.1953, y = -349.203308, z = 7549.53809, adjacent =
	[
	]},
	["FP_ITEMSPAWN_BANDITS_03"] = {x = -32871.2773, y = 852.221863, z = 9097.07617, adjacent =
	[
	]},
	["FP_ITEMSPAWN_LONEBEACH_01"] = {x = -35992.8906, y = -1954.64734, z = 11237.6875, adjacent =
	[
	]},
	["FP_ITEMSPAWN_WATER_01"] = {x = -43202.4883, y = -2040.79077, z = 14008.0508, adjacent =
	[
	]},
	["FP_ITEMSPAWN_BEACH_01"] = {x = -36647.8906, y = -1950.02112, z = 15996.5156, adjacent =
	[
	]},
	["FP_ITEMSPAWN_BEACH_02"] = {x = -33126.1914, y = -1898.11975, z = 17708.5137, adjacent =
	[
	]},
	["FP_ITEMSPAWN_LURKERBEACH_01"] = {x = -29643.2656, y = -2029.25, z = 25741.7129, adjacent =
	[
	]},
	["FP_ITEMSPAWN_LURKERBEACH_02"] = {x = -27869.4238, y = -2024.6543, z = 25819.8184, adjacent =
	[
	]},
	["FP_ITEMSPAWN_LURKERBEACH_03"] = {x = -25239.5625, y = -2171.38623, z = 24675.3516, adjacent =
	[
	]},
	["FP_ITEMSPAWN_LONEBEACH_02"] = {x = -31646.1289, y = -1958.26416, z = 5646.49268, adjacent =
	[
	]},
	["FP_PEE_PIR_01"] = {x = -37099.4414, y = -1976.15283, z = 19481.7832, adjacent =
	[
	]},
	["FP_PEE_STAND_PIR_04"] = {x = -37997.4141, y = -2106.33472, z = 20852.8652, adjacent =
	[
	]},
	["FP_PEE_PIR_03"] = {x = -37229.1445, y = -1954.53467, z = 18252.4297, adjacent =
	[
	]},
	["FP_PEE_PIR_02"] = {x = -36914.1133, y = -1854.21533, z = 19852.7656, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_01"] = {x = -11823.7119, y = -4539.80859, z = -25381.9199, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_188"] = {x = -21895.9004, y = -4056.95801, z = -10371.29, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_187"] = {x = -22189.1074, y = -4074.84717, z = -9887.23438, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_184"] = {x = -10019.585, y = -5052.86084, z = -25253.998, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_183"] = {x = -10081.6553, y = -5081.82861, z = -24866.4688, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_182"] = {x = -9418.61816, y = -5274.78271, z = -23367.1777, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_181"] = {x = -9664.53125, y = -5259.39648, z = -23134.3066, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_180"] = {x = -8633.86133, y = -5193.39453, z = -21282.1348, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_179"] = {x = -8470.49805, y = -5201.46875, z = -20918.4258, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_178"] = {x = -8222.34961, y = -5251.84521, z = -21169.543, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_177"] = {x = -12694.1074, y = -3230.9563, z = -24722.2695, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_176"] = {x = -12396.2568, y = -3271.81274, z = -24906.9961, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_175"] = {x = -12359.6943, y = -3305.01929, z = -24577.0605, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_146"] = {x = -10136.9688, y = -3622.50879, z = -23667.9336, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_145"] = {x = -10618.8418, y = -3479.18652, z = -24183.1758, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_144"] = {x = -10449.1201, y = -3547.33594, z = -23978.5293, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_143"] = {x = -9939.62793, y = -3739.15137, z = -23370.668, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_142"] = {x = -10147.2891, y = -3712.31567, z = -23227.6875, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_141"] = {x = -14933.9678, y = -2880.82373, z = -21945.0508, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_140"] = {x = -14481.2559, y = -3110.22266, z = -22200.4453, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_139"] = {x = -14380.1318, y = -2917.51636, z = -21719.9102, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_138"] = {x = -14546.6855, y = -2858.56641, z = -21395.2793, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_137"] = {x = -13931.6846, y = -2740.58301, z = -20315.2266, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_136"] = {x = -13492.6611, y = -2757.37744, z = -20852.8242, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_135"] = {x = -13802.5947, y = -2741.68896, z = -20637.1523, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_134"] = {x = -13393.1914, y = -3007.14014, z = -20128.2246, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_133"] = {x = -13728.3809, y = -2921.02173, z = -20136.0625, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_132"] = {x = -13364.3779, y = -2891.12329, z = -20528.0527, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_001"] = {x = -22006.1992, y = -2898.89771, z = -5765.82715, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_002"] = {x = -22514.6973, y = -2916.02588, z = -5499.85449, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_003"] = {x = -20568.5137, y = -2454.94751, z = -4296.26123, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_004"] = {x = -20759.1152, y = -2429.38794, z = -3871.4353, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_005"] = {x = -20300.7168, y = -2458.46899, z = -4544.68506, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_007"] = {x = -22799.1484, y = -2917.47534, z = -3890.28711, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_008"] = {x = -22507.6074, y = -2776.07349, z = -3120.75659, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_009"] = {x = -22967.6289, y = -2822.06982, z = -3039.5293, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_010"] = {x = -23055.8887, y = -2851.48193, z = -3269.34619, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_011"] = {x = -22715.207, y = -2854.16797, z = -3352.2041, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_012"] = {x = -19587.5039, y = -3829.35596, z = -10115.3994, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_013"] = {x = -19226.8027, y = -4024.41235, z = -10050.3154, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_014"] = {x = -21679.0879, y = -4256.14697, z = -9450.00098, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_015"] = {x = -22148.5508, y = -4262.73047, z = -9449.47363, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_016"] = {x = -21742.6211, y = -4148.13818, z = -9944.21387, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_017"] = {x = -20527.0684, y = -3823.90698, z = -7552.55078, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_018"] = {x = -22323.8809, y = -3613.05811, z = -6904.2793, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_020"] = {x = -19930.2051, y = -1343.66809, z = -6851.7002, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_021"] = {x = -23447.957, y = -3410.86841, z = -12473.4746, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_022"] = {x = -23702.5254, y = -3430.87378, z = -12136.9629, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_023"] = {x = -21139.9648, y = -3390.56348, z = -13652.2041, adjacent =
	[
	]},
	["FP_CAMPFIRE_VALLEY_001"] = {x = -22052.8203, y = -3668.29907, z = -17949.3262, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_024"] = {x = -21850.2773, y = -3656.57251, z = -18297.2383, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_025"] = {x = -21681.6523, y = -3657.16797, z = -18333.5488, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_026"] = {x = -17381.4141, y = -2828.3501, z = -10969.7529, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_027"] = {x = -17669.0117, y = -2875.27539, z = -11245.083, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_028"] = {x = -17665.3066, y = -2942.22559, z = -10735.0635, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_032"] = {x = -21686.8691, y = -3802.85864, z = -12425.4014, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_033"] = {x = -21544.1191, y = -3826.41113, z = -12167.042, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_034"] = {x = -21727.2617, y = -3773.0564, z = -12007.3604, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_035"] = {x = -21968.4238, y = -3793.24561, z = -12262.0801, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_036"] = {x = -19233.6055, y = -3108.80762, z = -13639.3779, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_037"] = {x = -18868.6699, y = -3136.8479, z = -13750.2852, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_038"] = {x = -21115.4121, y = -3332.63208, z = -16116.9346, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_039"] = {x = -21157.9902, y = -3349.84668, z = -16547.5332, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_040"] = {x = -21472.666, y = -3381.66748, z = -16240.0449, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_041"] = {x = -18644.1816, y = -3472.67603, z = -19358.9121, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_042"] = {x = -18559.7637, y = -3516.90015, z = -19077.1777, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_043"] = {x = -21506.3008, y = -3115.29761, z = -25413.7891, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_044"] = {x = -22200.2148, y = -3200.89331, z = -25787.3203, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_045"] = {x = -22470.1543, y = -3162.77563, z = -25505.2559, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_046"] = {x = -21776.1309, y = -3189.7583, z = -25160.1504, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_047"] = {x = -21821.0781, y = -3137.44678, z = -25732.3418, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_048"] = {x = -22237.4434, y = -3195.29785, z = -25183.5664, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_049"] = {x = -22325.2324, y = -3191.50073, z = -24405.3203, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_050"] = {x = -22049.627, y = -3179.92114, z = -24737.7695, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_051"] = {x = -21632.4141, y = -3196.47827, z = -24785.4512, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_052"] = {x = -21446.8965, y = -3187.97998, z = -24422.248, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_053"] = {x = -21331.5391, y = -3196.42578, z = -25010.377, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_054"] = {x = -22410.1055, y = -3169.94678, z = -24829.3516, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_055"] = {x = -21547.3809, y = -3174.86157, z = -26157.2637, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_056"] = {x = -22671.623, y = -3197.70532, z = -26016.8965, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_057"] = {x = -21638.8926, y = -3184.5979, z = -23574.7207, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_059"] = {x = -21601.0547, y = -3192.31128, z = -23169.8281, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_060"] = {x = -21481.125, y = -3185.98389, z = -23010.6914, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_061"] = {x = -21884.4297, y = -3197.54468, z = -23530.1563, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_063"] = {x = -21857.6836, y = -3189.07666, z = -23242.8887, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_064"] = {x = -22018.9004, y = -3205.15088, z = -23080.6934, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_065"] = {x = -21433.7852, y = -3210.78516, z = -22468.166, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_066"] = {x = -21727.0742, y = -3340.08911, z = -22312.9707, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_067"] = {x = -21453.6426, y = -3473.02783, z = -22221.9531, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_068"] = {x = -21639.1445, y = -3601.1814, z = -22069.9219, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_069"] = {x = -21327.7129, y = -3679.78809, z = -21963.5977, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_070"] = {x = -21645.6309, y = -3795.89111, z = -21791.8926, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_081"] = {x = -19576.457, y = -3848.11426, z = -22169.166, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_082"] = {x = -19648.9863, y = -3840.47144, z = -21919.8574, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_083"] = {x = -19481.0938, y = -3903.6853, z = -21738.707, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_084"] = {x = -19188.4434, y = -3830.60083, z = -21730.332, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_085"] = {x = -19010.6992, y = -3868.35864, z = -22076.291, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_086"] = {x = -19270.7715, y = -3850.50659, z = -22333.0859, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_058"] = {x = -20763.4473, y = -3320.42456, z = -16393.2637, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_087"] = {x = -23278.4434, y = -3579.73071, z = -20914.8418, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_088"] = {x = -23169.0742, y = -3592.2583, z = -21387.8008, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_089"] = {x = -20338.8008, y = -4054.24854, z = -19879.2578, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_090"] = {x = -20118.541, y = -3975.5188, z = -19806.6777, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_091"] = {x = -20586.4414, y = -4079.05957, z = -19917.5508, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_092"] = {x = -19836.1953, y = -4060.16699, z = -19761.9805, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_093"] = {x = -18385.3145, y = -4692.2417, z = -21156.1621, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_094"] = {x = -18503.0273, y = -4645.39111, z = -21300.8887, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_095"] = {x = -19224.8906, y = -4814.34473, z = -21195.375, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_096"] = {x = -18393.9082, y = -4596.07275, z = -20611.8555, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_098"] = {x = -18980.9707, y = -4753.82959, z = -21409.293, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_099"] = {x = -18592.6191, y = -4689.19043, z = -20865.0391, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_101"] = {x = -19951.9707, y = -4158.33887, z = -20047.6309, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_102"] = {x = -20296.9785, y = -4143.08203, z = -20123.8652, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_103"] = {x = -21393.9492, y = -3750.65771, z = -19043.4688, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_104"] = {x = -21007.5547, y = -3803.94604, z = -18987.332, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_105"] = {x = -21251.3867, y = -3757.19507, z = -18806.332, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_073"] = {x = -23530.0352, y = -3568.57202, z = -21273.2949, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_107"] = {x = -16198.5186, y = -3573.41699, z = -13662.3213, adjacent =
	[
	]},
	["FP_CAMPFIRE_VALLEY_003"] = {x = -14187.8828, y = -3522.52588, z = -14481.3359, adjacent =
	[
	]},
	["FP_CAMPFIRE_VALLEY_002"] = {x = -14667.0781, y = -3553.50757, z = -14420.2529, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_108"] = {x = -13190.3379, y = -2643.49683, z = -12151.127, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_109"] = {x = -12843.3389, y = -2587.19214, z = -12098.7295, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_110"] = {x = -12629.4883, y = -2588.56177, z = -12338.5254, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_111"] = {x = -12612.3506, y = -2574.28174, z = -12125.4453, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_112"] = {x = -11960.6455, y = -2686.41113, z = -12442.3506, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_113"] = {x = -11737.9795, y = -2608.99902, z = -12251.6162, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_114"] = {x = -11571.1172, y = -3169.25073, z = -13879.1494, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_115"] = {x = -11441.3535, y = -3147.60303, z = -14249.6807, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_116"] = {x = -11313.418, y = -3274.34155, z = -13747.9014, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_117"] = {x = -11179.291, y = -3205.84009, z = -14042.9258, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_118"] = {x = -12099.7598, y = -3568.2749, z = -16115.6621, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_119"] = {x = -12194.6621, y = -3647.87769, z = -16440.0742, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_120"] = {x = -12318.9678, y = -3571.65283, z = -16057.3623, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_121"] = {x = -11964.4063, y = -3683.59912, z = -16381.9346, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_122"] = {x = -12145.0664, y = -3644.14038, z = -15893.5811, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_124"] = {x = -11970.8525, y = -3685.59082, z = -16026.4395, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_125"] = {x = -11325.915, y = -3306.72168, z = -20034.0801, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_126"] = {x = -11356.5635, y = -3132.354, z = -20771.291, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_127"] = {x = -11842.9951, y = -3096.27173, z = -20586.1992, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_128"] = {x = -12008.123, y = -3279.89746, z = -20197.5215, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_129"] = {x = -11624.4688, y = -3166.31787, z = -20215.6172, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_130"] = {x = -11122.5742, y = -3238.39746, z = -20341.7637, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_131"] = {x = -9685.25391, y = -3157.11401, z = -19896.8086, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_186"] = {x = -21390.6543, y = -4228.90234, z = -9938.35254, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_150"] = {x = -9254.87891, y = -5298.75439, z = -22491.9863, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_151"] = {x = -9548.57715, y = -5259.00732, z = -22528.4902, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_152"] = {x = -9444.99609, y = -5283.125, z = -22979.2891, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_153"] = {x = -9591.61719, y = -5291.41455, z = -23639.8145, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_154"] = {x = -10851.8242, y = -4952.24512, z = -25175.8848, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_155"] = {x = -10283.2471, y = -4998.10303, z = -25381.3105, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_156"] = {x = -10636.6455, y = -4853.78711, z = -25485.8945, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_157"] = {x = -10404.1709, y = -4948.88037, z = -25033.9063, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_158"] = {x = -13257.4717, y = -4372.05273, z = -29622.4434, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_159"] = {x = -13005.3506, y = -4391.85889, z = -29108.1445, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_160"] = {x = -12712.9326, y = -4393.20068, z = -28742.0723, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_161"] = {x = -12414.3447, y = -4365, z = -30048.9355, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_162"] = {x = -12063.2881, y = -4371.14209, z = -29762.5098, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_163"] = {x = -11757.2207, y = -4364.95898, z = -29309.627, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_SPAWN_01"] = {x = -7365.78613, y = -4970.77588, z = -24992.4531, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_SPAWN_03"] = {x = -5899.64648, y = -4742.59082, z = -23055.9355, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_165"] = {x = -11401.6631, y = -5285.56836, z = -20112.2578, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_166"] = {x = -11722.6631, y = -5230.65479, z = -20110.7852, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_167"] = {x = -11333.8291, y = -5267.65527, z = -19843.3848, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_168"] = {x = -13053.7021, y = -5247.65576, z = -21260.7461, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_169"] = {x = -13153.6602, y = -5219.41943, z = -20911.8574, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_170"] = {x = -12728.8057, y = -5286.54834, z = -20928.4023, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_171"] = {x = -12645.5586, y = -5304.41895, z = -21266.2793, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_190"] = {x = -17343.7422, y = -4573.57568, z = -8601.16309, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_191"] = {x = -17381.6445, y = -4596.15771, z = -9245.21289, adjacent =
	[
	]},
	["FP_ROAM_VALLEY_192"] = {x = -17775.8613, y = -4545.27344, z = -8995.21387, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_02"] = {x = -5637.09717, y = -4800.82324, z = -24281.8418, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_03"] = {x = -10690.4678, y = -3472.1499, z = -24770.9512, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_04"] = {x = -15162.9063, y = -2801.47485, z = -21647.6094, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_05"] = {x = -10146.4248, y = -3235.88232, z = -19736.4668, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_06"] = {x = -12311.4883, y = -3600.75464, z = -15527.1748, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_07"] = {x = -12668.5244, y = -3151.25464, z = -14560.7783, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_08"] = {x = -10581.042, y = -3317.94946, z = -15375.3242, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_09"] = {x = -10252.5664, y = -3558.35376, z = -24618.4961, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_10"] = {x = -18885.0781, y = -2556.78882, z = -5606.61816, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_11"] = {x = -24273.25, y = -2918.53003, z = -3834.54053, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_12"] = {x = -24505.8047, y = -2711.15186, z = -529.420898, adjacent =
	[
	]},
	["FP_ITEM_VALLEY_13"] = {x = -24762.3301, y = -2711.53638, z = -527.880066, adjacent =
	[
	]},

}
