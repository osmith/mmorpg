// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEventHandler("onPlayerJoin", function(pid)
{
    ManagedPlayer.addPlayer(pid);
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerDisconnect", function(pid)
{
    ManagedPlayer.removePlayer(pid);
});

// ------------------------------------------------------------------- //

local cellUpdateNextTick = getTickCount();
local cellUpdateTickInterval = 1000;

addEventHandler("onTick", function()
{
    local tick = getTickCount();
    if (tick > cellUpdateNextTick)
    {
        for (local pid = 0; pid < getMaxSlots(); pid++)
            if (isPlayerConnected(pid))
                ManagedPlayer.getPlayer(pid).updateCell();

        cellUpdateNextTick = tick + cellUpdateTickInterval;
    }
});

// ------------------------------------------------------------------- //

class ManagedPlayer
{
    // ------------------------------------------------------------------- //

    function updateCell()
    {
        local pos = getPlayerPosition(m_ID);
        local newCell = m_CellSystem.getIndexByPos(pos.x, pos.z);

        if (m_Cell != newCell)
        {
            if (m_Cell != null)
                m_CellSystem.removeFromCell(m_Cell, m_ID);

            onChangeCell();

            m_Cell = newCell;
            m_CellSystem.addToCell(newCell, m_ID, this);
        }
    }

    // ------------------------------------------------------------------- //

    function onChangeCell()
    {
        updateVisibleBots();
    }

    // ------------------------------------------------------------------- //

    function updateVisibleBots()
    {
        local oldVisibleBots = m_VisibleBots;
        local pos = getPlayerPosition(getID());

        // -- put all bots from Bot.m_CellSystem in m_VisibleBots that roughly have distance of m_MaxInViewDistance
        // -- Actual distance can be higher. Search shape isn't cylinder, but square
        m_VisibleBots = Bot.m_CellSystem.getContentNear(pos.x, pos.z, m_MaxInViewDistance);
        // -- ignore bots in other worlds -- //
        foreach (key, bot in m_VisibleBots)
            if (bot.getWorld() != getPlayerWorld(getID()))
                delete m_VisibleBots[key];

        foreach (bot in oldVisibleBots)
        {
            if (!(bot in m_VisibleBots))
                bot.setPlayerNotVisible(this);
        }

        foreach (bot in m_VisibleBots)
        {
            if (!(bot in oldVisibleBots))
                bot.setPlayerVisible(this);
        }
    }

    // ------------------------------------------------------------------- //

    function setBotVisible(bot)
    {
        local id = bot.getID();
        if (!(id in m_VisibleBots))
            m_VisibleBots[id] <- bot;
    }

    // ------------------------------------------------------------------- //

    function setBotNotVisible(bot)
    {
        local id = bot.getID();
        if (id in m_VisibleBots)
            delete m_VisibleBots[id];
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getID() {return m_ID;}
    function getVisibleBots() {return m_VisibleBots;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function getAllVisibleBots()
    {
        local visibleBots = {};

        foreach (player in m_ManagedPlayers)
            foreach (bot in player.getVisibleBots())
                visibleBots[bot.getID()] <- bot;

        return visibleBots;
    }

    // ------------------------------------------------------------------- //

    static function addPlayer(id)
    {
        m_ManagedPlayers[id] <- ManagedPlayer(id);
    }

    // ------------------------------------------------------------------- //

    static function removePlayer(id)
    {
        delete m_ManagedPlayers[id];
    }

    // ------------------------------------------------------------------- //

    static function getPlayer(id)
    {
        return m_ManagedPlayers[id];
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(id)
    {
        m_ManagedPlayers[id] <- this;
        m_ID = id;

        m_VisibleBots = [];

        updateCell();
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ID = -1;
    m_Cell = null;
    m_VisibleBots = null;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_ManagedPlayers = {};
    static m_CellSystem = Cell(2000);
    static m_MaxInViewDistance = 4000;
}
