// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class ActionComponent extends UniqueComponent
{
    function tick(deltaTime)
    {
        base.tick(deltaTime);

        local combatComp = getParent().getCombatComp();

        // -- parent is fighting. ignore actionqueue during fight -- //
        if (combatComp != null && combatComp.isFighting() && getParent().isInView())
        {
            combatComp.tick(deltaTime);
        }
        else
        {
            executeCurrent();
        }
    }

    // ------------------------------------------------------------------- //

    function isNextAction(funcName)
    {
        if (m_ActionQueue.len() < 2)
            return false;
        return m_ActionQueue[m_ActionQueue.len() - 2].funcName == funcName;
    }

    // ------------------------------------------------------------------- //

    function hasActionInQueue(funcName)
    {
        foreach (i, action in m_ActionQueue)
            if (action.funcName == funcName)
                return true;
        return false;
    }

    // ------------------------------------------------------------------- //

    // -- don't call directly. Use individual action method -- //
    function add(priority, funcName, params, unique = false, restart = false)
    {
        if (unique)
        {
            foreach (i, action in m_ActionQueue)
            {
                if (params.funcName == funcName)
                {
                    if (restart)
                    {
                        m_ActionQueue.remove(i);
                        break;
                    }
                    else
                        return null;
                }
            }
        }

        local action = {["funcName"] = funcName, ["params"] = params};

        if (priority == PRIORITY_LOW)
            m_ActionQueue.insert(0, action);
        else
            m_ActionQueue.append(action);
    }

    // ------------------------------------------------------------------- //

    function executeCurrent()
    {
        if (m_ActionQueue.len() != 0)
        {
            local action = m_ActionQueue[m_ActionQueue.len() - 1];
            m_CurrAction = action.funcName;

            if (!this[action.funcName](action.params))
                m_ActionQueue.pop();

            m_LastAction = action.funcName;
        }
        else if (getParent().isInView())
        {
            m_CurrAction = "idle";
            exIdle();
            m_LastAction = "idle";
        }
    }

    // ------------------------------------------------------------------- //

    function removeCurrent()
    {
        if (m_ActionQueue.len() > 0)
        {
            m_ActionQueue.pop();
        }
    }

    // ------------------------------------------------------------------- //

    function clear()
    {
        m_ActionQueue.clear();
    }

    // ------------------------------------------------------------------- //

    function remove(index)
    {
        if (index in m_ActionQueue)
            m_ActionQueue.remove(index);
    }

    // ------------------------------------------------------------------- //

    function removeByFuncName(funcName, removeAll = true)
    {
        for (local i = 0; i < m_ActionQueue.len(); i++)
        {
            local action = m_ActionQueue[i];
            if (action.funcName == funcName)
            {
                m_ActionQueue.remove(i);
                if (!removeAll)
                    break;
                i--;
            }
        }
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Add action -- //

    function addGotoWP(wp)
    {
        add(PRIORITY_LOW, "exGotoWP", {["wp"] = wp,  ["path"] = null});
    }

    // ------------------------------------------------------------------- //

    function addGoto(x, y, z)
    {
        add(PRIORITY_LOW, "exGoto", {["x"] = x, ["y"] = y, ["z"] = z, ["wp"] = null, ["path"] = null});
    }

    // ------------------------------------------------------------------- //

    function addSetAngle(angle)
    {
        add(PRIORITY_LOW, "exSetAngle", {["angle"] = angle});
    }

    // ------------------------------------------------------------------- //

    function addTurn(angle)
    {
        add(PRIORITY_LOW, "exTurn", {["angle"] = angle});
    }

    // ------------------------------------------------------------------- //

    function addPlayAni(ani, aniLength = 1000)
    {
        add(PRIORITY_LOW, "exPlayAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false});
    }

    // ------------------------------------------------------------------- //

    function addPlayRandomAni(ani, aniLength = 1000)
    {
        add(PRIORITY_LOW, "exPlayRandomAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false});
    }

    // ------------------------------------------------------------------- //

    function addRepeatAni(ani, aniLength = 1000, repeatCount = 0)
    {
        add(PRIORITY_LOW, "exRepeatAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false, ["repeatCount"] = repeatCount});
    }

    // ------------------------------------------------------------------- //

    function addRepeatRandomAni(ani, aniLength = 1000, repeatCount = 0)
    {
        add(PRIORITY_LOW, "exRepeatRandomAni", {["ani"] = ani, ["aniLength"] = aniLength, ["endAt"] = null, ["playedAni"] = false, ["repeatCount"] = repeatCount});
    }

    // ------------------------------------------------------------------- //

    function addSleep(wp, angle)
    {
        add(PRIORITY_LOW, "exSleep", {["wp"] = wp, ["angle"] = angle});
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Execute action -- //

    function exIdle()
    {
    }

    // ------------------------------------------------------------------- //

    // -- Don't use waypoints -- //
    function exGotoDirect(x, y, z)
    {
        local parent = getParent();
        local pos = parent.getPosition();

        if (isFirstExecution())
            parent.ignoreStuckOnce();

        // -- bot isn't close to any player. Just cheat and immediately set him to his target -- //
        if (!(parent.isInView()) || parent.isStuck())
        {
            parent.ignoreStuckOnce();
            parent.forcePosition(x, y, z);
            return false;
        }

        local dist = getDistance2d(pos.x, pos.z, x, z);

        if (dist <= m_MinGotoTargetDist)
            // -- Don't repeat action -- //
            return false;

        local targetAngle = getVectorAngle(pos.x, pos.z, x, z);

        parent.setInterpAngle(targetAngle, m_GotoTurnSpeed);
        parent.playMovementAni();

        // -- Repeat action -- //
        return true;
    }

    // ------------------------------------------------------------------- //

    // -- Use waypoints. Target is waypoint -- //
    function exGotoWP(params)
    {
        local parent = getParent();

        local world = parent.getWorld();
        local pos = parent.getPosition();
        local wp = params.wp;

        // -- ToDo: Notice when path is invalid due to the bot straying from path too far (teleportation, other action that was executed meanwhile etc.)

        // -- Generate path -- //
        if (isFirstExecution())
        {
            local wpNameStart = WayPoints.getNearestWP(pos.x, pos.y, pos.z, world);
            params.path = WayPoints.getWPRoute(world, wpNameStart, wp);
        }

        if (params.path == null || params.path.len() == 0)
            return false;

        local currNodeName = params.path.top();
        local currNode = WayPoints.getWPByName(world, currNodeName);

        if (!exGotoDirect(currNode.x, currNode.y, currNode.z))
        {
            // -- Set to precise position when arriving at target node -- //
            if (params.path.len() == 1)
            {
                parent.stopAni();
                parent.forcePosition(currNode.x, pos.y, currNode.z);
            }

            params.path.pop();
        }

        return true;
    }

    // ------------------------------------------------------------------- //

    // -- First use waypoints, then directly go to coordinate. Target is coordinate -- //
    function exGoto(params)
    {
        local parent = getParent();

        local world = parent.getWorld();
        local pos = parent.getPosition();
        local x = params.x;
        local y = params.y;
        local z = params.z;

        // -- speed needs to be updated -- //
        if (isFirstExecution())
        {
            params.wp = WayPoints.getNearestWP(x, y, z, world);
        }

        if (!exGotoWP(params))
        {
            if (!exGotoDirect(x, y, z))
            {
                parent.stopAni();
                parent.forcePosition(x, y, z);
                return false;
            }
        }
        return true;
    }

    // ------------------------------------------------------------------- //

    function exSleep(params)
    {
        local wpName = params.wp;
        local angle = params.angle;

        addGotoWP(wpName);
        addTurn(angle);
        addPlayRandomAni("sleep");

        return false;
    }

    // ------------------------------------------------------------------- //

    function exTurn(params)
    {
        local parent = getParent();

        local targetAngle = params.angle;
        parent.setInterpAngle(targetAngle, m_GotoTurnSpeed);

        // -- Cancel action if angles are equal -- //
        return parent.getAngle() != targetAngle;
    }

    // ------------------------------------------------------------------- //

    function exSetAngle(params)
    {
        local angle = params.angle;

        setAngle(angle);
        return false;
    }

    // ------------------------------------------------------------------- //

    function exPlayAni(params)
    {
        local parent = getParent();

        local ani = params.ani;
        local tick = getTickCount();

        if (params.endAt == null)
            params.endAt = tick + params.aniLength;

        if (!params.playedAni)
        {
            parent.playAni(ani);
            params.playedAni = true;
        }

        // -- Cancel if finished -- //
        return tick < params.endAt;
    }

    // ------------------------------------------------------------------- //

    function exPlayRandomAni(params)
    {
        local parent = getParent();

        local ani = params.ani;
        local tick = getTickCount();

        if (params.endAt == null)
            params.endAt = tick + params.aniLength;

        if (!params.playedAni)
        {
            parent.playRandomAni(ani);
            params.playedAni = true;
        }

        // -- Cancel if finished -- //
        return tick < params.endAt;
    }

    // ------------------------------------------------------------------- //

    // -- repeatCount <= 0 -> repeat until stopped manually -- //
    function exRepeatAni(params)
    {
        local parent = getParent();

        local ani = params.ani;
        local tick = getTickCount();

        if (params.endAt == null)
            params.endAt = tick + params.aniLength;

        if (!params.playedAni)
        {
            parent.playAni(ani);
            params.playedAni = true;

            params.endAt = tick + params.aniLength;
        }
        else if (tick >= params.endAt)
        {
            if (params.repeatCount != 1)
            {
                params.playedAni = false;
                params.repeatCount--;
            }
            else
                return false;
        }

        return true;
    }

    // ------------------------------------------------------------------- //

    // -- repeatCount <= 0 -> repeat until stopped manually -- //
    function exRepeatRandomAni(params)
    {
        local parent = getParent();

        local ani = params.ani;
        local tick = getTickCount();

        if (params.endAt == null)
            params.endAt = tick + params.aniLength;

        if (!params.playedAni)
        {
            parent.playRandomAni(ani);
            params.playedAni = true;

            params.endAt = tick + params.aniLength;
        }
        else if (tick >= params.endAt)
        {
            if (params.repeatCount != 1)
            {
                params.playedAni = false;
                params.repeatCount--;
            }
            else
                return false;
        }

        return true;
    }

    // ------------------------------------------------------------------- //

    function isFirstExecution() {return m_LastAction != m_CurrAction;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- getters -- //

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);

        m_ActionQueue = [];
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ActionQueue = null;
    // -- true if the action was called multiple times in a row -- //
    m_LastAction = "";
    m_CurrAction = "";

    // -- GOTO -- //
        m_GotoTurnSpeed = 0.01;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    // -- Determines, how close the bot has to be to the targetpoint -- //
    static m_MinGotoTargetDist = 50;
}
