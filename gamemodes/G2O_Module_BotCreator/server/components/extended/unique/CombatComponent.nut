// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local BOT_NEXTFINDENEMYTICK = getTickCount();
local BOT_FINDENEMYTICKINTERVAL = 1500;

// ------------------------------------------------------------------- //

addEventHandler("onTick", function()
{
    local tick = getTickCount();

    if (tick > BOT_NEXTFINDENEMYTICK)
    {
        foreach (bot in Bot.m_Bots)
        {
            if (!bot.isDead() && bot.isInView())
            {
                local combatComp = bot.getCombatComp();
                if (combatComp != null)
                {
                    local enemy = combatComp.findEnemy();
                    if (enemy != null)
                        combatComp.setEnemy(enemy);
                }
            }
        }

        BOT_NEXTFINDENEMYTICK = tick + BOT_FINDENEMYTICKINTERVAL;
    }
});

// ------------------------------------------------------------------- //

class CombatComponent extends UniqueComponent
{
    function tick(deltaTime)
    {
        base.tick(deltaTime);

        handleCombat();
    }

    // ------------------------------------------------------------------- //

    // -- State machine:

    // -- idle -> warnStart

    // -- warnStart -> warn
    // -- warn -> drawStart/sheatheStart

    // -- drawStart -> draw
    // -- draw -> chaseStart

    // -- chaseStart -> chase
    // -- chase -> attackStart/sheatheStart/evadeStart

    // -- evadeStart -> evade
    // -- evade -> chaseStart

    // -- attackStart -> attack
    // -- attack -> chaseStart/sheatheStart

    // -- sheathStart -> sheath
    // -- sheath -> idle
    function handleCombat()
    {
        local enemy = getEnemy();
        if (enemy != null && !enemy.isValid())
            setEnemy(null);

        switch (getCombatState())
        {
            // -- should never happen -- //
            case ECombatState.idle:
                break;

            case ECombatState.warnStart:
                exWarnStart();
               // print("State: Warn Start");
                break;
            case ECombatState.warn:
                exWarn();
               // print("State: Warn");
                break;

            case ECombatState.drawStart:
                exDrawStart();
               // print("State: Draw Start");
                break;
            case ECombatState.draw:
                exDraw();
               // print("State: Draw");
                break;

            case ECombatState.sheathStart:
                exSheathStart();
                //print("State: Sheath Start");
                break;
            case ECombatState.sheath:
                exSheath();
               // print("State: Sheath");
                break;

            case ECombatState.chaseStart:
                exChaseStart();
               // print("State: Chase Start");
                break;
            case ECombatState.chase:
                exChase();
                //print("State: Chase");
                break;

            case ECombatState.evadeStart:
                exEvadeStart();
                //print("State: Evade Start");
                break;
            case ECombatState.evade:
                exEvade();
                //print("State: Evade");
                break;

            case ECombatState.attackStart:
                exAttackStart();
               // print("State: Attack Start");
                break;
            case ECombatState.attack:
                exAttack();
                //print("State: Attack");
                break;

            case EcombatMode.fleeStart:
                exFleeStart();
                break;
            case EcombatMode.flee:
                exFlee();
                break;
        }
    }

    // ------------------------------------------------------------------- //

    function exWarnStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        parent.stopAni();
        m_EndWarnTick = getTickCount() + typeComp.getMaxWarnTime();

        setCombatState(ECombatState.warn);

        turnToEnemy(typeComp.getWarnTurnSpeed());
    }

    // ------------------------------------------------------------------- //

    function exWarn()
    {
        local tick = getTickCount();

        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        local dist = enemy.getDistance();
        local speed = parent.getSpeed();

        local pos = parent.getPosition();
        local enemyPos = enemy.getPosition();

        local targetAngle = getVectorAngle(pos.x, pos.z, enemyPos.x, enemyPos.z);

        local facingAway = Util.getAngleDelta(targetAngle, parent.getMovementAngle()) > 90;
        local tooClose = dist < typeComp.getMinWarnDist();
        local moving = parent.getSpeed() > 0.0;
        local tookTooLong = tick > m_EndWarnTick;

        // -- change to chase state -- //
        if (dist > typeComp.getMaxWarnDist())
        {
            setEnemy(null);
            return;
        }
        else if (tick >= m_MinWarnTime && (tookTooLong || tooClose) && !(moving && facingAway))
        {
            setCombatState(ECombatState.drawStart);
            return;
        }

        m_LastWarnDist = dist;

        parent.playRandomAni("warn");
    }

    // ------------------------------------------------------------------- //

    function exDrawStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        m_LastDrawTick = getTickCount() + typeComp.getDrawTime();
        parent.stopAni();

        if (parent.getWeaponMode() != typeComp.getDrawnWeaponMode())
        {
            getParent().setWeaponMode(typeComp.getDrawnWeaponMode());

            getParent().playDrawAni(typeComp.getDrawnWeaponMode());
            setCombatState(ECombatState.draw);
        }
        else
            setCombatState(ECombatState.chaseStart);
    }

    // ------------------------------------------------------------------- //

    function exDraw()
    {
        local tick = getTickCount();

        local parent = getParent();
        local enemy = getEnemy();

        if (tick >= m_LastDrawTick)
            setCombatState(ECombatState.chaseStart);
    }

    // ------------------------------------------------------------------- //

    function exSheathStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        m_LastDrawTick = getTickCount() + typeComp.getDrawTime();

        if (parent.getWeaponMode() != 0)
        {
            getParent().playSheathAni(typeComp.getDrawnWeaponMode());
            setCombatState(ECombatState.sheath);
        }
        else
            stopCombat();
    }

    // ------------------------------------------------------------------- //

    function exSheath()
    {
        local tick = getTickCount();

        local parent = getParent();
        local enemy = getEnemy();

        if (tick >= m_LastDrawTick)
        {
            stopCombat();
        }
    }

    // ------------------------------------------------------------------- //

    function exChaseStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        getParent().stopAni();
        getParent().ignoreStuckOnce();
        setCombatState(ECombatState.chase);

        turnToEnemy(typeComp.getChaseTurnSpeed());
    }

    // ------------------------------------------------------------------- //

    function exChase()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();
        local maxMoveDist = typeComp.getMaxMoveDist();

        local pos = parent.getPosition();
        local dist = enemy.getDistance();
        local pos2 = enemy.getPosition();

        local combatDist = getDistance2d(parent.getOriginPos().x, parent.getOriginPos().z, pos2.x, pos2.z);
        if (combatDist >= maxMoveDist)
        {
            
            //m_MinWarnTime = getTickCount() + m_MinCancelChaseTime;
            //setCombatState(ECombatState.warnStart);
            parent.teleport(parent.getOriginPos().x,parent.getOriginPos().y,parent.getOriginPos().z);
            parent.setHealth(parent.getMaxHealth());
            setEnemy(null);
            return;
        }
        else if (dist > typeComp.getMaxChaseDist())
        {
            setEnemy(null);
            return;
        }
        else if (dist <= typeComp.getAttackRange())
        {
            setCombatState(ECombatState.attackStart);
            return;
        }
        else if (parent.isStuck())
        {
            setCombatState(ECombatState.evadeStart);
            return;
        }

        parent.playRunAni();
    }

    // ------------------------------------------------------------------- //

    function exEvadeStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        parent.ignoreStuckOnce();

        local rnd = rand() % 2;
        if (rnd == 0)
            parent.playRandomAni("jump");
        else
            parent.playRandomAni("strafe");

        turnToEnemy(typeComp.getChaseTurnSpeed());

        setCombatState(ECombatState.evade);
        m_EvadeStateEndTick = getTickCount() + m_EvadeTime;
    }

    // ------------------------------------------------------------------- //

    function exEvade()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        local tick = getTickCount();
        if (tick >= m_EvadeStateEndTick)
            setCombatState(ECombatState.chaseStart);
    }

    // ------------------------------------------------------------------- //

    function exAttackStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        turnToEnemy(typeComp.getAttackTurnSpeed());
        parent.stopAni();
        setCombatState(ECombatState.attack);
    }

    // ------------------------------------------------------------------- //

    // -- State machine:

    // -- ready -> swing/jumpBack/strafe
    // -- swing -> recover
    // -- recover -> ready
    function exAttack()
    {
        switch (m_AttackState)
        {
            case EAttackState.ready:
            attackReady();
            break;

            case EAttackState.swing:
            attackSwing();
            break;

            case EAttackState.recover:
            attackRecover();
            break;

            case EAttackState.jumpBack:
            attackJumpBack();
            break;

            case EAttackState.strafe:
            attackStrafe();
            break;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot can potentially start attack -- //
    function attackReady()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        if (enemy.getDistance() > typeComp.getAttackRange())
        {
            setCombatState(ECombatState.chaseStart);
            return;
        }

        local tooClose = enemy.getDistance() <= typeComp.getJumpBackRange();

        // -- jump back -- //
        if (tooClose && (rand() % 100.0) / 100.0 <= typeComp.getJumpBackChance())
        {
            m_AttackStateStartTick = getTickCount();
            parent.playRandomAni("jumpBack");
            m_AttackState = EAttackState.jumpBack;
        }
        // -- strafe -- //
        else if ((rand() % 100.0) / 100.0 <= typeComp.getStrafeChance())
        {
            m_AttackStateStartTick = getTickCount();
            parent.playRandomAni("strafe");
            m_AttackState = EAttackState.strafe;
        }
        // -- attack -- //
        else if (canHit())
        {
            parent.playRandomAni(parent.getAttackAniSet());
            stopTurningToEnemy();

            m_AttackStateStartTick = getTickCount();
            m_AttackState = EAttackState.swing;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot is mid-swing -- //
    function attackSwing()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        if (tick >= m_AttackStateStartTick + typeComp.getDamageDelay())
        {
            if (canHit())
            {
                enemy.receiveHit(parent.getID());
                callEvent("onBotHit", parent.getID(), enemy.getID());
            }
            turnToEnemy(typeComp.getAttackTurnSpeed());

            m_AttackStateStartTick = getTickCount();
            m_AttackState = EAttackState.recover;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot has finished attacking. Don't attack for m_AttackRecoveryTime ms -- //
    function attackRecover()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        if (getEnemy().getDistance() > typeComp.getAttackRange())
        {
            setCombatState(ECombatState.chaseStart);
            return;
        }

        local tick = getTickCount();
        local enemy = getEnemy();

        if (tick >= m_AttackStateStartTick + typeComp.getAttackRecoveryTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    function attackJumpBack()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        if (tick >= m_AttackStateStartTick + typeComp.getJumpBackTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    function attackStrafe()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        if (tick >= m_AttackStateStartTick + typeComp.getStrafeTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    // -- ToDo
    function exFleeStart()
    {
        setCombatState(ECombatState.flee);
    }

    // ------------------------------------------------------------------- //

    // -- ToDo
    function exFlee()
    {

    }

    // ------------------------------------------------------------------- //

    function turnToEnemy(speed)
    {
        local parent = getParent();
        local enemy = getEnemy();
        parent.setInterpAngleId(enemy.getID(), speed);
    }

    // ------------------------------------------------------------------- //

    function stopTurningToEnemy()
    {
        local parent = getParent();
        parent.setInterpAngleId(-1, parent.getInterpAngleSpeed());
    }

    // ------------------------------------------------------------------- //

    function onDeath(attackerId)
    {
        stopCombat();
    }

    // ------------------------------------------------------------------- //

    // ------------------------------------------------------------------- //

    function receiveDamage(attackerId, dmg, type)
    {
        if (!isFighting() && !getParent().isDead())
            setEnemy(BotEnemy(attackerId, getParent()));
    }

    // ------------------------------------------------------------------- //

    function onStartFight()
    {

    }

    // ------------------------------------------------------------------- //

    function onEndFight()
    {
        stopTurningToEnemy();

        getParent().getActionComp().clear();
        getParent().getScheduleComp().returnToSchedule(getTime());
    }

    // ------------------------------------------------------------------- //

    function canHit()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        local pos = parent.getPosition();
        local enemyPos = enemy.getPosition();

        local angle = parent.getAngle();
        local targetAngle = getVectorAngle(pos.x, pos.z, enemyPos.x, enemyPos.z);

        local angleDelta = Util.getAngleDelta(angle, targetAngle);
        local dist = enemy.getDistance();

        local enemyAni = enemy.getAni();

        local enemyParrying = enemyAni in PARRY_ANIMS;

        return angleDelta <= typeComp.getMaxAttackAngle() && dist <= typeComp.getAttackRange() && !enemyParrying;
    }

    // ------------------------------------------------------------------- //

    function hit()
    {
        getEnemy().receiveHit(getParent().getID());
    }

    // ------------------------------------------------------------------- //

    function findEnemy()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        return getNearestEnemy(typeComp.getEnemyViewDist(), true);
    }

    // ------------------------------------------------------------------- //

    function stopCombat()
    {
        local parent = getParent();

        m_Enemy = null;
        setCombatState(ECombatState.idle);
        stopTurningToEnemy();
        getParent().stopAni();

        parent.setWeaponMode(0);

        onEndFight();
    }

    // ------------------------------------------------------------------- //

    function startCombat()
    {
        local parent = getParent();
        local typeComp = getParent().getCombatTypeComp();

        m_CombatEndTick = getTickCount() + typeComp.getMaxChaseTime();
        m_CombatStartPos = parent.getPosition();

        if (isFighting() || !typeComp.getDoEverWarn())
        {
            setCombatState(ECombatState.drawStart);
        }
        else
        {
            setCombatState(ECombatState.warnStart);
            onStartFight();
        }
    }

    // ------------------------------------------------------------------- //

    function restartCombat()
    {
        local parent = getParent();
        local typeComp = getParent().getCombatTypeComp();

        m_CombatEndTick = getTickCount() + typeComp.getMaxChaseTime();
        m_CombatStartPos = parent.getPosition();

        setCombatState(ECombatState.chaseStart);
    }

    // ------------------------------------------------------------------- //

    function setEnemy(enemy)
    {
        local parent = getParent();
        local oldEnemy = m_Enemy;
        local typeComp = getParent().getCombatTypeComp();
        local isNewEnemy = oldEnemy != enemy;
        if (__test == -1)
            __test = parent.getPosition();
        if (oldEnemy != null && enemy != null)
            isNewEnemy = oldEnemy.getID() != enemy.getID();

        if (isNewEnemy)
        {
            m_Enemy = enemy;

            if (enemy == null && parent.isInView() && !parent.isDead())
            {
                // -- Try finding new enemy -- //
                parent.teleport(parent.getOriginPos().x,parent.getOriginPos().y,parent.getOriginPos().z);
                local newEnemy = findEnemy();
                if (newEnemy == null)
                {
                    parent.teleport(parent.getOriginPos().x,parent.getOriginPos().y,parent.getOriginPos().z);
                    setCombatState(ECombatState.sheathStart);
                }
                else
                {
                    m_Enemy = newEnemy;
                    restartCombat();
                }
            }
            else if (enemy != null && oldEnemy == null)
            {
                startCombat();
            }
        }
    }

    // ------------------------------------------------------------------- //

    function clearEnemy()
    {
        setEnemy(null);
    }

    // ------------------------------------------------------------------- //

    function getEnemyDistance()
    {
        if (m_Enemy != null)
        {
            local pos = getPosition();
            local enemyPos = m_Enemy.getPosition();

            return getDistance3d(pos.x, pos.y, pos.z, enemyPos.x, enemyPos.y, enemyPos.z);
        }
        return -1;
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemy(radius = 5000, alive = null)
    {
        local pos = getParent().getPosition();

        local enemyBot = getNearestEnemyBot(radius, alive);
        local enemyPlayer = getNearestEnemyPlayer(radius, alive);

        local botValid = enemyBot.isValid();
        local playerValid = enemyPlayer.isValid();

        if (botValid == playerValid)
        {
            // -- Both invalid -- //
            if (!botValid)
                return null;
            // -- Both valid. Chose closest -- //
            else
            {
                local botPos = enemyBot.getPosition();
                local botDist = getDistance3d(botPos.x, botPos.y, botPos.z, pos.x, pos.y, pos.z);

                local playerPos = enemyPlayer.getPosition();
                local playerDist = getDistance3d(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z);

                if (botDist < playerDist)
                    return enemyBot;
                else
                    return enemyPlayer;
            }
        }
        // -- One is valid -- //
        else
        {
            if (!botValid)
                return enemyPlayer;
            return enemyBot;
        }
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyPlayer(radius = 5000, alive = null)
    {
        local lastDist = radius + 1;
        local lastPid = null;
        local botPos = getParent().getPosition();

        for(local pid = 0; pid < getMaxSlots(); pid++)
        {
            if (isPlayerConnected(pid) &&
            alive != isPlayerDead(pid))
            {
                local playerWorld = getPlayerWorld(pid);
                if (playerWorld == getParent().getWorld())
                {
                    local playerPos = getPlayerPosition(pid);
                    local dist = getDistance3d(botPos.x, botPos.y, botPos.z, playerPos.x, playerPos.y, playerPos.z);
                    if (dist <= radius && dist < lastDist && getParent().getFactionComp().isPlayerEnemy(pid))
                    {
                        lastPid = pid;
                        lastDist = dist;
                    }
                }
            }
        }

        return BotEnemy(lastPid, getParent());
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyBot(radius = 5000, alive = null)
    {
        local parent = getParent();
        local pos = parent.getPosition();

        local bots = BotAI.m_CellSystem.getContentNear(pos.x, pos.z, 5000);
        local lastDistance = null;
        local lastBotId = null;

        foreach (i, bot in bots)
        {
            if (bot != parent)
            {
                local otherPos = bot.getPosition();
                local currDistance = getDistance3d(pos.x, pos.y, pos.z, otherPos.x, otherPos.y, otherPos.z);

                if (currDistance <= radius && (currDistance < lastDistance || lastBotId == null) && parent.getFactionComp().isBotEnemy(bot) && (alive == null || !bot.isDead() ))
                {
                    lastDistance = currDistance;
                    lastBotId = bot.getID();
                }
            }
        }

        return BotEnemy(lastBotId, getParent());
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setCombatState(state){m_CombatState = state;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getEnemy(){return m_Enemy;}
    function getAttackRange(){return m_AttackRange;}
    function getCombatState(){return m_CombatState;}
    function isFighting() {return m_CombatState != ECombatState.idle;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Enemy = null;
    m_CombatState = ECombatState.idle;

    m_CombatEndTick = 0;
    m_CombatStartPos = null;
    __test = -1;

    // -- WARN -- //
        m_EndWarnTick = getTickCount();
        m_LastWarnDist = 0;
        m_MinWarnTime = getTickCount();

    // -- DRAW -- //
        m_LastDrawTick = 0;

    // -- ATTACK -- //
        m_AttackStateStartTick = 0;
        m_AttackState = EAttackState.ready;

    // -- EVADE -- //
        m_EvadeStateEndTick = 0;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_EvadeTime = 1000;
    static m_MinCancelChaseTime = 1000;
}
