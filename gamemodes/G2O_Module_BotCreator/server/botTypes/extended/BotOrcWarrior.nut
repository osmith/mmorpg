// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class BotOrcWarrior extends BotOrc
{
    constructor(name, x = 0, y = 0, z = 0, angle = 0, world = "NEWWORLD\\NEWWORLD.ZEN")
    {
        base.constructor(name, x, y, z, angle, world);
        m_Instance = "ORCWARRIOR_ROAM";

        m_ActionComponent = ActionComponent(this);
        m_ScheduleComponent = ScheduleComponent(this);
        m_FactionComponent = FactionComponent(this);
        m_RespawnComponent = RespawnComponent(this);
        m_CombatComponent = CombatComponent(this);

        m_AnimTypeComponent = ANIMTYPE_ORC;
        m_CombatTypeComponent = COMBATTYPE_ORC_2H;

        getFactionComp().addToFaction("orc");
    }
}
