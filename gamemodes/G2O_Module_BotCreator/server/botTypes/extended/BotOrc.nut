// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class BotOrc extends BotAI
{
    function getAttackAniSet()
    {
        switch (m_WeaponMode)
        {
            case 0:
                return "attackFist";
                break;

            case 1:
                return "attackFist";
                break;

            case 2:
            case 3:
            case 4:
            case 5:
                return "attack2h";
                break;
        }
    }

    // ------------------------------------------------------------------- //

    function playWalkAni()
    {
        if (getWeaponMode() > 1)
            playAniAt("walk", 1);
        else
            playAniAt("walk", 0);
    }

    // ------------------------------------------------------------------- //

    function playRunAni()
    {
        if (getWeaponMode() > 1)
            playAniAt("run", 1);
        else
            playAniAt("run", 0);
    }

    // ------------------------------------------------------------------- //

    function playMovementAni()
    {
        if (getWeaponMode() > 1)
            playAniAt(getMovementAniSet(), 1);
        else
            playAniAt(getMovementAniSet(), 0);
    }

    // ------------------------------------------------------------------- //

    function playDrawAni(drawnWeaponMode)
    {
        playAniAt("draw", 0);
    }

    // ------------------------------------------------------------------- //

    function playSheathAni(drawnWeaponMode)
    {
        playAniAt("sheath", 0);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(name, x = 0, y = 0, z = 0, angle = 0, world = "NEWWORLD\\NEWWORLD.ZEN")
    {
        base.constructor(name, x, y, z, angle, world);
        m_Instance = "ORCWARRIOR_ROAM";

        m_ActionComponent = ActionComponent(this);
        m_ScheduleComponent = ScheduleComponent(this);
        m_FactionComponent = FactionComponent(this);
        m_RespawnComponent = RespawnComponent(this);
        m_CombatComponent = CombatComponent(this);

        m_AnimTypeComponent = ANIMTYPE_ORC;
        m_CombatTypeComponent = COMBATTYPE_ORC_2H;

        getFactionComp().addToFaction("orc");
    }
}
