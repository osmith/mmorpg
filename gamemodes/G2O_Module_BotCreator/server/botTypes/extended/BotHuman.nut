// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class BotHuman extends BotAI
{
    constructor(name, x = 0, y = 0, z = 0, angle = 0, world = "NEWWORLD\\NEWWORLD.ZEN")
    {
        base.constructor(name, x, y, z, angle, world);
        m_Instance = "PC_HERO";

        m_ActionComponent = ActionComponent(this);
        m_ScheduleComponent = ScheduleComponent(this);
        m_FactionComponent = FactionComponent(this);
        m_RespawnComponent = RespawnComponent(this);
        m_CombatComponent = CombatComponent(this);

        m_AnimTypeComponent = ANIMTYPE_HUMAN;
        m_CombatTypeComponent = COMBATTYPE_HUM_1H;

        getFactionComp().addToFaction("human");
    }
}
