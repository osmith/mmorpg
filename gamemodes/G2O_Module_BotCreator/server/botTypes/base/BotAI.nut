// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class BotAI extends Bot
{
    function onLateInit()
    {
        base.onLateInit();

        getScheduleComp().returnToSchedule(getTime());
    }

    // ------------------------------------------------------------------- //

    function onTeleport()
    {
        local actionComponent = getActionComp();
        actionComponent.clear();

        getScheduleComp().returnToSchedule(getTime());
    }

    // ------------------------------------------------------------------- //

    function onDeath(attackerId)
    {
        base.onDeath(attackerId);

        local actionComponent = getActionComp();
        local combatComp = getCombatComp();

        actionComponent.clear();
        if (combatComp != null)
            combatComp.onDeath(attackerId);

        // -- Weapons are unequipped on death. Save them to reequip them on respawn -- //
        getRespawnComp().m_PreDeathMeleeWeapon = getMeleeWeapon();
        getRespawnComp().m_PreDeathRangedWeapon = getRangedWeapon();

        getRespawnComp().scheduleRespawn();

        playRandomAni("die");

        if (m_LogBots)
            print("Bot " + getName() + "(" + getID() + ") died");
    }

    // ------------------------------------------------------------------- //

    function onRevival()
    {
        base.onRevival();
        getRespawnComp().unScheduleRespawn();
    }

    // ------------------------------------------------------------------- //

    function damageBot(attackerId, dmg, type)
    {
        base.damageBot(attackerId, dmg, type);

        local combatComp = getCombatComp();
        if (combatComp != null)
            combatComp.receiveDamage(attackerId, dmg, type);
    }

    // ------------------------------------------------------------------- //

    function playDrawAni(drawnWeaponMode)
    {
        playAniAt("draw", drawnWeaponMode);
    }

    // ------------------------------------------------------------------- //

    function playSheathAni(drawnWeaponMode)
    {
        playAniAt("sheath", drawnWeaponMode);
    }

    // ------------------------------------------------------------------- //

    function playWalkAni()
    {
        playAniAt("walk", getWeaponMode());
    }

    // ------------------------------------------------------------------- //

    function playRunAni()
    {
        playAniAt("run", getWeaponMode());
    }

    // ------------------------------------------------------------------- //

    function playMovementAni()
    {
        playAniAt(getMovementAniSet(), getWeaponMode());
    }

    // ------------------------------------------------------------------- //

    function getMovementAniSet()
    {
        switch (m_MovementMode)
        {
            case EMovementMode.walking:
                return "walk";
                break;

            case EMovementMode.running:
                return "run";
                break;

            case EMovementMode.sprinting:
                return "sprint";
                break;
        }
    }

    // ------------------------------------------------------------------- //

    function getAttackAniSet()
    {
        switch (m_WeaponMode)
        {
            case 0:
                return "attackFist";
                break;

            case 1:
                return "attackFist";
                break;

            case 2:
                return "attack1h";
                break;

            case 3:
                return "attack2h";
                break;

            case 4:
                return "attack2h";
                break;

            case 5:
                return "attack2h";
                break;
        }
    }

    // ------------------------------------------------------------------- //

    function teleport(x, y, z)
    {
        setPosition(x, y, z);
        //if (setPosition(x, y, z))
            //m_WasTeleported = true;
        onTeleport();
    }

    // ------------------------------------------------------------------- //
    // -- ANIMATION -- //

    function playNextAni(identifier)
    {
        local animSequences = getAnimTypeComp().getAnimationSequences();

        if (!isDead() && identifier in m_AnimationIndizes)
        {
            local index = m_AnimationIndizes[identifier] + 1;
            if (index > animSequences[identifier].len() - 1)
                index = 0;

            local botAni = animSequences[identifier][index];
            playAni(botAni.getAni());
            m_AnimationIndizes[identifier] = index;
        }
    }

    // ------------------------------------------------------------------- //

    function playPrevAni(identifier)
    {
        local animSequences = getAnimTypeComp().getAnimationSequences();

        if (identifier in animSequences)
        {
            local index = m_AnimationIndizes[identifier] - 1;
            if (index < 0)
                index = animSequences[identifier].len() - 1;

            local botAni = animSequences[identifier][index];
            playAni(botAni.getAni());
            m_AnimationIndizes[identifier] = index;
        }
    }

    // ------------------------------------------------------------------- //

    function playRandomAni(identifier)
    {
        local animSequences = getAnimTypeComp().getAnimationSequences();

        if (!isDead() && identifier in animSequences)
        {
            local index = rand() %  animSequences[identifier].len();
            local botAni = animSequences[identifier][index];

            playAni(botAni.getAni());
        }
    }

    // ------------------------------------------------------------------- //

    function playAniAt(identifier, at)
    {
        local animSequences = getAnimTypeComp().getAnimationSequences();

        if (identifier in animSequences && at in animSequences[identifier])
        {
            local botAni = animSequences[identifier][at];
            playAni(botAni.getAni());
        }
    }

    // ------------------------------------------------------------------- //

    function tick(deltaTime)
    {
        base.tick(deltaTime);

        m_DeltaTime = deltaTime;
        if (!isDead())
        {
            local actionComponent = getActionComp();
            actionComponent.tick(deltaTime);
        }
    }

    // ------------------------------------------------------------------- //

    function idle()
    {
        local combatComp = getCombatComp();
        if (combatComp != null)
            combatComp.setEnemy(null);

        getActionComp().clear();
        stopAni();
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function getNearestBot(x, y, z, radius = 5000)
    {
        local bots = Bot.m_CellSystem.getContentNear(x, z, 5000);
        local lastDistance = null;
        local lastBot = null;
        foreach (i, bot in bots)
        {
            local currDistance = getDistance3d(bot.m_Pos.x, bot.m_Pos.y, bot.m_Pos.z, x, y, z);
            if (currDistance <= radius && (currDistance < lastDistance || lastBot == null))
            {
                lastDistance = currDistance;
                lastBot = bot;
            }
        }

        return lastBot;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setAnimTypeComp(animTypeComp){m_AnimTypeComponent = animTypeComp;}
    function setCombatTypeComp(combatTypeComp){m_CombatTypeComponent = combatTypeComp;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- getters -- //

    function getDeltaTime(){return m_DeltaTime;}

    // -- Components -- //
    function getActionComp() {return m_ActionComponent;}
    function getScheduleComp() {return m_ScheduleComponent;}
    function getFactionComp() {return m_FactionComponent;}
    function getRespawnComp() {return m_RespawnComponent;}
    function getCombatComp() {return m_CombatComponent;}

    function getAnimTypeComp() {return m_AnimTypeComponent;}
    function getCombatTypeComp() {return m_CombatTypeComponent;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(name, x = 0, y = 0, z = 0, angle = 0, world = "NEWWORLD\\NEWWORLD.ZEN")
    {
        base.constructor(name, x, y, z, angle, world);

        m_AnimationIndizes = {};

        // -- Init components -- //
        /*m_ActionComponent = ActionComponent(this);
        m_ScheduleComponent = ScheduleComponent(this);
        m_FactionComponent = FactionComponent(this);
        m_RespawnComponent = RespawnComponent(this);
        m_CombatComponent = CombatComponent(this);

        m_AnimTypeComponent = AnimTypeComponent();
        m_CombatTypeComponent = CombatTypeComponent();*/
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    // -- Time between last 2 ticks in ms -- //
	m_DeltaTime = 0;

    m_MovementMode = EMovementMode.walking;

    // -- Indizes of last played anims -- //
    m_AnimationIndizes = null;

    // ------------------------------------------------------------------- //
    // -- COMPONENTS -- //
        m_ActionComponent = null;
        m_ScheduleComponent = null;
        m_FactionComponent = null;
        m_RespawnComponent = null;
        m_CombatComponent = null;

        m_AnimTypeComponent = null;
        m_CombatTypeComponent = null;
}
