// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEventHandler("onPacket", function(packet)
{
    local packetID = packet.readUInt16();

    switch (packetID)
    {
        case EPacketId3DDraws.packetIDCreate:

            local x = packet.readFloat();
            local y = packet.readFloat();
            local z = packet.readFloat();
            local text = packet.readString();

            WPDraws.create(x, y, z, text);
            break;

        case EPacketId3DDraws.packetIDClear:
            WPDraws.clear();
            break;
    }
});

// ------------------------------------------------------------------- //

class WPDraws
{
    static function create(x, y, z, text)
    {
        local draw = Draw3d(x, y, z);
        draw.insertText(text);
        draw.visible = true;

        draws.append(draw);
    }

    // ------------------------------------------------------------------- //

    static function clear()
    {
        draws.clear();
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static draws = [];
}
