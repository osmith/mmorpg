// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local botInterpInterval = 10;
local botInterpLastTick = getTickCount();

local botSpawnInterval = 2000;
local botSpawnNextTick = getTickCount();

local botCheckHealthInterval = 100;
local botCheckHealthNextTick = getTickCount() + botCheckHealthInterval;

// ------------------------------------------------------------------- //

addEventHandler("onRender", function()
{
    local tick = getTickCount();
    if (tick >= botInterpLastTick + botInterpInterval)
    {
        local deltaTime = tick - botInterpLastTick;

        foreach (bot in Bot.m_Bots)
        {
            if (bot.isSpawned())
            {
                bot.interpAngle(deltaTime);
                //bot.interpPosition(deltaTime);
            }
        }

        botInterpLastTick = tick;
    }

    if (tick >= botSpawnNextTick)
    {
        foreach (bot in Bot.m_Bots)
            bot.updateIsStreamed();

        botSpawnNextTick = tick + botSpawnInterval;
    }

    if (tick >= botCheckHealthNextTick)
    {
        foreach (bot in Bot.m_Bots)
        {
            bot.checkHealth();
        }

        botCheckHealthNextTick = tick + botCheckHealthInterval;
    }
});

// ------------------------------------------------------------------- //

class BotStruct
{
    function interpAngle(deltaTime)
    {
        // -- first try turning to static targetAngle -- //
        local targetAngle = getInterpAngle();
        if (targetAngle >= 0)
        {
            local currAngle = getPlayerAngle(getID());
            local interpAngle = Util.lerpAngle(currAngle, targetAngle, getInterpAngleSpeed() * deltaTime);

            setPlayerAngle(getID(), interpAngle);
            setAngle(interpAngle);

            if (interpAngle == targetAngle)
            {
                m_InterpAngleId = -1;
                m_InterpAngle = -1;
            }
        }
        // -- no static targetAngle to turn towards. Try turning towards player/bot -- //
        else if (getInterpAngleId() >= 0)
        {
            interpAngleId(deltaTime);
            return;
        }
    }

    // ------------------------------------------------------------------- //

    function interpAngleId(deltaTime)
    {
        local targetId = getInterpAngleId();

        local currPos = getPlayerPosition(getID());
        local targetPos = getPlayerPosition(targetId);
        local targetAngle = getVectorAngle(currPos.x, currPos.z, targetPos.x, targetPos.z);

        local currAngle = getPlayerAngle(getID());
        local interpAngle = Util.lerpAngle(currAngle, targetAngle, getInterpAngleSpeed() * deltaTime);

        setPlayerAngle(getID(), interpAngle);
        setAngle(interpAngle);
    }

    // ------------------------------------------------------------------- //

    /*function interpPosition(deltaTime)
    {
        local targetPos = getInterpPosition();
        if (targetPos != null)
        {
            local currPos = getPlayerPosition(getID());
            local x = Util.lerp(currPos.x, targetPos.x, m_PosInterpDelta);
            local y = Util.lerp(currPos.y, targetPos.y, m_PosInterpDelta);
            local z = Util.lerp(currPos.z, targetPos.z, m_PosInterpDelta);

            setPlayerPosition(getID(), x, y, z);
            setPosition(x, y, z);

            if (x == targetPos.x && y == targetPos.y && z == targetPos.z)
                m_InterpPos = null;
        }
    }*/

    // ------------------------------------------------------------------- //

    // -- has the health of the bot changed without a health packet being received
    // -- or a damage event being triggered?
    function checkHealth()
    {
        if (isSpawned())
        {
            local oldHealth = getHealth();
            local newHealth = getPlayerHealth(getID());
            if (newHealth != oldHealth)
            {
                local healthDelta = newHealth - oldHealth;
                if (healthDelta < 0)
                    onTakeUnknownDamage(abs(healthDelta));
                else
                    onReceiveUnknownHeal(healthDelta);
            }
        }
    }

    // ------------------------------------------------------------------- //

    function onTakeUnknownDamage(dmg)
    {
        setHealth(getPlayerHealth(getID()));
        if (getSyncPid() == heroId)
            Bot.sendUnknownDamage(this, dmg);
    }

    // ------------------------------------------------------------------- //

    function onReceiveUnknownHeal(amount)
    {
        setHealth(getPlayerHealth(getID()));
        if (getSyncPid() == heroId)
            Bot.sendUnknownHeal(this, dmg);
    }

    // ------------------------------------------------------------------- //

    // -- spawns/unspawns the bot -- //
    function handleSpawnUnspawn()
    {
        local playerWorld = getWorld();
        local playerVWorld = getVirtualWorld();

        if (isSpawned())
        {
            if (playerWorld != getBotWorld() ||  playerVWorld != getVirtualWorld() || !isInView() || !isStreamed())
            {
                unspawn();
            }
        }
        else
        {
            if (playerWorld == getBotWorld() && playerVWorld == getVirtualWorld() && isInView() && isStreamed())
            {
                spawn();
            }
        }
    }

    // ------------------------------------------------------------------- //

    function spawn()
    {
        local id = getID();

        spawnNpc(id);

        local armor = getArmor();
        local meleeWeapon = getMeleeWeapon();
        local rangedWeapon = getRangedWeapon();
        local shield = getShield();
        local helmet = getHelmet();
        local pos = getPosition();
        local ani = getAni();
        local weaponMode = getWeaponMode();
        local angle = getAngle();

        setPlayerInstance(id, getInstance());
        setPlayerVisual(id, getBodyModel(), getBodyTexture(), getHeadModel(), getHeadTexture());
        setPlayerFatness(id, getFatness());

        setPlayerSkillWeapon(id, WEAPON_1H, getOneH());
        setPlayerSkillWeapon(id, WEAPON_2H, getTwoH());
        setPlayerSkillWeapon(id, WEAPON_BOW, getBow());
        setPlayerSkillWeapon(id, WEAPON_CBOW, getCBow());

        setPlayerMaxHealth(id, getMaxHealth());
        setPlayerHealth(id, getHealth());
        setPlayerPosition(getID(), pos.x, pos.y, pos.z);
        setPlayerStrength(id, getStrength());
        setPlayerDexterity(id, getDexteriy());
        setPlayerScale(id, getScale().x,getScale().y,getScale().z);

        if (armor >= 0)
            equipArmor(id, armor);
        if (meleeWeapon >= 0)
            equipMeleeWeapon(id, meleeWeapon);
        if (rangedWeapon >= 0)
            equipRangedWeapon(id, rangedWeapon);
        if (shield >= 0)
            equipShield(id, shield);
        if (helmet >= 0)
            equipHelmet(id, helmet);

        setPlayerWeaponMode(id, weaponMode);

        // -- workaround for restoring animations when spawning due to bots falling down when spawning -- //
        setTimer(function(bot = this)
        {
            setPlayerPosition(id, pos.x, pos.y, pos.z);
            setPlayerAngle(id, angle);
            playAni(id, ani);
        }, 500, 1);

        setSpawned(true);
    }

    // ------------------------------------------------------------------- //

    function unspawn()
    {
        local pos = getPlayerPosition(getID());

        setAni(getPlayerAni(getID()));
        setPosition(pos.x, pos.y, pos.z);
        setAngle(getPlayerAngle(getID()));

        setSpawned(false);
        unspawnNpc(getID());
    }

    // ------------------------------------------------------------------- //

    function updateIsStreamed()
    {
        local playerPos = getPlayerPosition(heroId);
        local pos = getEffectivePosition();

        local streamed = true;
        // -- if bot has to be synced to server, always stream -- //
        if (!getSyncBack())
        {
            local dist = getDistance3d(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z);
            streamed = dist <= m_MaxRenderDistance;
        }

        setStreamed(streamed);
    }

    // ------------------------------------------------------------------- //

    function setStreamed(mode)
    {
        if (mode != m_Streamed)
        {
            m_Streamed = mode;
            handleSpawnUnspawn();
        }
    }

    // ------------------------------------------------------------------- //

    function getEffectivePosition()
    {
        if (isSpawned())
            return getPlayerPosition(getID());
        return getPosition();
    }

    // ------------------------------------------------------------------- //

    constructor(id)
    {
        m_ID = id;
        m_Pos = {x = 0, y = 0, z = 0};
        //m_InterpPos = null;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setID(id) {m_ID = id;}
    function setInView(inView) {m_InView = inView;}
    function setSpawned(spawned) {m_Spawned = spawned;}
    function setSyncPid(syncPid) {m_SyncPid = syncPid;}

    //function setInterpPosition(x, y, z) {m_InterpPos = {["x"] = x, ["y"] = y, ["z"] = z};}
    function setInterpAngleId(id){m_InterpAngleId = id;}
    function setInterpAngle(angle){m_InterpAngle = angle;}
    function setInterpAngleSpeed(speed) {m_InterpAngleSpeed = speed;}

    function setName(name) {m_Name = name;}
    function setInstance(instance) {m_Instance = instance;}
    function setPosition(x, y, z) {m_Pos.x = x; m_Pos.y = y; m_Pos.z = z;}
    function setAngle(angle) {m_Angle = angle;}
    function setAni(ani) {m_Ani = ani;}
    function setBotWorld(world) {m_World = world;}
    function setWeaponMode(weaponMode) {m_WeaponMode = weaponMode;}

    function setMaxHealth(maxHealth) {m_MaxHealth = maxHealth;}
    function setHealth(health)
    {
        if (health < 0)
            health = 0;
        m_Health = health;
    }
    function setDexterity(dexterity) {m_Dexterity = dexterity;}
    function setStrength(strength) {m_Strength = strength;}
    function setOneH(oneH) {m_OneH = oneH;}
    function setTwoH(twoH) {m_TwoH = twoH;}
    function setBow(bow) {m_Bow = bow;}
    function setCBow(cBow) {m_CBow = cBow;}

    function setArmor(armor) {m_Armor = armor;}
    function setMeleeWeapon(meleeWeapon) {m_MeleeWeapon = meleeWeapon;}
    function setRangedWeapon(rangedWeapon) {m_RangedWeapon = rangedWeapon;}
    function setShield(shield) {m_Shield = shield;}
    function setHelmet(helmet) {m_Helmet = helmet;}

    function setHeadModel(headModel) {m_HeadModel = headModel;}
    function setHeadTexture(headTexture) {m_HeadTexture = headTexture;}
    function setBodyModel(bodyModel) {m_BodyModel = bodyModel;}
    function setBodyTexture(bodyTexture) {m_BodyTexture = bodyTexture;}
    function setFatness(fatness) {m_Fatness = fatness;}

    function setVirtualWorld(virtualWorld) {m_VirtualWorld = virtualWorld;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getID() {return m_ID;}
    function isInView() {return m_InView;}
    function isStreamed() {return m_Streamed;}
    function isSpawned() {return m_Spawned;}
    function getSyncPid() {return m_SyncPid;}
    function getSyncBack() {return m_SyncPid == heroId;}

    //function getInterpPosition() {return m_InterpPos;}
    function getInterpAngleId() {return m_InterpAngleId;}
    function getInterpAngle() {return m_InterpAngle;}
    function getInterpAngleSpeed() {return m_InterpAngleSpeed;}

    function getName(){return m_Name;}
    function getInstance(){return m_Instance;}
    function getPosition(){return m_Pos;}
    function getAngle(){return m_Angle;}
    function getAni(){return m_Ani;}
    function getBotWorld(){return m_World;}
    function getWeaponMode(){return m_WeaponMode;}

    function getMaxHealth(){return m_MaxHealth;}
    function getHealth(){return m_Health;}
    function getDexteriy(){return m_Strength;}
    function getStrength(){return m_Dexterity;}
    function getOneH(){return m_OneH;}
    function getTwoH(){return m_TwoH;}
    function getBow(){return m_Bow;}
    function getCBow(){return m_CBow;}

    function getArmor(){return m_Armor;}
    function getMeleeWeapon(){return m_MeleeWeapon;}
    function getRangedWeapon(){return m_RangedWeapon;}
    function getShield(){return m_Shield;}
    function getHelmet(){return m_Helmet;}

    function getHeadModel(){return m_HeadModel;}
    function getHeadTexture(){return m_HeadTexture;}
    function getBodyModel(){return m_BodyModel;}
    function getBodyTexture(){return m_BodyTexture;}
    function getFatness(){return m_Fatness;}

    function getVirtualWorld(){return m_Fatness;}
    function getScale(){return m_Scale;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ID = -1
    m_SyncPid = -1;
    m_AniChanged = false;
    // -- in view of any player -- //
    m_InView = false;
    // -- in view of this client -- //
    m_Streamed = false;
    m_Spawned = false;
    m_VirtualWorld = 0;

    //m_InterpPos = null;
    m_InterpAngleId = -1;
    m_InterpAngle = -1;
    m_InterpAngleSpeed = 0.0;

    // -- true when the bot didn't move between last two sends. Bot's speed on server is now 0,
    // -- don't send anymore
    m_DontSendPosNextTime = false;

    m_Name = null;
    m_Instance = null;
    m_World = null;
    m_Pos = null;
    m_Angle = null;
    m_Ani = null;
    m_Level = 0;
    m_Scale = -1;

    // -- Stats -- //
        m_MaxHealth = null;
        m_Health = null;
        m_Strength = null;
        m_Dexterity = null;
        m_OneH = null;
        m_TwoH = null;
        m_Bow = null;
        m_CBow = null;
        m_WeaponMode = null;

    // -- equiment -- //
        m_Armor = null;
        m_MeleeWeapon = null;
        m_RangedWeapon = null;
        m_Shield = null;
        m_Helmet = null;

    // -- Visuals -- //
        m_BodyModel = null;
        m_BodyTexture = null;
        m_HeadModel = null;
        m_HeadTexture = null;
        m_Fatness = null;

    // ------------------------------------------------------------------- //

    //static m_PosInterpDelta = 0.1;
    static m_MaxRenderDistance = 4000;
}

function BotStruct::getLevel() {
    return m_Level;
}

function BotStruct::setLevel(lvl) {
    m_Level = lvl;
}

function BotStruct::setScale(x,y,z) {
    m_Scale = {x = x, y = y, z = z};
}