// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

enum EPacketId3DDraws
{
    packetIDCreate = 9950,
    packetIDClear = 9951,
}

// -- TC: to client
// -- TS: to server
enum EPacketIdBots
{
    TCUpdateBot = 400,
    //TCPlayAni = 420,
    //TCStopAni = 430,
    TCSetPosImmediate = 440,
    TCCreate = 450,
    TCDestroy = 460,
    TCRespawn = 470,
    TCHit = 480,
    TCHitAndSendBack = 490,
    // -- -- //
    TSUpdatePos = 500,
    TSUpdateAni = 510,
    TSBotHit = 520,
    TSDamage = 530,
    TSUnknownDamage = 540,
    TSUnknownHeal = 540,
}
